/*
 * app_usb.h
 *
 *  Created on: 6 janv. 2017
 *      Author: romain
 */

#ifndef SRC_APPS_APP_USB_H_
#define SRC_APPS_APP_USB_H_

#include "../eptk_utils/eptk_utils.h"

//GLOBALS
extern eptk_utils_UART_str_t app_usb_uart_infos_str;

int8_t app_usb_init();
void app_usb_task(void);
int8_t app_usb_rx_callback(uint8_t usb_rawchar);



#endif /* SRC_APPS_APP_USB_H_ */
