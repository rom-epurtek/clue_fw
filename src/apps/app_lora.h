/**
 * \file app_lora.h
 *  \date 03 mar. 2017
 *  \author: RS
 *  \brief LoRa management library.
 *
 */

#ifndef APPS_APP_LORA_H_
#define APPS_APP_LORA_H_

/**
 * \addtogroup LoRa
 * \brief LoRa Module Management
 * @{
 */

#include <driverlib.h>
#include "../eptk_utils/eptk_utils.h"

/*Params*/
#define LORA_PRM_END_CHAR			'\n'
#define LORA_PRM_SLEEP_TIME			1000//"4294967295" //ms
#define LORA_PRM_SIZE_TX_BUFFER		255
#define LORA_PRM_SIZE_RX_BUFFER		50
#define LORA_PRM_CMD_BUFF_SIZE		15
#define LORA_PRM_RX_TIMEOUT			10000
#define LORA_PRM_TX_RTRN_TIMEOUT	5000

/*Commands*/
#define LORA_CMD_GET_VERSION	"sys get ver\r\n"
#define LORA_CMD_SLEEP			"sys sleep 4294967295\r\n" //-> param : time in ms

#define LORA_CMD_MAC_PAUSE		"mac pause\r\n"
#define LORA_CMD_SET_MODE 		"radio set mod lora\r\n"
#define LORA_CMD_SET_FREQ		"radio set freq 868100000\r\n"
#define LORA_CMD_SET_BW			"radio set bw 125\r\n" //125
#define LORA_CMD_SET_SF			"radio set sf sf12\r\n" //12
#define LORA_CMD_SET_SYNC		"radio set sync 12\r\n"//12
#define LORA_CMD_SET_CRC_ON		"radio set crc on\r\n"
#define LORA_CMD_SET_PRLEN		"radio set prlen 8\r\n"//8
#define LORA_CMD_SET_PWR		"radio set pwr 14\r\n" //14
#define LORA_CMD_SET_WDT_0		"radio set wdt 0\r\n"
#define LORA_CMD_SET_CR			"radio set cr 4/8\r\n"//4/8
#define LORA_CMD_RX				"radio rx 0\r\n"
#define LORA_CMD_GET_SNR		"radio get snr\r\n"


/*! \brief Init lora and place it in shutdown mode
 *  \return RETURN_CODE :
 *  \li RTRN_OK        -1, init ok
 *  \li RTRN_ERR       -2, uart init went wrong
 */
int8_t app_lora_init();

/*! \brief Task periodically called in main loop
 *
 */
int8_t app_lora_task();


/*! \brief Send string pointed by *fmt to the LoRa module to be sent via LoRa.
 *  If mode is set to UART_SEND_NOW, force message sending to the LoRa module at the end of the function.
 *  If mode is set to UART_SEND_NORMAL, wait sending buffer to be full before sending it to LoRa module.
 *  \param  mode : UART_SEND_NOW or UART_SEND_NORMAL
 *  \param  *fmt : String to be sent
 *  \return RETURN_CODE :
 *  \li RTRN_OK         -1
 *  \li RTRN_FULL       -4
 *  \li RTRN_BUSY       -6
 */
int8_t app_lora_radio_printf(uint8_t mode, const char * fmt, ...);

/*! \brief Works as int8_t app_lora_radio_printf(uint8_t mode, const char * fmt, ...); without printf formating
 *  \param  *str_ptr : String to be sent
 *  \param  mode : UART_SEND_NOW or UART_SEND_NORMAL
 *  \param  charNb : length of the string to be sent
 *  \return RETURN_CODE :
 *  \li RTRN_OK         -1
 *  \li RTRN_FULL       -4
 *  \li RTRN_BUSY       -6
*/
int8_t app_lora_radio_puts(const uint8_t *str_ptr, uint8_t mode, uint16_t charNb);

/**
 * @}
 */

#endif /* APPS_APP_LORA_H_ */


