/**
 * \file app_stateMachine.h
 *
 * \date 6 juil. 2016
 * \author romain
 */

#ifndef APPS_APP_STATE_MACHINE_H_
#define APPS_APP_STATE_MACHINE_H_

#include "driverlib.h"
#include "../eptk_utils/eptk_utils.h"

/**
 * \addtogroup Main_State_Machine
 * \brief Main state machine code : Highest level application
 * @{
 */

/**
 * \brief State machine init fn
 * Place here all fonctions you have to call once before entering main loop
 */
int8_t app_stateMachine_init();
/**
 * \brief State machine task
 * Main state machine task, called in each loop
 */
int8_t app_stateMachine_task();


/**
 * @}
 */
#endif /* APPS_APP_STATE_MACHINE_H_ */
