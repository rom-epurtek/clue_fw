/**
 * \file app_mox.h
 *	\date 02 feb. 2016
 *  \author: RS
 *  \brief Mox sensor control library.
 *  Provides functions to turn on and off sensors heat resistors and to measure sensors value.
 *
 */

#ifndef SRC_APPS_APP_MOX_H_
#define SRC_APPS_APP_MOX_H_

#include "driverlib.h"
#include "../conf/bsp.h"
#include "../eptk_utils/eptk_utils.h"

/**
 * \addtogroup MOX_Sensor
 * \brief MOX Sensor Management
 * @{
 */

#define MOX_HEAT_NOX		MOX_OX_HEAT_BASE, MOX_OX_HEAT_PIN
#define MOX_SENSOR_NOX		MOX_OX_ADC_BASE, MOX_OX_ADC_PIN
#define MOX_3STE_NOX		MOX_OX_3STE_BASE, MOX_OX_3STE_PIN
//
#define MOX_HEAT_CO			MOX_RED_HEAT_BASE, MOX_RED_HEAT_PIN
#define MOX_SENSOR_CO		MOX_RED_ADC_BASE, MOX_RED_ADC_PIN
#define MOX_3STE_CO			MOX_RED_3STE_BASE, MOX_RED_3STE_PIN

#define MOX_AUTO_HEAT_DURATION		30000

//#define MOX_RESVAL_CO_LOW
//#define MOX_RESVAL_CO_HIGH

/** \brief Enumeration to define sensor for auto measurement
 */
typedef enum {
	MOX_NOX = BIT0,
	MOX_CO = BIT1,
}mox_sensor_e_t;

/** \brief Structure containing mox sensor data and flags to manage measurement.
 */
typedef struct {
	bool_t nox_heat_state_f;
	bool_t co_heat_state_f;
	uint32_t nox_value;
	uint32_t co_value;
    uint32_t nox_sensing_res;
    uint32_t co_sensing_res;
    bool_t pvt_nox_3ste_state_f;
    bool_t pvt_co_3ste_state_f;
    bool_t pvt_getting_nox_f;
    bool_t pvt_getting_co_f;
    bool_t pvt_newValueNox_f;
    bool_t pvt_newValueCo_f;
    bool_t dataNOXReadyToWrite_f;
    bool_t dataCOReadyToWrite_f;
} app_mox_str_t;

/** \brief MOX gas sensor data. It is an extern variable and is made to be used to access gas data.
 */
extern app_mox_str_t app_mox_data_str;

/** \brief Init Mox sensor related GPIO
 * \return ERROR CODE :
 * 	\li RTRN_OK			-1
 */
int8_t app_mox_init(void);

/** \brief Turn On NOx Heater
 * \return ERROR CODE :
 *  \li RTRN_OK         -1
 */
int8_t app_mox_heatOnNOx(void);

/** \brief Turn Off NOx Heater
 * \return ERROR CODE :
 *  \li RTRN_OK         -1
 */
int8_t app_mox_heatOffNOx(void);

/** \brief Turn On CO Heater
 * \return ERROR CODE :
 *  \li RTRN_OK         -1
 */
int8_t app_mox_heatOnCO(void);

/** \brief Turn Off CO Heater
 * \return ERROR CODE :
 *  \li RTRN_OK         -1
 */
int8_t app_mox_heatOffCO(void);

/** \brief Acquire NOx sensor value through 10-bit ADC
 * \return ERROR CODE :
 *  \li RTRN_OK         -1
 *  \li RTRN_BUSY       -6, ADC is currently busy with an other conversion on the same or on a different channel
 */
int8_t app_mox_measureNOx(void);

int8_t app_mox_measureCO(void);

/** \brief Set on the heater of the sensor (NOx, CO or both) for the provided time. At the end of the timer,
 * app_mox_task() performs the measurement and turn off heater.
 * \param duration : heating time before measurement
 * \param sensor_e : MOX_NOX or MOX_CO
 * \return ERROR CODE :
 * 	\li RTRN_OK			-1, an automatic measure has been successfully initiated
 * 	\li RTRN_BUSY		-2, an automatic measure is already performing
 */
int8_t app_mox_measureAuto(uint32_t duration, mox_sensor_e_t sensor_e);

/** \brief Reset automatic measure parameters. Cancel any performing automatic measurement. Turn off all heaters.
 */
void app_mox_measureAutoCancel(void);


/** \brief Periodically called function. If automatic measurement timer is finished, set off heater and start conversion.
 * If a conversion is finished, store sensor resistance value in the data structure.
 * If necessary, switch to low or high sensing resistance resistance to maximize sensing sensibility.
 * \return ERROR CODE :
 *  \li RTRN_OK         -1
 */
int8_t app_mox_task(void);

/**
 * @}
 */

#endif /* SRC_APPS_APP_MOX_H_ */
