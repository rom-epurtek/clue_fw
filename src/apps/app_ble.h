/*
 * app_ble.h
 *
 *  Created on: 6 janv. 2017
 *      Author: romain
 */

#ifndef SRC_APPS_APP_BLE_H_
#define SRC_APPS_APP_BLE_H_

#include "../eptk_utils/eptk_utils.h"

//GLOBALS
extern eptk_utils_UART_str_t app_ble_uart_infos_str;

int8_t app_ble_init();
void app_ble_task(void);
int8_t app_ble_rx_callback(uint8_t ble_rawchar);

#endif /* SRC_APPS_APP_BLE_H_ */
