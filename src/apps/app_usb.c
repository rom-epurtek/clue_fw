/*
 * app_usb.c
 *
 *  Created on: 6 janv. 2017
 *      Author: romain
 */


#include "driverlib.h"
#include "app_usb.h"
#include "../eptk_utils/uart.h"
#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include <stdio.h>
#include <stdarg.h>

#include "app_console.h"

/* Variausbs declaration */

//GLOBALS
eptk_utils_UART_str_t app_usb_uart_infos_str;
//LOCALS
//eptk_utils_UART_struc_t app_usb_uart_infos_str;
timer_str_t usb_todo_timer_str;
volatile bool_t app_usb_todo_f = FALSE;

int8_t app_usb_init(){

	int8_t error = RTRN_OK;

	// Configure Unused BSL pins
    GPIO_setAsInputPin(BSL_RX_BASE, BSL_RX_PIN);
    GPIO_setAsInputPin(BSL_TX_BASE, BSL_TX_PIN);

    //#PWREN PIN init moved to alim

    /* 115200bps */
	error = eptk_utils_UART_Init(FTDI_UART_RX_BASE, FTDI_UART_RX_PIN, FTDI_UART_TX_PIN, &app_usb_uart_infos_str, app_usb_rx_callback,
						USCI_A_UART_CLOCKSOURCE_SMCLK, 13, 9, 0,
						USCI_A_UART_NO_PARITY, USCI_A_UART_LSB_FIRST, USCI_A_UART_ONE_STOP_BIT, USCI_A_UART_MODE, USCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION);

    //921600bps
	/*error = eptk_utils_UART_Init(FTDI_UART_RX_BASE, FTDI_UART_RX_PIN, FTDI_UART_TX_PIN, &app_usb_uart_infos_str, app_usb_rx_callback,
							USCI_A_UART_CLOCKSOURCE_SMCLK, 27, 0, 1,
							USCI_A_UART_NO_PARITY, USCI_A_UART_LSB_FIRST, USCI_A_UART_ONE_STOP_BIT, USCI_A_UART_MODE, USCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION);
	*/
	if(error < RTRN_OK){
		return error;
	}

	/*	INIT Interruptions */
	eptk_utils_UART_rx_on(&app_usb_uart_infos_str);

	return error;
}

void app_usb_task(void){
	//uint8_t rcvdChar = 0;
	/*if(app_usb_todo_f){
		rcvdChar = eptk_utils_bufferPop(&app_usb_uart_infos_str.RX_buffer_struc);
		if(rcvdChar){
			eptk_utils_UART_sendMsg(&rcvdChar, &app_usb_uart_infos_str, 1);
		}
		else{
			app_usb_todo_f = FALSE;
		}
		//eptk_utils_UART_puts(&usb_uart_infos,UART_SEND_NOW,"Coucou !\r\n", sizeof("Coucou !\r\n");
		//eptk_utils_UART_send(&app_usb_uart_infos_str,UART_SEND_NOW,"Coucou !\r\n");
	}*/
}

int8_t app_usb_rx_callback(uint8_t usb_rawchar){

	int8_t error = RTRN_OK;
	eptk_utils_bufferPush(&app_usb_uart_infos_str.RX_buffer_struc,usb_rawchar);

	app_console_todo_f = TRUE;
	app_usb_todo_f = TRUE;
	//Routine d'interruption RX

	return error;
}
