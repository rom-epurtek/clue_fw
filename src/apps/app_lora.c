/*
 * app_lora.c
 *
 *  Created on: 5 aout 2016
 *      Author: romain
 */

#include <driverlib.h>
#include "app_lora.h"
#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "app_console.h"
#include "app_usb.h"

typedef enum {
	LORA_MODE_UNINIT = 0,
	LORA_MODE_SLEEP,
	LORA_MODE_ACTIVE,
	LORA_MODE_RX,
	LORA_MODE_RX_WAIT,
	LORA_MODE_TX_WAIT,
	LORA_MODE_TX,
} lora_state_e_t;

typedef struct{
	const char * cmd_ptr_buff_a[LORA_PRM_CMD_BUFF_SIZE];
	uint8_t cmd2sendIdx, cmd2pushIdx;
	uint8_t txCount;
    uint8_t radioTxBuff_a [LORA_PRM_SIZE_TX_BUFFER];
    bool_t newData_f;
    bool_t msgToRead_f;
    bool_t cmd2send_f;
	bool_t txPeriodTimeout_f;
    bool_t radioTx_lock_f;
    lora_state_e_t state_e;
	timer_str_t lora_tx_timer_str;
	eptk_utils_buffer_str_t radioTxBuff_str;
} lora_data_str_t;

/* Functions declaration */
int8_t app_lora_start();

int8_t app_lora_sleep();

int8_t app_lora_rx_callback(uint8_t lora_rawchar);

/* Variables declaration */
eptk_utils_UART_str_t lora_uart_infos;
timer_str_t lora_sw_wdt_str;
volatile lora_data_str_t app_lora_data_struc = {0};
volatile bool_t lora_trans_f = 0;
volatile bool_t lora_tx_end_f = 0;

/*uint8_t lora_rx_buff[LORA_PRM_SIZE_RX_BUFFER + 1]; // +1 pour le '\0'
eptk_utils_buffer_struc_t lora_rx_buff_struc;

uint8_t lora_tx_buff[LORA_PRM_SIZE_TX_BUFFER];
volatile uint8_t lora_recepIndex = 0;*/

volatile bool_t uart_lora_msgRecep_f = FALSE;
volatile uint8_t lora_rawchar = 0;

//private fns
int8_t app_lora_cmd_push(const char* str_ptr);
int8_t app_lora_cmd_send_next(void);

void app_lora_cmd_buffer_reset(){
	app_lora_data_struc.cmd2send_f = 0;
	app_lora_data_struc.cmd2sendIdx = 0;
	app_lora_data_struc.cmd2pushIdx = 0;
}

int8_t app_lora_cmd_send_next(void){
	//Command buffer
	if(app_lora_data_struc.cmd2send_f){

		if(app_lora_data_struc.state_e == LORA_MODE_SLEEP){
			app_lora_start();
		}

		//Debug
		app_console_log("LoRa -> %s",
				(char*)app_lora_data_struc.cmd_ptr_buff_a[app_lora_data_struc.cmd2sendIdx]);

		eptk_utils_UART_puts(&lora_uart_infos, UART_SEND_NOW, app_lora_data_struc.cmd_ptr_buff_a[app_lora_data_struc.cmd2sendIdx++]);
		app_lora_data_struc.state_e = LORA_MODE_TX;

		/* SW Wdt */
		eptk_utils_sw_timer_start(&lora_sw_wdt_str, LORA_PRM_TX_RTRN_TIMEOUT, SW_TIMER_SINGLE_SHOT, NULL, NULL, FALSE);

		if(app_lora_data_struc.cmd2sendIdx == app_lora_data_struc.cmd2pushIdx){
			app_lora_data_struc.cmd2send_f = 0;
			app_lora_data_struc.cmd2sendIdx = 0;
			app_lora_data_struc.cmd2pushIdx = 0;
			//app_lora_data_struc.radioTx_lock = FALSE;
		}
		return RTRN_OK;
	}
	else{
		return RTRN_EMPTY;
	}
}


int8_t app_lora_cmd_push(const char* str_ptr){
	if(app_lora_data_struc.state_e == LORA_MODE_UNINIT){
		return RTRN_NOT_FOUND;
	}
	if(app_lora_data_struc.cmd2pushIdx < LORA_PRM_CMD_BUFF_SIZE){
		app_lora_data_struc.cmd_ptr_buff_a[app_lora_data_struc.cmd2pushIdx++] = str_ptr;
		app_lora_data_struc.cmd2send_f = TRUE;
		return RTRN_OK;
	}
	else{
		return RTRN_FULL;
	}
}


int8_t app_lora_task(){
	int8_t error = RTRN_SLEEP_OK;
	//Traitement des messages reçus


	if(app_lora_data_struc.msgToRead_f){

		/*if(!strcmp("ok", lora_uart_infos.RX_buffer_struc.head_ptr)){
		}*/
		if(!strcmp("radio_tx_ok", lora_uart_infos.RX_buffer_struc.head_ptr) || lora_sw_wdt_str.isFinished){
			app_lora_data_struc.radioTx_lock_f = FALSE;
			lora_sw_wdt_str.isActive = 0;
		}
		/*else{
			app_lora_data_struc.radioTx_lock = FALSE;
		}*/
		/*else{

		}*/

		if(app_lora_data_struc.state_e == LORA_MODE_RX_WAIT){
			app_lora_data_struc.state_e = LORA_MODE_RX;
		}
		else if(app_lora_data_struc.state_e == LORA_MODE_TX/* && !app_lora_data_struc.radioTx_lock*/){
			app_lora_data_struc.state_e = LORA_MODE_ACTIVE;
		}
		//else if()
		/*else if(app_lora_data_struc.state == LORA_MODE_SLEEP && !app_lora_data_struc.cmd2send_f){
			app_lora_sleep();
		}*/
		//Debug RX
		//app_console_log("LoRa <- %s\r\n",lora_uart_infos.RX_buffer_struc.head_ptr);


		eptk_utils_bufferReset(&lora_uart_infos.RX_buffer_struc);
		app_lora_data_struc.msgToRead_f = FALSE;

		//Machine � �tats
		if( !app_lora_data_struc.radioTx_lock_f &&
				(app_lora_data_struc.state_e == LORA_MODE_ACTIVE
						//|| app_lora_data_struc.state == LORA_MODE_TX
						//|| app_lora_data_struc.state == LORA_MODE_RX
				)){

			if(app_lora_cmd_send_next() == RTRN_EMPTY){ //Go to standby
				app_lora_sleep();
			}
		}
	} //end MsgRead

	else if( app_lora_data_struc.cmd2send_f && 	app_lora_data_struc.state_e == LORA_MODE_SLEEP){
		app_lora_cmd_send_next();
	}

	return error;
}

int8_t app_lora_init(){
	int8_t error = RTRN_OK;

	//UART INIT
	error = eptk_utils_UART_Init(LORA_RX_BASE, LORA_RX_PIN, LORA_TX_PIN, &lora_uart_infos, app_lora_rx_callback,
			USCI_A_UART_CLOCKSOURCE_SMCLK, 27, 2, 0,
			USCI_A_UART_NO_PARITY, USCI_A_UART_LSB_FIRST, USCI_A_UART_ONE_STOP_BIT, USCI_A_UART_MODE, USCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION);
	/*error = eptk_utils_UART_Init(LORA_RX_BASE, LORA_RX_PIN, LORA_TX_PIN, &lora_uart_infos, app_lora_rx_callback,
			USCI_A_UART_CLOCKSOURCE_SMCLK, 26, 1, 0,
			USCI_A_UART_NO_PARITY, USCI_A_UART_LSB_FIRST, USCI_A_UART_ONE_STOP_BIT, USCI_A_UART_MODE, USCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION);
	 */
	if(error < RTRN_OK){
		return error;
	}

	/*	INIT LORA */
	eptk_utils_UART_rx_on(&lora_uart_infos);
	//app_lora_start();

	GPIO_setOutputHighOnPin(LORA_RST_BASE, LORA_RST_PIN);
	GPIO_setAsOutputPin(LORA_RST_BASE, LORA_RST_PIN);

	/*eptk_utils_sw_timer_start(&lora_timer_str_t, 1000, SW_TIMER_SINGLE_SHOT, NULL,NULL, NULL);
	while(!eptk_utils_sw_timer_isFinished(&lora_timer_str_t));
	 */
	app_lora_data_struc.state_e = LORA_MODE_ACTIVE;

	//
	//	//SET RADIO PARAMS
	//app_lora_cmd_push(LORA_CMD_SET_MODE);
	app_lora_cmd_push(LORA_CMD_SET_MODE);
	app_lora_cmd_push(LORA_CMD_MAC_PAUSE);
	app_lora_cmd_push(LORA_CMD_SET_FREQ);
	app_lora_cmd_push(LORA_CMD_SET_BW);
	app_lora_cmd_push(LORA_CMD_SET_SF);
	app_lora_cmd_push(LORA_CMD_SET_PWR);
	app_lora_cmd_push(LORA_CMD_SET_WDT_0);

	app_lora_cmd_push(LORA_CMD_SET_SYNC);
	app_lora_cmd_push(LORA_CMD_SET_CRC_ON);
	app_lora_cmd_push(LORA_CMD_SET_PRLEN);
	app_lora_cmd_push(LORA_CMD_SET_CR);


	//reset du flag
	//app_lora_data_struc.msgToRead_f = FALSE;
	eptk_utils_bufferInit(&app_lora_data_struc.radioTxBuff_str, app_lora_data_struc.radioTxBuff_a, LORA_PRM_SIZE_TX_BUFFER);
	eptk_utils_bufferWrite(&app_lora_data_struc.radioTxBuff_str, "radio tx ", sizeof("radio tx ") - 1 ); //Remove trailing terminator

	return error;
}


int8_t app_lora_start(){

	/*if(lora_state_e != LORA_MODE_SLEEP){
		return RTRN_ERR;
	}*/
	/*Debug*/
	//app_console_log("START\r\n");
	/*
	 * SEND A BREAK TO WAKE UP
	 *
	 */
	//HWREG8(lora_uart_infos.interface + OFS_UCAxCTL1) |= UCTXBRK;
	//HWREG8(lora_uart_infos.interface + OFS_UCAxABCTL) |= UCDELIM1;
	/*lora_uart_infos.tx_end_f = FALSE;
	USCI_A_UART_transmitBreak(lora_uart_infos.interface);
	while(!lora_uart_infos.tx_end_f){
	//	LP_MODE();
	}*/
	GPIO_setOutputHighOnPin(LORA_TX_BASE, LORA_TX_PIN);
	GPIO_setAsOutputPin(LORA_TX_BASE, LORA_TX_PIN);
	GPIO_setOutputLowOnPin(LORA_TX_BASE, LORA_TX_PIN);
	eptk_utils_sw_timer_delay_ms(5);
	GPIO_setOutputHighOnPin(LORA_TX_BASE, LORA_TX_PIN);
	GPIO_setAsPeripheralModuleFunctionOutputPin(LORA_TX_BASE, LORA_TX_PIN);

	/*lora_uart_infos.tx_end_f = FALSE;
	USCI_A_UART_transmitData(lora_uart_infos.interface, 0x55);
	while(!lora_uart_infos.tx_end_f){
		//LP_MODE();
	}*/

	eptk_utils_UART_puts(&lora_uart_infos, UART_SEND_NOW, "U");

	/*while(!app_lora_data_struc.msgToRead_f){
		//LP_MODE();
	}

	app_lora_data_struc.msgToRead_f = FALSE;*/
	//app_lora_cmd_push(LORA_CMD_GET_VERSION);
	//eptk_utils_sw_timer_delay_ms(1000);
	app_lora_data_struc.state_e = LORA_MODE_ACTIVE;
	//app_console_log("LoRa start\r\n");

	return RTRN_OK;
}


int8_t app_lora_sleep(){ //Todo time as runtime param

	/*Debug*/
	//app_console_log("STOP\r\n");

	eptk_utils_UART_puts(&lora_uart_infos, UART_SEND_NORMAL, LORA_CMD_SLEEP);

	app_console_log("LoRa \tsleep\r\n");

	app_lora_data_struc.state_e = LORA_MODE_SLEEP;

	return RTRN_OK;
}

int8_t app_lora_radio_send(uint8_t *str_ptr, uint8_t mode){
	int8_t error = RTRN_OK;

	if(app_lora_data_struc.radioTx_lock_f == TRUE){
		return RTRN_BUSY;
	}

	error = eptk_utils_bufferWrite(&app_lora_data_struc.radioTxBuff_str, str_ptr, strlen((char*)str_ptr));

	if(error != RTRN_OK){
		return error;
	}

	if(mode == UART_SEND_NOW){
		eptk_utils_UART_puts(&lora_uart_infos, UART_SEND_NORMAL, app_lora_data_struc.radioTxBuff_str.begin_ptr);
		error = eptk_utils_bufferWrite(&app_lora_data_struc.radioTxBuff_str, (uint8_t*) "\r\n", sizeof("\r\n"));

		//Push cmd
		app_lora_cmd_push((char *) app_lora_data_struc.radioTxBuff_str.begin_ptr);
		app_lora_data_struc.radioTx_lock_f = TRUE;
		//reset buffer
		eptk_utils_bufferReset(&app_lora_data_struc.radioTxBuff_str);
		eptk_utils_bufferWrite(&app_lora_data_struc.radioTxBuff_str, (uint8_t*) "radio tx ", sizeof("radio tx ") - 1); //Remove 0 terminator
	}

	return error;
}

int8_t app_lora_radio_puts(const uint8_t *str_ptr, uint8_t mode, uint16_t charNb){
	uint8_t i = 0;
	uint8_t sendConvBuffer[LORA_PRM_SIZE_TX_BUFFER * 2 + 1] = {0};

	if(app_lora_data_struc.radioTx_lock_f == TRUE){
		return RTRN_BUSY;
	}
	else if(charNb > LORA_PRM_SIZE_TX_BUFFER){
		app_console_log("LORA\t tx buff FULL\r\n");
		return RTRN_FULL;
	}

	//Convert to ascii bin
	for(i = 0 ; i < charNb ; i++){
		eptk_utils_hx8toAscii(*(str_ptr++), sendConvBuffer + (i*2));
		//snprintf((char*) convBuffer + (i*2),2, "%02X", *(str_ptr++));
	}
	sendConvBuffer[charNb * 2] = 0;
	return app_lora_radio_send(sendConvBuffer, mode);
}

int8_t app_lora_radio_printf(uint8_t mode, const char * fmt, ...){
	uint16_t size;
	uint8_t convBuff[LORA_PRM_SIZE_TX_BUFFER];
	__VALIST args;

	va_start(args, fmt);
	size = vsnprintf(convBuff, EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE, fmt, args);
	va_end(args);

	return app_lora_radio_puts(convBuff, mode, size);
}

int8_t app_lora_rx_callback(uint8_t lora_rawchar){

	int8_t error = RTRN_OK;
	/*eptk_utils_bufferPush(&lora_uart_infos.RX_buffer_struc,lora_rawchar);
	lora_msgToRead_f = TRUE;*/
	//Routine d'interruption RX
	if(lora_rawchar == LORA_PRM_END_CHAR){
		app_lora_data_struc.msgToRead_f = TRUE;
		eptk_utils_bufferPopLast(&lora_uart_infos.RX_buffer_struc);
		if(eptk_utils_bufferPush(&lora_uart_infos.RX_buffer_struc, '\0')
				== RTRN_FULL){
			eptk_utils_bufferReset(&lora_uart_infos.RX_buffer_struc);
		}
	}
	else if(eptk_utils_bufferPush(&lora_uart_infos.RX_buffer_struc, lora_rawchar)
			== RTRN_FULL){
		eptk_utils_bufferReset(&lora_uart_infos.RX_buffer_struc);
	}

	return error;
}


int8_t app_lora_setState(lora_state_e_t newState){
	app_lora_data_struc.state_e = newState;
	return RTRN_OK;
}
