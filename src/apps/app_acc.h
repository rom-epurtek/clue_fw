#ifndef APPS_APP_ACC_H_
#define APPS_APP_ACC_H_

/**
 * \file app_acc.h
 *  \date 03 mar. 2017
 *  \author: RS
 *  \brief Accelerometer ST LIS2DH12 control library.
 *
 */

#include "driverlib.h"
#include "../eptk_utils/eptk_utils.h"

//*****************************************************************************
//! \addtogroup Accelerometer
//! \brief Accelerometer app
//! @{
//*****************************************************************************

#define ACC_CS_LOW() 	while(USCI_B_SPI_isBusy(ACC_SPI_BASE));GPIO_setOutputLowOnPin(ACC_CS_BASE,ACC_CS_PIN)
#define ACC_CS_HIGH() 	GPIO_setOutputHighOnPin(ACC_CS_BASE,ACC_CS_PIN)

#define ACC_FIFO_SIZE	32*6

/**
 * \brief Struct typedef used to store accelerometer data (32B for each axis -> FIFO mode)
 */
typedef struct{
	int8_t Xval[32], Yval[32], Zval[32];
}acc_data_str_t;

/**
 * \brief Main acc data structure typedef
 */
typedef struct{
	bool_t data2log_f; /*!< \brief New data available in accData_str. Must be reset by user */
	bool_t mvtDetected_f; /*!< \brief Movement detected. Must be reset by user */
	acc_data_str_t accData_str; /*!< \brief Accelerometer data stored in aquisition mode */
	uint8_t accTemp;	/*!< \brief Temperature value */
	bool_t pvt_accTempActive_f;
	bool_t pvt_dataReady_f;
}app_acc_data_str_t;

/**
 * \brief Main accelerometer data structure. See typedef above for details
 */
extern volatile app_acc_data_str_t app_acc_data_str;

/**
 * \brief Accelerometer init function
 */
int8_t app_acc_init();

/**
 * \brief Start acc aquisition mode (stops movement detection)
 * Data will be stored in app_acc_data_str.accData_str : 32 values fifo for each axis, then app_acc_data_str.data2log_f is raised
 */
int8_t app_acc_startAquMode(void);
/**
 * \brief Start movement detection mode (stops data aquisition)
 * app_acc_data_str.mvtDetected_f will be set if one axis reaches ACC_MOVT_TRIGGER_VALUE defined in app_conf.h
 */
int8_t app_acc_startMvtDetectMode(void);
/**
 * \brief Task used to handle accelerometer. Should be called in main loop
 */
int8_t app_acc_task();
/**
 * \brief Autotest function : reads ID
 */
int8_t app_acc_autotest();

//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

/* Accelerometer Register Mapping : do not modify */
#define ACC_REG_WHO_AM_I				  0x0F
#define ACC_REG_CTRL_REG1                 0x20
#define ACC_REG_CTRL_REG2                 0x21
#define ACC_REG_CTRL_REG3                 0x22
#define ACC_REG_CTRL_REG4                 0x23
#define ACC_REG_CTRL_REG5                 0x24
#define ACC_REG_CTRL_REG6                 0x25

#define ACC_REG_REFERENCE		          0x26
#define ACC_REG_STATUS                    0x27
#define ACC_REG_OUT_X_L                   0x28
#define ACC_REG_OUT_X_H                   0x29
#define ACC_REG_OUT_Y_L                   0x2A
#define ACC_REG_OUT_Y_H                   0x2B
#define ACC_REG_OUT_Z_L                   0x2C
#define ACC_REG_OUT_Z_H                   0x2D
#define ACC_REG_FIFO_CTRL				  0x2E
#define ACC_REG_FIFO_SRC				  0x2F

#define ACC_REG_INT1_CFG                  0x30
#define ACC_REG_INT1_SRC                  0x31
#define ACC_REG_INT1_THS                  0x32
#define ACC_REG_INT1_DURATION             0x33
#define ACC_REG_INT2_CFG                  0x34
#define ACC_REG_INT2_SRC                  0x35
#define ACC_REG_INT2_THS                  0x36
#define ACC_REG_INT2_DURATION             0x37
#define ACC_REG_CLICK_CFG				  0x38
#define ACC_REG_CLICK_SRC				  0x39
#define ACC_REG_CLICK_THS				  0x3A
#define ACC_REG_TIME_LIMIT				  0x3B
#define ACC_REG_TIME_LATENCY			  0x3C
#define ACC_REG_TIME_WINDOW				  0x3D
#define ACC_REG_ACT_THS					  0x3E
#define ACC_REG_ACT_DUR					  0x3F

#define ACC_REG_TEMP_CFG				  0x1F
#define ACC_REG_TEMP_L					  0x0C
#define ACC_REG_TEMP_H					  0x0D
#define ACC_REG_INT_CNTR				  0x0E

#define BIT(x) ( 1<<(x) )

/* Accelerometer Configuration Defines  */

// accelerometer sensitivity (mg/LSB) with different full scale selection
#define ACC_DEF_Sensitivity_2g     1    /*!< \brief accelerometer sensitivity with 2 g full scale [mg/LSB] */
#define ACC_DEF_Sensitivity_4g     2    /*!< \brief accelerometer sensitivity with 4 g full scale [mg/LSB] */
#define ACC_DEF_Sensitivity_8g     4    /*!< \brief accelerometer sensitivity with 8 g full scale [mg/LSB] */
#define ACC_DEF_Sensitivity_16g    12   /*!< \brief accelerometer sensitivity with 16 g full scale [mg/LSB] */

/* CTRL REG 1 bit definition */
#define ACC_DEF_XEN                             BIT(0)
#define ACC_DEF_YEN                             BIT(1)
#define ACC_DEF_ZEN                             BIT(2)
#define ACC_DEF_XYZEN                           ((uint8_t)0x07)
#define ACC_DEF_Lowpower_En                     BIT(3)
#define ACC_DEF_Power_Down                      ((uint8_t)0x00)
#define ACC_DEF_ODR_1                           ((uint8_t)0x10)
#define ACC_DEF_ODR_10                          ((uint8_t)0x20)
#define ACC_DEF_ODR_25                          ((uint8_t)0x30)
#define ACC_DEF_ODR_50                          ((uint8_t)0x40)
#define ACC_DEF_ODR_100                         ((uint8_t)0x50)
#define ACC_DEF_ODR_200                         ((uint8_t)0x60)
#define ACC_DEF_ODR_400                         ((uint8_t)0x70)
#define ACC_DEF_ODR_LP_1620                     ((uint8_t)0x80)
#define ACC_DEF_ODR_NP_1344                     ((uint8_t)0x90)
#define ACC_DEF_ODR_LP_5376                     ACC_DEF_ODR_NP_1344

/* CTRL REG 2 bit definition */
#define ACC_DEF_HPCLICK_En                      BIT(2)
#define ACC_DEF_Data_Filter_En                  BIT(3)
#define ACC_DEF_All_Filter_Disable              ((uint8_t)0x00)

/* CTRL REG 3 bit definition */
#define ACC_DEF_REG_3_I1_FIFO_OVERRUN				BIT(1)
#define ACC_DEF_REG_3_I1_FIFO_WTM					BIT(2)
/*#define ACC_DEF_REG_3_I1_DRDY2						0//BIT(3)*/
#define ACC_DEF_REG_3_I1_DRDY1						BIT(4)
#define ACC_DEF_REG_3_I1_AOI2						BIT(5)
#define ACC_DEF_REG_3_I1_AOI1						BIT(6)
#define ACC_DEF_REG_3_I1_CLICK						BIT(7)

/* CTRL REG 4 bit definition */
#define ACC_DEF_SPI_3Wire_En                    BIT(0)
#define ACC_DEF_REG_4_SELF_TEST0				BIT(1)
#define ACC_DEF_REG_4_SELF_TEST1				BIT(2)
#define ACC_DEF_HighRes_Out_Mode                BIT(3)      //Needed for NORMAL operating mode
#define ACC_DEF_FS_2g                           0
#define ACC_DEF_FS_4g                           BIT(4)
#define ACC_DEF_FS_8g                   		BIT(5)
#define ACC_DEF_FS_16g                   		(BIT(4) + BIT(5))
#define ACC_DEF_Big_Endian_En                   BIT(6)      //Default Little Endian
#define ACC_DEF_BDU_Single                      BIT(7)      //Default Continuous update

/* CTRL REG 5 bit definition */
#define ACC_DEF_4D_Detect_onINT2                BIT(0)
#define ACC_DEF_Latch_INT2                      BIT(1)
#define ACC_DEF_4D_Detect_onINT1                BIT(2)
#define ACC_DEF_Latch_INT1                      BIT(3)
#define ACC_DEF_FIFO_En                         BIT(6)
#define ACC_DEF_Reboot_Mem                      BIT(7)

/* CTRL REG 6 bit definition */
#define ACC_DEF_REG6_Click_onPAD2                    BIT(7)
#define ACC_DEF_REG6_INT1_onPAD2                     BIT(6)
#define ACC_DEF_REG6_INT2_onPAD2                     BIT(5)
#define ACC_DEF_REG6_BOOT_onPAD2                     BIT(4)
#define ACC_DEF_REG6_PAD2_Active                     BIT(3)
#define ACC_DEF_REG6_Int_Active_Low                  BIT(1)

/* STATUS REG bit definition */
#define ACC_DEF_XYZ_OVR                         BIT(7)
#define ACC_DEF_Z_OVR                           BIT(6)
#define ACC_DEF_Y_OVR                           BIT(5)
#define ACC_DEF_X_OVR                           BIT(4)
#define ACC_DEF_XYZ_NewData_Avail               BIT(3)
#define ACC_DEF_Z_NewData_Avail                 BIT(2)
#define ACC_DEF_Y_NewData_Avail                 BIT(1)
#define ACC_DEF_X_NewData_Avail                 BIT(0)

/* FIFO CTRL REG bit definition */
#define ACC_DEF_Trigger_Select                  BIT(5)
#define ACC_DEF_FIFO_Bypass                     0x00
#define ACC_DEF_FIFO_Mode                       0x40
#define ACC_DEF_Stream_Mode                     0x80
#define ACC_DEF_Trigger_Mode                    0xC0

/* FIFO SRC REG bit definitions */

/* INT1 & INT2 Config bit definition */
#define ACC_DEF_Int_AND_OR_En                   BIT(7)
#define ACC_DEF_6D_Detect_En                    BIT(6)
#define ACC_DEF_Z_High_Evt_Int_En               BIT(5)
#define ACC_DEF_Z_Low_Evt_Int_En                BIT(4)
#define ACC_DEF_Y_High_Evt_Int_En               BIT(3)
#define ACC_DEF_Y_Low_Evt_Int_En                BIT(2)
#define ACC_DEF_X_High_Evt_Int_En               BIT(1)
#define ACC_DEF_X_Low_Evt_Int_En                BIT(0)

/* INT1 & INT2 SRC bit definitiosn  */
#define ACC_DEF_AND_EN		                    BIT(6)
#define ACC_DEF_6D_EN		                    BIT(6)
#define ACC_DEF_Z_High_Evt                      BIT(5)
#define ACC_DEF_Z_Low_Evt                       BIT(4)
#define ACC_DEF_Y_High_Evt                      BIT(3)
#define ACC_DEF_Y_Low_Evt                       BIT(2)
#define ACC_DEF_X_High_Evt                      BIT(1)
#define ACC_DEF_X_Low_Evt                       BIT(0)


#define ACC_DEF_FilterMode_Normal               ((uint8_t)0x00)
#define ACC_DEFH_FilterMode_Reference           ((uint8_t)0x20)

#define ACC_DEF_Filter_Enable                   ((uint8_t)0x10)
#define ACC_DEF_Filter_Disable                  ((uint8_t)0x00)

#define ACC_DEF_Filter_HPc8                     ((uint8_t)0x00)
#define ACC_DEF_Filter_HPc16                    ((uint8_t)0x01)
#define ACC_DEF_Filter_HPc32                    ((uint8_t)0x02)
#define ACC_DEF_Filter_HPc64 ((uint8_t)0x03)

#endif /* APPS_APP_ACC_H_ */
