/*
 * app_alim.c
 *
 *  Created on: 6 janv. 2017
 *      Author: romain
 */


#include "driverlib.h"
#include "app_alim.h"
#include "app_sd_log.h"
#include "../eptk_utils/uart.h"
#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include <stdio.h>
#include <stdarg.h>

#include "app_console.h"
#include "../conf/app_conf.h"

/* Variable alims declaration */

//GLOBALS
app_alim_state_str_t app_alim_data_str = {0};
//LOCALS
volatile bool_t app_alim_todo_f = TRUE;
volatile timer_str_t app_alim_update_timer_str = {0};
bool_t app_alim_adcLvlRdy;

uint8_t timeTxBuff[15] = {0};

int8_t app_alim_init(){

	int8_t error = RTRN_OK;

	GPIO_INPUT_PULLDOWN(FTDI_nPWREN);
	GPIO_INPUT_PULLUP(ALIM_SIG_nPG);
	GPIO_INPUT_PULLUP(ALIM_SIG_STAT1);
	GPIO_INPUT_PULLUP(ALIM_SIG_STAT2);
	GPIO_setAsPeripheralModuleFunctionInputPin(ALIM_BATT);

	GPIO_HIGH(ALIM_SIG_PROG2);
	GPIO_OUTPUT(ALIM_SIG_PROG2);

	app_alim_data_str.battCharge_mV = 4000;
	app_alim_todo_f = TRUE;
	app_alim_task(); //Update status

	//Debug
	error = eptk_utils_sw_timer_start(&app_alim_update_timer_str, APP_ALIM_REFRESH_TIME, SW_TIMER_CONTINUOUS,
			NULL, &app_alim_todo_f, TRUE);

	/*	INIT Interruptions */
	//TODO

	return error;
}

int8_t app_alim_task(void){

	if(app_alim_todo_f){
		app_alim_todo_f = FALSE;

		//Removed to ensure 200mA charge
		// FTDI_VBUS_Sense -> 1 si VUUSB OK
		if(FTDI_BATT_CHG_PxIN & FTDI_BATT_CHG_PIN){
			//ALIM_SIG_PROG2_PxOUT &= ~ALIM_SIG_PROG2_PIN; //Inv
			app_alim_data_str.usbPW = TRUE;
			//DEBUG_LED_OFF(LED_GREEN);
		}
		else{
			app_alim_data_str.usbPW = FALSE;
			//DEBUG_LED_ON(LED_GREEN);
			//ALIM_SIG_PROG2_PxOUT |= ALIM_SIG_PROG2_PIN; //Inv
		}


		// nPG PW Supply if PG low (see datasheet MCP73871 p23)
		if(! GPIO_GET(ALIM_SIG_nPG)){
			app_alim_data_str.pwGood_f = TRUE;
			if( GPIO_GET(ALIM_SIG_STAT2) ){	//Not in charge
				if( GPIO_GET(ALIM_SIG_STAT1) ){ //Batt discon
					app_alim_data_str.battState_e = BATT_DISCONNECTED;
				}
				else{
					app_alim_data_str.battState_e = BATT_CHARGING;
				}
			}
			else{
				if( GPIO_GET(ALIM_SIG_STAT1) ){
					app_alim_data_str.battState_e = BATT_CHARGE_COMPLETE;
				}
				else{
					app_alim_data_str.battState_e = BATT_FAULT;
				}
			}
		}
		else{
			app_alim_data_str.pwGood_f = FALSE;
			// STAT 1 = 0 if low batt
			if(GPIO_GET(ALIM_SIG_STAT1)){
				app_alim_data_str.battState_e = BATT_IDLE;
			}
			else{
				app_alim_data_str.battState_e = BATT_LOW;
			}
		}
		app_alim_adcLvlRdy = FALSE;
		eptk_utils_adc_startConversion(ALIM_ADC_INPUT, &app_alim_adcLvlRdy);
	}

	else if(app_alim_adcLvlRdy){
		/* Conversion to mV : * 2 (divider before measure) * 3300 (voltage ref) / 4096 (adc full scale)
		 * = 6600 / 4096 = 825 / 512
		 */
		app_alim_data_str.newBattLvl_f = TRUE;

		app_alim_data_str.battCharge_mV = (((uint32_t)ADC12_A_getResults(ADC12_A_BASE, ALIM_ADC_INPUT))*825)/512;
		app_alim_adcLvlRdy = FALSE;

		if(app_alim_data_str.battCharge_mV < ALIM_BATT_0PRCT){
		    app_alim_data_str.battCharge_percent = 0;
		    //TODO Stop immediately
		}
		else if(app_alim_data_str.battCharge_mV < ALIM_BATT_5PRCT){
            app_alim_data_str.battCharge_percent = 0;
            //TODO Initiate stop
		}
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_10PRCT){
            app_alim_data_str.battCharge_percent = 5;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_20PRCT){
            app_alim_data_str.battCharge_percent = 10;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_30PRCT){
            app_alim_data_str.battCharge_percent = 20;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_40PRCT){
            app_alim_data_str.battCharge_percent = 30;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_50PRCT){
            app_alim_data_str.battCharge_percent = 40;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_60PRCT){
            app_alim_data_str.battCharge_percent = 50;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_70PRCT){
            app_alim_data_str.battCharge_percent = 60;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_80PRCT){
            app_alim_data_str.battCharge_percent = 70;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_90PRCT){
            app_alim_data_str.battCharge_percent = 80;
        }
        else if(app_alim_data_str.battCharge_mV < ALIM_BATT_100PRCT){
            app_alim_data_str.battCharge_percent = 90;
        }
        else if(app_alim_data_str.battCharge_mV >= ALIM_BATT_100PRCT){
            app_alim_data_str.battCharge_percent = 100;
        }

		//app_console_log("BATT\tPG : %d***\r\n", app_alim_data_str.pwSupply_f);
		//app_sd_log(SD_LOG_WRITE_WHEN_FULL,"BATT\t%u\tmV\tState:%d\tPW:%d\r\n",
				//app_alim_data_str.battCharge_mV, app_alim_data_str.battState_e, app_alim_data_str.pwSupply_f);
	}
	return RTRN_SLEEP_OK;
}

