/**
 * \file app_console.h
 *
 *  \date 19 juil. 2016
 *  \author romain
 */

#ifndef APPS_APP_CONSOLE_H_
#define APPS_APP_CONSOLE_H_
//*****************************************************************************
//! \addtogroup Console_log
//! \brief Console application
//! @{
//*****************************************************************************

#include <driverlib.h>
#include "../eptk_utils/cmdline.h"
#include "../eptk_utils/eptk_utils.h"

#define APP_CONSOLE_TX_BUFF_SIZE 128
#define APP_CONSOLE_RX_BUFF_SIZE 64
#define APP_CONSOLE_END_CHAR 	'\r'
#define APP_CONSOLE_BS_CHAR  	'\b'
#define APP_CONSOLE_DEL_CHAR 	127u

extern volatile bool_t app_console_todo_f;

int8_t app_console_init(void);
int8_t app_console_task(void);


/** \brief Send a message to the console interfaces (usb and BLE)
 * \param *fmt : string to be sent
 */
void app_console_log(const char *fmt, ...);

typedef struct{
	eptk_utils_UART_str_t* uart_str_ptr;
	bool_t rx_f;
	bool_t log_active_f;
}app_console_str_t;

/**
 * \name CONFIG
 */
/**
 * \brief This table holds the command names, implementing functions,and brief description.
 */
extern tCmdLineEntry g_sCmdTable[];

/**
 * \brief Declaration of uart communication interfaces : usb and bluetooth
 */
extern app_console_str_t app_console_struct_a[];

//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

#endif /* APPS_APP_CONSOLE_H_ */
