
/**
 * \file app_gps.h
 *	\date 02 feb. 2016
 *  \author: RS
 *  \brief GPS localization system
 *  Includes GPS data structure containing all relevant navigation data GPS and configuration parameters.
 *  Includes GPS set ON and set OFF function.
 *
 */

#ifndef APPS_APP_GPS_H_
#define APPS_APP_GPS_H_

/**
 * \addtogroup GPS
 * \brief GPS Application
 * @{
 */

#include "driverlib.h"
#include "../eptk_utils/eptk_utils.h"

#define GPS_RX_BUFF_LEN	512
#define GPS_TX_BUFF_LEN	100
#define GPS_PULSE_MS	200
#define GPS_WAKEUP_TIMEOUT_MS 1500

#define GPS_MODE_NMEA			1
#define GPS_MODE_NMEA_MSG		"8102010100010101050101010001000100000001000012C0"

#define OSP_IDX_PAYLOAD_SIZE_END 1
#define OSP_IDX_ID_CODE			 2

#define GPS_START_STEP0				0
#define GPS_START_STEP1				1
#define GPS_START_STEP2				2
#define GPS_START_STEP3				3
#define GPS_START_STEP4				4
#define GPS_START_STEP5				5
#define GPS_START_REC				6
#define GPS_MSG_MAX_LENGTH			300

#define GPS_MODE_OSP			2
#define GPS_MODE_OSP_MSG		"$PSRF100,0,115200,8,1,0*04\r\n"
#define GPS_MODE_OSP_MSG_LEN	sizeof(GPS_MODE_OSP_MSG)

#define GPS_OSP_MODE_DSBL_MSGS	"A602000000000000"
#define GPS_OSP_MODE_MPM		"DA0200000000"//DA0200000000"
#define GPS_OSP_MODE_NORMAL		"DA00"//DA0200000000"
#define GPS_OSP_POLL_PW_MODE	"E90B"
#define GPS_OSP_POLL_CLK_STAT	"9000"
#define GPS_OSP_SET_MODE_DEGRAD	"8800000301000000000000050201" //MID 136 :Alt Constraining = Yes, Degraded Mode = clock then direction
															   //Altitude = 0, Alt Hold Mode = Auto, Alt Source = Last Computed,
																//Degraded Time Out = 5, DR Time Out = 2, Track Smoothing = Yes

#define GPS_OSP_INIT_DATA_SOURCE "80004690920001C7610042C798000177190000000000000C01"	//"80FFD700F9FFBE5266003AC57A000124F80083D600039C0C33"
//XECEF 4624530d = 00469092
//YECEF 116577d = 0001C761
//ZECEF 4376472d = 0042C798
//CLKDrift 96025Hz = 00017719
/*A0 A2 00 19 80 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 0C 00
00 8C B0 B3 */

#define GPS_NMEA_STANDBY		"$PMTK161,0*28\r\n"
/*debug*/
#define GPS_INIT_RCV_BUFF_NB

#define GPS_PAYLOAD_SIZE 			2

#define GPS_ID_2_NAV_ECEF			2
#define GPS_ID_2_IDX_MODE1 			(19 + GPS_PAYLOAD_SIZE)
#define GPS_ID_2_PMODE_MASK 		0x07

#define GPS_ID_7_RESP_POLL_CLK		7
#define GPS_ID_7_IDX_CLK_DRFT_LSB 	(11 + GPS_PAYLOAD_SIZE)

#define GPS_ID_11_ACK			11

#define GPS_ID_12_NACK			12

#define GPS_ID_18_OKTOSEND		18

#define GPS_ID_41_NAV_DEG		41
#define GPS_ID_41_IDX_VALID_MSG (1 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_VALID_NAV	(4 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_YEAR		(11 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_MONTH		(13 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_DAY		(14 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_HOUR		(15 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_MIN		(16 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_SEC		(17 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_LAT_MSB 	(23 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_LAT_LSB 	(26 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_LONG_MSB 	(27 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_LONG_LSB 	(30 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_ALT_MSB 	(35 + GPS_PAYLOAD_SIZE)
#define GPS_ID_41_IDX_ALT_LSB 	(38 + GPS_PAYLOAD_SIZE)

#define GPS_ID_65_GPIO			65

/** \brief Possible states of OSP message reception state machine
 */
typedef enum {
	GPS_STATE_UNINIT =0,
	GPS_STATE_OFF,
	GPS_STATE_IDLE,
	GPS_STATE_PRE_RCVNG,
	GPS_STATE_RCVNG,
	GPS_STATE_DUMP,
}gps_state_e_t;

/** \brief Structure containing OSP message data for proper reception
 */
typedef struct{
	uint16_t payloadLength;
	uint16_t CRC;
	uint8_t	 msgID;
}OSPMsg_str_t;

/** \brief Structure containing navigation data
 */
typedef struct {
	eptk_utils_buffer_str_t pvt_rx_buff_str;
	eptk_utils_buffer_str_t pvt_tx_buff_str;
	uint8_t pvt_rxBuff_a[GPS_RX_BUFF_LEN];
	uint8_t pvt_txBuff_a[GPS_TX_BUFF_LEN];
	uint8_t SV;
	gps_state_e_t pvt_OSPstate_e;
	OSPMsg_str_t pvt_OSPMsg_str;
	bool_t pvt_okToSend_f;
	bool_t navValid_f; /*!< \brief \brief Navigation data is valid. Read only*/
	bool_t timeValid_f;/*!< \brief Time data is valid. Read only*/
	bool_t toDoUpdateRTC_f; /*!< \brief If set, RTC will be updated on next time valid GPS message. Write only*/
	bool_t newData2log_f; /*!< \brief New navigation data available */
	int32_t posLat; /*!< \brief Latitude */
	int32_t posLong; /*!< \brief Longitude */
	int32_t posAlt; /*!< \brief Altitude */
} app_gps_str_t;

/** \brief GPS navigation data. It is an extern variable and is made to be used to access navigation data.
 */
extern app_gps_str_t app_gps_data_str;

/** \brief Set ON GPS
 * \return ERROR CODE :
 * 	\li RTRN_OK			-1, GPS has been powered on normally
 * 	\li RTRN_ERR		-2, after timeout expired, GPS wake up pin didn't go HIGH after sending ON sequence
 * 	\li RTRN_NOT_FOUND	-3, GPS was already awaken
 */
int8_t app_gps_setON(void);


/** \brief Set OFF GPS
 * \return ERROR CODE :
 * 	\li RTRN_OK			-1, GPS has been powered off normally
 * 	\li RTRN_ERR		-2, after timeout expired, GPS wake up pin didn't go LOW after sending OFF sequence
 * 	\li RTRN_NOT_FOUND	-3, GPS was already off
 */
int8_t app_gps_setOFF(void);


/** \brief Set GPS Low Power Mode / Normal mode
 * \param isOn : boolean
 * \li TRUE : go to low power mode
 * \li FALSE : exit low power mode
 */
int8_t app_gps_setLPM(bool_t isOn);

/** \brief Initialize GPS buffer, data structure and GPIOs. It is needed to call app_gps_setON after the initialization to set on the GPS.
 * \return ERROR CODE :
 * 	\li RTRN_OK			-1, successful initialization
 * 	\li RTRN_NOT_FOUND	-3, after timeout expired, GPS wake up pin didn't go HIGH after sending ON sequence
 */
int8_t app_gps_init(void);

/** \brief Periodic task called every second to deal with received GPS messages and send message in the tx buffer. Update navigation data.
 * \return ERROR CODE :
 * 	\li RTRN_OK			-1
 */
int8_t app_gps_task(void);

/*Outputs*/
#define GPS_CS		GPS_CS_BASE, GPS_CS_PIN
#define GPS_RESET	GPS_RST_BASE,GPS_RST_PIN
#define GPS_ON_OFF	GPS_ON_OFF_BASE,GPS_ON_OFF_PIN

/*Input*/
#define GPS_WAKEUP	GPS_WAKEUP_BASE,GPS_WAKEUP_PIN

/**
 * @}
 */

#endif /* APPS_APP_GPS_H_ */
