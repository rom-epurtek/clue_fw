#include <driverlib.h>
#include "../conf/app_conf.h"
#include "../conf/bsp.h"
#include "../eptk_utils/eptk_utils.h"

/*HEARTBEAT*/
timer_str_t heartBeat_timer_struc = {0};
volatile bool_t heartBeat_toDo_f = FALSE;
volatile uint32_t app_heartbeatMode = HEARTBEAT_TIME_OFF_LONG;

#ifdef USE_HEARTBEAT
void heartBeatTimer_task(){
	static uint32_t HTBT_time;
	if(heartBeat_toDo_f){
		if(DEBUG_LED_IS_ON(LED_BLUE)){
			DEBUG_LED_OFF(LED_BLUE);
			HTBT_time = app_heartbeatMode;
		}
		else{
			HTBT_time = HEARTBEAT_TIME_ON;
			DEBUG_LED_ON(LED_BLUE);
		}

		heartBeat_toDo_f = FALSE;
		eptk_utils_sw_timer_start(&heartBeat_timer_struc, HTBT_time , SW_TIMER_CONTINUOUS , NULL,(bool_t*) &heartBeat_toDo_f, TRUE);
	}
}
#endif
