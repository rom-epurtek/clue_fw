/*
 * app_acc.c
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */

#include "driverlib.h"
#include "app_acc.h"
#include "math.h"
#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include "../hal/hal_spi_b.h"
#include "../conf/bsp.h"
#include "../conf/app_conf.h"


//GLOBALS
volatile bool_t app_acc_todo_f = FALSE;
volatile app_acc_data_str_t app_acc_data_str = {0};
timer_str_t acc_read_timer_struc = {0};

uint8_t RXData = 0;
uint8_t TXData = 0;
int8_t fifoBuffer[ACC_FIFO_SIZE] = {0};

void app_acc_write(uint8_t addr, uint8_t val){
	WAIT_WHILE_HAL_SPI_IS_BUSY(ACC_SPI_BASE);
	hal_spi_b_init(ACC_FREQ, HAL_SPI_B_PHASE_1,HAL_SPI_B_POL_1, ACC_SPI_BASE);
	ACC_CS_LOW();
	hal_spi_b_sendByte(addr & ~0xC0, ACC_SPI_BASE);
	hal_spi_b_sendByte(val, ACC_SPI_BASE);
	ACC_CS_HIGH();
}

/* Clear Ref fn */
void app_acc_clearRef(void){
	app_acc_write(ACC_REG_REFERENCE, 0);
}

/*INT 1 ISR*/
void app_acc_dataReady_isr(void){
	app_acc_data_str.pvt_dataReady_f = TRUE;
	app_acc_todo_f = TRUE;
}
/*INT 2 ISR*/
void app_acc_mvtDetected_isr(void){
	//app_acc_todo_f = TRUE;
	app_acc_data_str.mvtDetected_f = TRUE;
}

void app_acc_read(uint8_t startAddr, uint8_t len, uint8_t * rxbuff){
	WAIT_WHILE_HAL_SPI_IS_BUSY(ACC_SPI_BASE);
	hal_spi_b_init(ACC_FREQ, HAL_SPI_B_PHASE_1,HAL_SPI_B_POL_1, ACC_SPI_BASE);
	ACC_CS_LOW();
	hal_spi_b_sendByte(startAddr | 0xC0, ACC_SPI_BASE);
	hal_spi_b_readFrame(rxbuff,len, ACC_SPI_BASE);
	ACC_CS_HIGH();
}

int8_t app_acc_autotest() {
	//Check ID
	app_acc_read(ACC_REG_WHO_AM_I, 1, &RXData);

	if(RXData == 0x33){
		return RTRN_OK;
	}
	else{
		return RTRN_ERR;
	}
}

void app_acc_clearMvtDetected(void){

	/* MVT DETECTED */
	if(app_acc_data_str.mvtDetected_f){

		/* Read source to clear INT 2 ir	(voir p32 33) */
		app_acc_read(ACC_REG_INT2_SRC, 1, (uint8_t *) &RXData);

		//DEBUG
		app_acc_data_str.mvtDetected_f = FALSE;
	}
}

int8_t app_acc_startAquMode(void){
	//FIFO MODE STREAM
	//Int source data ready on INT1
	app_acc_write(ACC_REG_CTRL_REG3, ACC_DEF_REG_3_I1_FIFO_OVERRUN);
	app_acc_write(ACC_REG_CTRL_REG5, ACC_DEF_FIFO_En); //Enable FIFO
	app_acc_write(ACC_REG_FIFO_CTRL, 0x80); //Stream mode

    app_acc_write(ACC_REG_CTRL_REG1, ACC_DEF_XYZEN + ACC_MOVING_LPM + ACC_MOVING_FREQ_MASK);

	//Set INT2 (movement detection) threshold value
	app_acc_write(ACC_REG_INT2_THS, ACC_MOVT_TRIGGER_VALUE); // 7bits
	//Set INT2 duration value
	app_acc_write(ACC_REG_INT2_DURATION, ACC_MOVT_TRIGGER_DURATION); // 7bits ADC

	/* Clear and enable irq*/
	GPIO_clearInterrupt(ACC_INT1_BASE, ACC_INT1_PIN); //Data Ready
	GPIO_enableInterrupt(ACC_INT1_BASE, ACC_INT1_PIN);

	app_acc_data_str.pvt_dataReady_f = FALSE;

	/* Dummy data read to clear INT 2 */
	app_acc_read(ACC_REG_OUT_X_L, 6, (uint8_t *) &app_acc_data_str.accData_str);
	return RTRN_OK;
}

int8_t app_acc_startMvtDetectMode(void){
	//Low power 10Hz //Todo set parameter in app.h (ACC_MOVING_FREQ)
	app_acc_write(ACC_REG_CTRL_REG1, ACC_DEF_XYZEN + ACC_HALTED_LPM + ACC_HALTED_FREQ_MASK);

	//Set INT2 threshold value
	app_acc_write(ACC_REG_INT2_THS, ACC_HALTED_TRIGGER_VALUE); // 7bits
	//Set INT2 duration value
	app_acc_write(ACC_REG_INT2_DURATION, ACC_HALTED_TRIGGER_DURATION); // 7bits ADC

	//Enable MVT interrupt
	GPIO_clearInterrupt(ACC_INT2_BASE, ACC_INT2_PIN);
	GPIO_enableInterrupt(ACC_INT2_BASE, ACC_INT2_PIN);

	/* Read source to clear INT 2 ir	(voir p32 33) */
	//app_acc_read(ACC_REG_INT2_SRC, 1, (uint8_t *) &RXData);

	//Disable dataready INT1
	app_acc_data_str.pvt_dataReady_f = 0;
	GPIO_disableInterrupt(ACC_INT1_BASE, ACC_INT1_PIN);

	return RTRN_OK;
}

int8_t app_acc_init(){

	GPIO_setOutputHighOnPin(ACC_CS_BASE,ACC_CS_PIN);
	GPIO_setAsOutputPin(ACC_CS_BASE,ACC_CS_PIN);

	// Configure acc interrupt INT2 (dataReady) on P2.4
	GPIO_selectInterruptEdge(ACC_INT1_BASE, ACC_INT1_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
	GPIO_setAsInputPin(ACC_INT1_BASE,ACC_INT1_PIN);
	eptk_utils_gpio_setIsr(ACC_INT1_BASE, ACC_INT1_PIN, app_acc_dataReady_isr);

	// Configure acc interrupt INT1 (movement detected) on P4.7
	GPIO_selectInterruptEdge(ACC_INT2_BASE, ACC_INT2_PIN, GPIO_LOW_TO_HIGH_TRANSITION);
	GPIO_setAsInputPin(ACC_INT2_BASE,ACC_INT2_PIN);
	eptk_utils_gpio_setIsr(ACC_INT2_BASE, ACC_INT2_PIN, app_acc_mvtDetected_isr);

	//AUTOTEST : check ID
	 if(app_acc_autotest() != RTRN_OK){
		 return RTRN_ERR;
	 }

	 /* Set control registers to default values  */
	app_acc_write(ACC_REG_CTRL_REG1, 0);
	app_acc_write(ACC_REG_CTRL_REG2, 0);
	app_acc_write(ACC_REG_CTRL_REG3, 0);
	app_acc_write(ACC_REG_CTRL_REG4, 0);
	app_acc_write(ACC_REG_CTRL_REG5, 0);
	app_acc_write(ACC_REG_CTRL_REG6, 0);

	//Int source data ready on INT1
	app_acc_write(ACC_REG_CTRL_REG3, ACC_DEF_REG_3_I1_DRDY1);//0x26); //p27

	//Block data update
	app_acc_write(ACC_REG_CTRL_REG4, ACC_DEF_BDU_Single);

	//ACC Interrupts
	app_acc_write(ACC_REG_CTRL_REG6, ACC_DEF_REG6_INT2_onPAD2 + ACC_DEF_REG6_Int_Active_Low);

	//Set INT2 threshold value
	app_acc_write(ACC_REG_INT2_THS, ACC_MOVT_TRIGGER_VALUE); // 7bits
	//Set INT2 duration value
	app_acc_write(ACC_REG_INT2_DURATION, ACC_MOVT_TRIGGER_DURATION); // 7bits ADC
	//SET INT2 6D mvmt ISR
	app_acc_write(ACC_REG_INT2_CFG, ACC_DEF_X_High_Evt_Int_En | ACC_DEF_Y_High_Evt_Int_En | ACC_DEF_Z_High_Evt_Int_En);//0x40);//6A p32

	//DEBUG
	app_acc_read(ACC_REG_OUT_X_L, 6, (uint8_t *) &app_acc_data_str.accData_str); //Dummy read to clear IRQ

	return RTRN_OK;
}

int8_t app_acc_task(){
	volatile uint8_t sampleCnt = 0, fifoBuffIdx = 0;

	if(app_acc_data_str.pvt_dataReady_f){
		//DEBUG
		DEBUG_LED_ON(LED_ORANGE);

		//Reset flag
		app_acc_data_str.pvt_dataReady_f = FALSE;

		//FIFO read
		app_acc_read(ACC_REG_OUT_X_L, ACC_FIFO_SIZE, (uint8_t *) fifoBuffer);

		while( fifoBuffIdx < ACC_FIFO_SIZE){
				app_acc_data_str.accData_str.Xval[sampleCnt] = fifoBuffer[fifoBuffIdx + 1];
				app_acc_data_str.accData_str.Yval[sampleCnt] = fifoBuffer[fifoBuffIdx + 3];
				app_acc_data_str.accData_str.Zval[sampleCnt] = fifoBuffer[fifoBuffIdx + 5];
				fifoBuffIdx+=6;
				sampleCnt++;
		}

		//DEBUG
		DEBUG_LED_OFF(LED_ORANGE);

		app_acc_data_str.data2log_f = TRUE;
	}

	return RTRN_SLEEP_OK;
}




