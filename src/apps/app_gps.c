/*
 * app_gps.c
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */

#include "driverlib.h"
#include "app_gps.h"
#include "app_console.h"

#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include "../hal/hal_spi_b.h"
#include "../conf/bsp.h"
#include "../conf/app_conf.h"

#include <string.h>

app_gps_str_t app_gps_data_str = {0};

timer_str_t	app_gps_delay_str, app_gps_wakeup_str;
volatile bool_t app_gps_timeout_f = FALSE, app_gps_todo_f = FALSE;

/*DEBUG*/
uint8_t RxChar;
bool_t rcvng = FALSE;
volatile bool_t task_rcvng = FALSE;

/*Pvt fn*/
int8_t app_gps_sendOspMsg(const char* hexStr);

void app_gps_wakeup_isr(void){
	/*if(P2IN & GPS_WAKEUP_PIN){
		P4OUT &= ~LED_1_BIT;
		P2IES |= GPS_WAKEUP_PIN;//GPIO_selectInterruptEdge(GPS_WAKEUP_BASE,GPS_WAKEUP_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
	}
	else{
		P4OUT |= LED_1_BIT;
		P2IES &= ~GPS_WAKEUP_PIN	;	//GPIO_selectInterruptEdge(GPS_WAKEUP_BASE,GPS_WAKEUP_PIN,GPIO_LOW_TO_HIGH_TRANSITION);
	}*/
}

void app_gps_sendOnPulse(void){
	GPIO_HIGH(GPS_ON_OFF);
	eptk_utils_sw_timer_delay_ms(GPS_PULSE_MS);
	GPIO_LOW(GPS_ON_OFF);
}

int8_t app_gps_setON(void){
	int8_t errorCode = RTRN_OK;
	if(app_gps_data_str.pvt_OSPstate_e == GPS_STATE_UNINIT){
		errorCode = app_gps_init();
			if(errorCode != RTRN_OK){
				app_console_log("GPS INIT Err %d",errorCode);
				return errorCode;
			}
	}

	//Si d�j� allum�
	if(GPIO_GET(GPS_WAKEUP)){
		if(app_gps_data_str.pvt_OSPstate_e == GPS_STATE_OFF){
			app_gps_data_str.pvt_OSPstate_e = GPS_STATE_IDLE;
			eptk_utils_sw_timer_start(&app_gps_delay_str, 1000, SW_TIMER_CONTINUOUS, NULL, &app_gps_todo_f, TRUE);
		}
		return RTRN_NOT_FOUND;
	}

	app_gps_sendOnPulse();

	/* Wait for WAKEUP with timeout */
	app_gps_timeout_f = FALSE;
	eptk_utils_sw_timer_start(&app_gps_delay_str,
			GPS_WAKEUP_TIMEOUT_MS,
			SW_TIMER_SINGLE_SHOT,
			NULL,
			&app_gps_timeout_f, TRUE);

	while(!GPIO_GET(GPS_WAKEUP) && !app_gps_timeout_f){
		LP_MODE();
	}
	eptk_utils_sw_timer_disable(&app_gps_delay_str);

	/*Timeout*/
	if(app_gps_timeout_f){
		app_gps_data_str.pvt_OSPstate_e = GPS_STATE_OFF;
		return RTRN_ERR;
	}
	else{
		app_gps_data_str.pvt_OSPstate_e = GPS_STATE_IDLE;
		app_gps_timeout_f = FALSE;
		app_gps_sendOspMsg(GPS_OSP_SET_MODE_DEGRAD);
		eptk_utils_sw_timer_start(&app_gps_wakeup_str, 1000, SW_TIMER_CONTINUOUS, NULL, &app_gps_todo_f, TRUE);
		return RTRN_OK;
	}
}

int8_t app_gps_setOFF(void){
	uint8_t rcvdChar = 0, lastChar = 0;

	//Si GPS d�j� �teint
	if(!GPIO_GET(GPS_WAKEUP)){
		app_gps_data_str.pvt_OSPstate_e = GPS_STATE_OFF;
		eptk_utils_sw_timer_disable(&app_gps_delay_str);
		return RTRN_NOT_FOUND;
	}
	app_gps_sendOnPulse();

	/*Empty buffer*/
	/*Todo init SPI avec params*/
	GPIO_LOW(GPS_CS);

	while(!(rcvdChar == 0xA7 && lastChar == 0xB4)){
		lastChar = rcvdChar;
		rcvdChar = hal_spi_b_readByte(GPS_SPI_BASE);
	}
	GPIO_HIGH(GPS_CS);

	//app_gps_sendOnPulse();

	/* Wait for WAKEUP with timeout */
	app_gps_timeout_f = FALSE;
	eptk_utils_sw_timer_start(&app_gps_delay_str,
			GPS_WAKEUP_TIMEOUT_MS,
			SW_TIMER_SINGLE_SHOT,
			NULL,
			&app_gps_timeout_f, TRUE);

	while(GPIO_GET(GPS_WAKEUP) && !app_gps_timeout_f){
		LP_MODE();
	}
	eptk_utils_sw_timer_disable(&app_gps_delay_str);

	/*Timeout*/
	if(app_gps_timeout_f){
		return RTRN_ERR;
	}
	else{
		return RTRN_OK;
	}
}

int8_t app_gps_setLPM(bool_t isLPM){
	if(isLPM){
		app_gps_sendOspMsg(GPS_OSP_MODE_MPM);
		//eptk_utils_sw_timer_disable(&app_gps_wakeup_str);
		app_console_log("GPS GO LPM\r\n");
	}
	else{
		app_gps_sendOspMsg(GPS_OSP_MODE_NORMAL);
		//eptk_utils_sw_timer_enable(&app_gps_wakeup_str);
		app_console_log("GPS GO NORMAL MODE\r\n");
	}

	return RTRN_OK;
}

int8_t app_gps_sendOspMsg(const char* hexStr){
	eptk_utils_bufferReset(&app_gps_data_str.pvt_tx_buff_str);
	uint16_t checksum = 0, payloadSize = 0;
	uint8_t newChar = 0;

	/*Start sequ*/
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, 0xA0);
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, 0xA2);
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, 0);
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, 0);

	while(*hexStr){
		newChar = eptk_utils_htoi(hexStr);
		payloadSize += 1;
		hexStr += 2;
		if(eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, newChar) != RTRN_OK){
			return RTRN_FULL;
		}
		else{
			checksum += newChar;
			checksum &= 0x7FFF;
		}
	}

	/*Update payload length*/
	*(app_gps_data_str.pvt_tx_buff_str.begin_ptr + 2) = payloadSize >> 8;
	*(app_gps_data_str.pvt_tx_buff_str.begin_ptr + 3) = payloadSize & 0x00FF;
	/*End sequ*/
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, checksum >> 8);
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, checksum & 0x00FF);
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, 0xB0);
	eptk_utils_bufferPush(&app_gps_data_str.pvt_tx_buff_str, 0xB3);

	/* Envoi fait dans app_gps_task (pour avoir le ready to send) */
	//hal_spi_b_sendFrame(app_gps_data_str.tx_buff_str.begin_ptr, eptk_utils_bufferGetCharNb(&app_gps_data_str.tx_buff_str), GPS_SPI_BASE);
	return RTRN_OK;
}

int8_t app_gps_change_mode(uint8_t newMode){
	/*Voir SPI_Interface_On_GPS_Receiver_A2035_H.pdf, page 9*/

	/*If OFF -> power ON*/
	if(!GPIO_GET(GPS_WAKEUP)){
		app_gps_sendOnPulse();
	}

	switch(newMode){
	case GPS_MODE_NMEA:
		app_gps_sendOspMsg(GPS_MODE_NMEA_MSG);
		break;
	case GPS_MODE_OSP:
		hal_spi_b_sendFrame(GPS_MODE_OSP_MSG, GPS_MODE_OSP_MSG_LEN, GPS_SPI_BASE);
		break;
	default:
		return RTRN_NOT_FOUND;
	}

	/* Power OFF*/
	app_gps_sendOnPulse();
	/* Wait 1s */
	eptk_utils_sw_timer_delay_ms(1000);
	/* Power ON*/
	app_gps_sendOnPulse();

	eptk_utils_sw_timer_delay_ms(2000);
	return RTRN_OK;
}

int8_t app_gps_init(void){
	int8_t error = RTRN_OK;

	hal_spi_b_init(GPS_FREQ, HAL_SPI_B_PHASE_1,HAL_SPI_B_POL_0, GPS_SPI_BASE);

	eptk_utils_bufferInit(&app_gps_data_str.pvt_rx_buff_str ,app_gps_data_str.pvt_rxBuff_a, GPS_RX_BUFF_LEN);
	eptk_utils_bufferInit(&app_gps_data_str.pvt_tx_buff_str, app_gps_data_str.pvt_txBuff_a, GPS_TX_BUFF_LEN);

	/* Control Pins */
	/*SPI*/
	GPIO_HIGH(GPS_CS);
	GPIO_OUTPUT(GPS_CS);

	/*IN*/
	GPIO_INPUT(GPS_WAKEUP);
	//GPIO_setAsInputPinWithPullUpResistor(GPS_1PPS_BASE, GPS_1PPS_PIN);
	//GPIO_setDriveStrength(GPS_1PPS_BASE, GPS_1PPS_PIN, GPIO_FULL_OUTPUT_DRIVE_STRENGTH);

	GPIO_clearInterrupt(GPS_WAKEUP_BASE, GPS_WAKEUP_PIN);
	GPIO_selectInterruptEdge(GPS_WAKEUP_BASE, GPS_WAKEUP_PIN,GPIO_LOW_TO_HIGH_TRANSITION);
	GPIO_enableInterrupt(GPS_WAKEUP_BASE, GPS_WAKEUP_PIN);
	eptk_utils_gpio_setIsr(GPS_WAKEUP_BASE, GPS_WAKEUP_PIN, &app_gps_wakeup_isr);

	/*Out*/
	GPIO_LOW(GPS_ON_OFF);
	GPIO_OUTPUT(GPS_ON_OFF);

	GPIO_LOW(GPS_RESET);
	GPIO_OUTPUT(GPS_RESET);


	/*RESET*/
	/*GPIO_HIGH(GPS_RESET);
	eptk_utils_sw_timer_delay_ms(100);
	GPIO_LOW(GPS_RESET);
	eptk_utils_sw_timer_delay_ms(100);*/
	eptk_utils_sw_timer_delay_ms(50);
	GPIO_HIGH(GPS_RESET);

	/* Wait for WAKEUP with timeout */
	app_gps_timeout_f = FALSE;
	eptk_utils_sw_timer_start(&app_gps_delay_str,
			GPS_WAKEUP_TIMEOUT_MS,
			SW_TIMER_SINGLE_SHOT,
			NULL,
			&app_gps_timeout_f, TRUE);

	while(!GPIO_GET(GPS_WAKEUP) && !app_gps_timeout_f){
		LP_MODE();
	}
	eptk_utils_sw_timer_disable(&app_gps_delay_str);

	/*Timeout*/
	if(app_gps_timeout_f){
		return RTRN_NOT_FOUND;
	}

	app_gps_data_str.pvt_OSPstate_e = GPS_STATE_OFF;

	/*ON*/
	//app_gps_sendOnPulse();

	/*Debug*/
	//GPIO_LOW(GPS_CS);

	//app_gps_sendOspMsg(GPS_OSP_INIT_DATA_SOURCE);
	//app_gps_change_mode(GPS_MODE_NMEA);

	/* Put low power mode */
	//app_gps_sendOspMsg(GPS_OSP_MODE_MPM);
	//hal_spi_b_sendFrame(GPS_NMEA_STANDBY, sizeof(GPS_NMEA_STANDBY), GPS_SPI_BASE);
	//app_gps_sendOnPulse();
	//GPIO_HIGH(GPS_CS);

	//error = app_gps_setOFF();

	//eptk_utils_sw_timer_start(&app_gps_wakeup_str, 500, SW_TIMER_CONTINUOUS, NULL, &app_gps_timeout_f, TRUE);
	return error;
}

int8_t app_gps_processOSP(OSPMsg_str_t * OSPMsg){
	char stopOspMsg[]="A600ID0000000000";

	switch(OSPMsg->msgID){

	/*case GPS_ID_2_NAV_ECEF:
		app_gps_data_str.pmode = app_gps_data_str.rxBuff_a[GPS_ID_2_IDX_MODE1] & GPS_ID_2_PMODE_MASK;
		app_console_log("GPS ID 2 : pmode = %d\r\n", app_gps_data_str.pmode);
		break;
	 */
	case GPS_ID_7_RESP_POLL_CLK:
		app_console_log("GPS\tID 7 : drft %ld Hz\r\n",
				(((uint32_t)app_gps_data_str.pvt_rxBuff_a[GPS_ID_7_IDX_CLK_DRFT_LSB-3])<<24)+
				(((uint32_t)app_gps_data_str.pvt_rxBuff_a[GPS_ID_7_IDX_CLK_DRFT_LSB-2])<<16)+
				(((uint32_t)app_gps_data_str.pvt_rxBuff_a[GPS_ID_7_IDX_CLK_DRFT_LSB-1])<<8)+
				((uint32_t)app_gps_data_str.pvt_rxBuff_a[GPS_ID_7_IDX_CLK_DRFT_LSB]));
		break;

	case GPS_ID_11_ACK:
		//app_console_log("GPS\tACK ID %d\r\n", app_gps_data_str.rxBuff_a[3]);
		break;

	case GPS_ID_12_NACK:
		//app_console_log("GPS\tNACK ID %d\r\n", app_gps_data_str.rxBuff_a[3]);
		break;

	case GPS_ID_18_OKTOSEND:
		app_gps_data_str.pvt_okToSend_f = TRUE;
		app_console_log("GPS\tOkToSend\r\n");
		break;

	case GPS_ID_41_NAV_DEG:
		//Filter if is valid
		if(!(app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_YEAR])
		/*|| (app_gps_data_str.rxBuff_a[GPS_ID_41_IDX_VALID_MSG] & 0x80)*/
		){
			app_gps_data_str.timeValid_f = FALSE;
		}
		else{

			app_gps_data_str.timeValid_f = TRUE;
		}

		//Store Lat/Long/Alt
		if((app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_VALID_NAV] & 0x07)){
			//DEBUG_LED_ON(LED_GREEN);

			uint8_t idx = 0;
			uint8_t* lat_ptr =  (uint8_t *) &app_gps_data_str.posLat;
			uint8_t* long_ptr = (uint8_t *) &app_gps_data_str.posLong;
			uint8_t* alt_ptr =  (uint8_t *) &app_gps_data_str.posAlt;

			app_gps_data_str.SV = app_gps_data_str.pvt_rxBuff_a[app_gps_data_str.pvt_OSPMsg_str.payloadLength - 3 + GPS_PAYLOAD_SIZE];

			app_gps_data_str.navValid_f = TRUE;
			app_gps_data_str.newData2log_f = TRUE;

			for(idx = 0; idx < sizeof(int32_t); idx++){
				*(lat_ptr++) = app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_LAT_LSB - idx];
				*(long_ptr++) = app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_LONG_LSB - idx];
				*(alt_ptr++) = app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_ALT_LSB - idx];
			}
			/*app_console_log("GPS\tLT:%ld,LG:%ld,ALT:%ld,SV:%d\r\n",
					app_gps_data_str.posLat, app_gps_data_str.posLong, app_gps_data_str.posAlt, app_gps_data_str.SV);
			*/
			//DEBUG_LED_OFF(LED_GREEN);
		}
		else{
			app_gps_data_str.navValid_f = FALSE;
		}

		//app_console_log("GPS\t SV:%d\r\n", app_gps_data_str.SV);
		/*app_console_log("GPS\tID 41 : %d:%d:%d\r\n",
				app_gps_data_str.rxBuff_a[GPS_ID_41_IDX_HOUR],
				app_gps_data_str.rxBuff_a[GPS_ID_41_IDX_MIN],
				((uint16_t) (app_gps_data_str.rxBuff_a[GPS_ID_41_IDX_SEC] << 8 ) + app_gps_data_str.rxBuff_a[GPS_ID_41_IDX_SEC + 1])/1000
				//,(app_gps_data_str.rxBuff_a[app_gps_data_str.OSPMsg.payloadLength - 1 + GPS_PAYLOAD_SIZE] & 0x08)
		);*/

		//Update RTC ?
		if(app_gps_data_str.toDoUpdateRTC_f && app_gps_data_str.timeValid_f == TRUE){ //If not init
			eptk_utils_rtcTimeNEW_str.Year = eptk_utils_decToBCD16(((uint16_t)app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_YEAR] << 8) + app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_YEAR + 1]);
			eptk_utils_rtcTimeNEW_str.Month = eptk_utils_decToBCD8(app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_MONTH]);
			eptk_utils_rtcTimeNEW_str.DayOfMonth = eptk_utils_decToBCD8(app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_DAY]);
			eptk_utils_rtcTimeNEW_str.Hours = eptk_utils_decToBCD8(app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_HOUR]);
			eptk_utils_rtcTimeNEW_str.Minutes = eptk_utils_decToBCD8(app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_MIN]);
			eptk_utils_rtcTimeNEW_str.Seconds = eptk_utils_decToBCD8(((uint16_t) (app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_SEC] << 8 ) + app_gps_data_str.pvt_rxBuff_a[GPS_ID_41_IDX_SEC + 1])/1000);

			eptk_utils_rtcTimeNEW_str.DayOfWeek = 1;

			eptk_utils_rtcTimeUpdate();

			app_gps_data_str.toDoUpdateRTC_f = FALSE;
		}
		break;

	case GPS_ID_65_GPIO:
	case 71:
		break;

	case 90: //Power Mode Response
		app_console_log("GPS 90 : Mode %02X Err %02X\r\n", app_gps_data_str.pvt_rxBuff_a[3], app_gps_data_str.pvt_rxBuff_a[4]);
		break;
	default:
		//Try to stop undesired message
		app_console_log("GPS\tDump %d\r\n", app_gps_data_str.pvt_OSPMsg_str.msgID);
		eptk_utils_hx8toAscii(app_gps_data_str.pvt_OSPMsg_str.msgID,stopOspMsg + 4);
		app_gps_sendOspMsg(stopOspMsg);
		return RTRN_NOT_FOUND;
		break;
	}

	return RTRN_OK;
}

int8_t app_gps_task(void){
	volatile uint8_t rcvdChar = 0, lastChar = 0, chartosend = 0;
	volatile uint8_t rcvng_idx = 0;

	if(app_gps_todo_f){
		//Task timeout
		eptk_utils_sw_timer_start(&app_gps_delay_str, 100, SW_TIMER_SINGLE_SHOT, NULL, NULL, FALSE);

		hal_spi_b_init(GPS_FREQ, HAL_SPI_B_PHASE_1,HAL_SPI_B_POL_0, GPS_SPI_BASE);
		GPIO_LOW(GPS_CS);

		while(  !eptk_utils_sw_timer_isFinished(&app_gps_delay_str)
				&& ((rcvdChar != 0xA7 && lastChar != 0xB4)
				|| eptk_utils_bufferGetCharNb(&app_gps_data_str.pvt_tx_buff_str)
				|| (app_gps_data_str.pvt_OSPstate_e == GPS_STATE_RCVNG))){ //While not padding

			lastChar = rcvdChar;
			if(app_gps_data_str.pvt_okToSend_f){
				//Read byte
				chartosend = eptk_utils_bufferPop(&app_gps_data_str.pvt_tx_buff_str);
			}

			rcvdChar = hal_spi_b_sendByte(chartosend, GPS_SPI_BASE);

			switch(app_gps_data_str.pvt_OSPstate_e){
			case GPS_STATE_OFF:
				return RTRN_OK;
				break;
			case GPS_STATE_IDLE:
				if((lastChar == 0xA0 && rcvdChar == 0xA2)
						){
					app_gps_data_str.pvt_OSPstate_e = GPS_STATE_RCVNG;
					eptk_utils_bufferReset(&app_gps_data_str.pvt_rx_buff_str);
				}
				else if((!lastChar && !rcvdChar)){
					app_gps_todo_f = FALSE;
				}
				break;
			case GPS_STATE_RCVNG:
				eptk_utils_bufferPush(&app_gps_data_str.pvt_rx_buff_str, rcvdChar);
				rcvng_idx = eptk_utils_bufferGetCharNb(&app_gps_data_str.pvt_rx_buff_str)-1;
				switch(rcvng_idx){
				case OSP_IDX_PAYLOAD_SIZE_END:
					//Read payload size
					app_gps_data_str.pvt_OSPMsg_str.payloadLength = (((uint16_t) app_gps_data_str.pvt_rxBuff_a[OSP_IDX_PAYLOAD_SIZE_END - 1]) << 8 ) + (uint16_t) app_gps_data_str.pvt_rxBuff_a[OSP_IDX_PAYLOAD_SIZE_END];
					break;
				case OSP_IDX_ID_CODE:
					//Read id
					app_gps_data_str.pvt_OSPMsg_str.msgID = rcvdChar;
					break;
					//CRC
				default:
					if(rcvng_idx == app_gps_data_str.pvt_OSPMsg_str.payloadLength + 3){
						app_gps_data_str.pvt_OSPMsg_str.CRC = (((uint16_t) lastChar) << 8) + (uint16_t) rcvdChar;
						__no_operation();
					}
					else if(rcvng_idx == app_gps_data_str.pvt_OSPMsg_str.payloadLength + 5){

						//FIN MESSAGE
						app_gps_processOSP(&app_gps_data_str.pvt_OSPMsg_str);
						app_gps_data_str.pvt_OSPstate_e = GPS_STATE_IDLE;
					}
					break;
				}
				break;
				default:
					break;
			}
		}
		//app_console_log("GPS Padding\r\n");
		GPIO_HIGH(GPS_CS);
		if(!eptk_utils_sw_timer_isFinished(&app_gps_delay_str)){
			app_gps_todo_f = FALSE;
			return RTRN_SLEEP_OK;
		}
		else{
			return RTRN_BUSY;
		}
	}
	return RTRN_SLEEP_OK;
}
