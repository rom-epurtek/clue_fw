/*
 * \file app_console.c
 *
 * \date 19 juil. 2016
 * \author: romain
 */

#include <driverlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "app_console.h"
#include "../eptk_utils/cmdline.h"
#include "../eptk_utils/eptk_utils.h"

#include "app_ble.h"
#include "app_usb.h"
#include "app_sd_log.h"
#include "app_mox.h"

/* Local fns */
int Cmd_help(int argc, char *argv[]);

int Cmd_sd_info(int argc, char *argv[]);

int Cmd_sd_log_read(int argc, char *argv[]);

int Cmd_sd_log_erase(int argc, char *argv[]);

int Cmd_reset(int argc, char *argv[]);

int Cmd_BSL(int argc, char *argv[]);

int Cmd_MoxHeatOn(int argc, char *argv[]);

int Cmd_MoxHeatOff(int argc, char *argv[]);

int Cmd_log_on_off(int argc, char *argv[]);

/* Config */
/**
 * \brief Declaration of uart communication interfaces : usb and bluetooth
 */
app_console_str_t app_console_struct_a[]={
		{uart_str_ptr : &app_usb_uart_infos_str, log_active_f : FALSE},
		{uart_str_ptr : &app_ble_uart_infos_str, log_active_f : FALSE},
		{ 0, 0 } //DO NOT REMOVE
};

/**
 * \brief This table holds the command names, implementing functions,and brief description.
 */
tCmdLineEntry g_sCmdTable[] =
{
		{ "help",   		Cmd_help,      		" : Command list" 	},
		{ "reset",			Cmd_reset,			" : Reset Board (choose por or bor in app_conf)" },
		{ "sd_info",		Cmd_sd_info,   		" : SD card infos" 	},
		{ "bsl",			Cmd_BSL,   		" : Go to BSL mode" 	},
		{ "heat_on",		Cmd_MoxHeatOn,   		" : MOX Nox Heat on" 	},
		{ "heat_off",		Cmd_MoxHeatOff,   		" : MOX Nox Heat off" 	},
		{ "sd_log_read",	Cmd_sd_log_read,	" : Read SD log" 			},
		{ "sd_log_erase",	Cmd_sd_log_erase,	" : Erase SD log" 			},
		{ "log",			Cmd_log_on_off,	" : Set Console on/off"	},
		{ 0, 0, 0 } //DO NOT REMOVE
};

/* GLOBALS */
volatile bool_t app_console_todo_f = FALSE;

//LOCALS
//uint8_t app_console_buffer_a[APP_CONSOLE_RX_BUFFER_SIZE];
bool_t app_console_isOn_f = TRUE;
char g_consoleTxBuff[EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE + 1];
uint8_t consoleRxBuff[APP_CONSOLE_RX_BUFF_SIZE + 1];
eptk_utils_buffer_str_t app_console_RX_buffer_str = {0};
app_console_str_t* app_console_current_struct_ptr=0;

int8_t app_console_init(){
	eptk_utils_bufferInit(&app_console_RX_buffer_str, consoleRxBuff, APP_CONSOLE_RX_BUFF_SIZE);
	return RTRN_OK;
}

int8_t app_console_task(void){
	uint8_t newChar = 0;
	uint8_t charTimeout = 0xFF;
	int8_t cmdProcessError = 0;

	//If nothing to do get out
	if (!app_console_todo_f){
		return RTRN_SLEEP_OK;
	}
	else{
		//Reset flag
		app_console_todo_f = FALSE;
	}

	/* Entering console mode */
	/*if(g_hal_sw_uart_state & UART_RX_DETECTED){
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_ptr, UART_SEND_NOW,INIT_MSG_FORMAT_STRING , VERSION_STRING, COMPIL_TIME_STRING);
		CmdLineProcess("help");
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_ptr, UART_SEND_NOW,"\r\n$>");
		hal_sw_uart_enableRX();
		return RTRN_OK;
	}*/
	app_console_current_struct_ptr = &app_console_struct_a[0];

	while(app_console_current_struct_ptr->uart_str_ptr){
		while(charTimeout-- && ((newChar = eptk_utils_bufferPop(&(app_console_current_struct_ptr->uart_str_ptr->RX_buffer_struc))) > 0)){
			if(newChar == APP_CONSOLE_END_CHAR){
				//Echo
				eptk_utils_UART_puts(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, (uint8_t *) "\r\n");
				//process cmd
				eptk_utils_bufferPush(&app_console_RX_buffer_str, 0);
				cmdProcessError = CmdLineProcess((char *)app_console_RX_buffer_str.head_ptr);
				switch(cmdProcessError){
				case CMDLINE_BAD_CMD:
					eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"Unknown command %s", app_console_RX_buffer_str.head_ptr);
					break;
				case CMDLINE_TOO_MANY_ARGS:
					eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"Too many arguments for '%s\r\n", app_console_RX_buffer_str.head_ptr);
					break;
				default:break;
				}
				eptk_utils_bufferReset(&app_console_RX_buffer_str);
				eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"\r\n$>");
				break;
			}
			else{
				if(eptk_utils_bufferPush(&app_console_RX_buffer_str, newChar)){
					//Echo
					eptk_utils_UART_sendMsg(&newChar, app_console_current_struct_ptr->uart_str_ptr, 1);
					if(( newChar == 8 || newChar == APP_CONSOLE_BS_CHAR) || ( newChar == APP_CONSOLE_DEL_CHAR)){
						/* Pop last char if exists */
						eptk_utils_bufferPopLast(&app_console_RX_buffer_str);
						eptk_utils_bufferPopLast(&app_console_RX_buffer_str);
					}
				}
			}
		}//while char
		app_console_current_struct_ptr++;
	}//while uart itf

	if(!charTimeout){
		return RTRN_BUSY;
	}
	else{
		return RTRN_SLEEP_OK;
	}
}

void app_console_log(const char *fmt, ...) {
	int8_t size = 0;
	app_console_str_t* app_console_struct_ptr;

	if(app_console_isOn_f){
		size += eptk_utils_hx8toAscii(eptk_utils_rtcTime_str.Hours, g_consoleTxBuff + size);
		*(g_consoleTxBuff + size++) = ':';
		size += eptk_utils_hx8toAscii(eptk_utils_rtcTime_str.Minutes, g_consoleTxBuff + size);
		*(g_consoleTxBuff + size++) = ':';
		size += eptk_utils_hx8toAscii(eptk_utils_rtcTime_str.Seconds, g_consoleTxBuff + size);
		*(g_consoleTxBuff + size++) = ' ';

		__VALIST args;
		va_start(args, fmt);
		size += vsnprintf(g_consoleTxBuff + size, EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE, fmt, args);
		va_end(args);

		g_consoleTxBuff[size] = 0;
		/* RETOUR de snprintf :
		 * APP_UART_LOG_MAX_FORMATED_SIZE doit contenir le 0
		 * snprintf retourne le nombre converti (sans compter le 0)
		 * donc size < APP_UART_LOG_MAX_FORMATED_SIZE
		 * si size >= APP_UART_LOG_MAX_FORMATED_SIZE c'est qu'il n'y avait pas la place
		 */
		//eptk_utils_UART_printf(app_console_current_struct_ptr->uart_ptr, UART_SEND_NORMAL,"%02d:%02d:%02d | ",
		//		eptk_utils_rtcTime_str.Hours, eptk_utils_rtcTime_str.Minutes, eptk_utils_rtcTime_str.Seconds);

		app_console_struct_ptr = &app_console_struct_a[0];

		while(app_console_struct_ptr->uart_str_ptr){
			if(app_console_struct_ptr->log_active_f){
				eptk_utils_UART_puts(app_console_struct_ptr->uart_str_ptr, UART_SEND_NORMAL, (uint8_t* ) g_consoleTxBuff);
			}
			app_console_struct_ptr++;
		}

	}
}

//*****************************************************************************
//
// This function implements the "help" command.  It prints a simple list
// of the available commands with a brief description.
//
//*****************************************************************************
int Cmd_help(int argc, char *argv[])
{
	tCmdLineEntry *pEntry;

	//
	// Print some header text.
	//
	eptk_utils_UART_puts(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, (uint8_t*) "Command set\r\n--\r\n");

	//
	// Point at the beginning of the command table.
	//
	pEntry = &g_sCmdTable[0];

	//
	// Enter a loop to read each entry from the command table.  The
	// end of the table has been reached when the command name is NULL.
	//
	while(pEntry->pcCmd)
	{
		//
		// Print the command name and the brief description.
		//
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr,UART_SEND_NOW,"%s\t%s\r\n", pEntry->pcCmd, pEntry->pcHelp);

		//
		// Advance to the next entry in the table.
		//
		pEntry++;
	}

	eptk_utils_UART_puts(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, (uint8_t*) "--\r\n");
	//
	// Return success.
	//
	return(0);
}


int Cmd_sd_info(int argc, char *argv[]){
	uint32_t logSize;
	/*Carte ins�r�e ?*/
	if(!app_sd_log_isCardInserted()){
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"SD NOT inserted\r\n");
		return 0;
	}

	eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"\r\nSD CARD INFOS\r\n--\r\nCapacity : %lu B\r\n", app_sd_log_struc.cardSize);
	logSize = app_sd_log_getLogSizeOnSD()*512;
	eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"Log size : %lu B\r\n" , logSize );
	eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"Occupied : %lu %%\r\n", logSize*100 / app_sd_log_struc.cardSize);
	return 0;
}

int Cmd_sd_log_erase(int argc, char *argv[]){
	uint8_t confirmChar = 0;
	eptk_utils_UART_puts(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,  (uint8_t*) "Erase log ? ('y' to confirm)");
	while(!(confirmChar = eptk_utils_bufferPop(&(app_console_current_struct_ptr->uart_str_ptr->RX_buffer_struc)))){
		LP_MODE();
	}
	if(confirmChar == 'y'){
		eptk_utils_bufferReset(&app_sd_log_struc.logBuffer_str);
		app_sd_log_struc.sectorReadIdx = app_sd_log_struc.sectorWriteIdx;
		//app_sd_log_struc.sectorWriteIdx = SD_LOG_FIRST_WRITE_SECTOR;
		if(app_sd_log_writeIndexes() == RTRN_OK){
			eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"\r\nLog erased\r\n");
		}
		else{
			eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"\r\nWrite err\r\n");
		}
	}
	else{
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"\r\nLog NOT erased\r\n");
	}
	return 0;
}

int Cmd_sd_log_read(int argc, char *argv[]){
	char buff[SD_LOG_SECTOR_SIZE + 1 ];
	int8_t error = RTRN_OK;
	uint32_t readIdxBckp = app_sd_log_struc.sectorReadIdx;

	if((app_sd_log_getLogSizeOnSD() == 0) && !eptk_utils_bufferGetCharNb(&app_sd_log_struc.logBuffer_str) ){
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,"Log empty\r\n");
		return 0;
	}

	app_sd_log_writeCurrentSector(); //Write ram buffer in sd

#ifdef USE_WATCHDOG
	/*WATCHDOG*/
	WDT_A_hold(WDT_A_BASE);
#endif
	__disable_interrupt();
	while( (error = app_sd_log_read((uint8_t *)buff, 1)) > 0){
		if(error != 1){
			eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, "SD read err\r\n");
#ifdef USE_WATCHDOG
			WDT_A_resetTimer(WDT_A_BASE);
			WDT_A_start(WDT_A_BASE);
#endif
			__enable_interrupt();
			return -1;
		}
		else{
			//Debug
			//eptk_utils_UART_puts( app_console_current_struct_ptr->uart_ptr, UART_SEND_NOW, "\r\nNEWSECTOR\r\n");
			buff[SD_LOG_SECTOR_SIZE] = '\0'; //END OF STRING
			eptk_utils_UART_puts( app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, (uint8_t *) buff);
		}
	}
#ifdef USE_WATCHDOG
	WDT_A_resetTimer(WDT_A_BASE);
	WDT_A_start(WDT_A_BASE);
#endif
	//commenter si on veut pas effacer apr�s le read
	//app_sd_log_writeIndexes();
	app_sd_log_struc.sectorReadIdx = readIdxBckp;
	__enable_interrupt();
	return 0;
}

int Cmd_reset(int argc, char *argv[]){
	uint8_t confirmChar = 0;

	eptk_utils_UART_puts(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, (uint8_t*) "Reset ? Will close connections ('y' to confirm)\r\n");

	while(!(confirmChar = eptk_utils_bufferPop(&(app_console_current_struct_ptr->uart_str_ptr->RX_buffer_struc)))){
		LP_MODE();
	}
	if(confirmChar == 'y'){
		eptk_utils_UART_puts(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW,(uint8_t*) "ok\r\n");
		SW_RESET();
	}
	else{
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, "Reset canceled\r\n");
	}
	return 0;
}

int Cmd_BSL(int argc, char *argv[]){

	WDT_A_hold(WDT_A_BASE);
	__disable_interrupt();
	eptk_utils_gpio_reset();
	GPIO_setAsInputPin(FTDI_UART_RX_BASE, FTDI_UART_RX_PIN + FTDI_UART_TX_PIN);

	((void (*)())0x1000)();
	return 0;
}

int Cmd_MoxHeatOn(int argc, char *argv[]){

	app_mox_heatOnNOx();

	return 0;
}

int Cmd_MoxHeatOff(int argc, char *argv[]){

	app_mox_heatOffNOx();

	return 0;
}

int Cmd_log_on_off(int argc, char *argv[]){
	uint8_t globalActive = 0;
	app_console_str_t* local_app_console_struct_ptr = &app_console_struct_a[0];
	if(argc > 1){
		if(0 == strcasecmp(argv[1], "on")){
			app_console_current_struct_ptr->log_active_f = TRUE;
		}
		else if(0 == strcasecmp(argv[1], "off")){
			app_console_current_struct_ptr->log_active_f = FALSE;
		}

		while(local_app_console_struct_ptr->uart_str_ptr){
			if(local_app_console_struct_ptr->log_active_f){
				globalActive++;
			}
			local_app_console_struct_ptr++;
		}

		if(!globalActive){
			app_console_isOn_f = FALSE;
		}
		else{
			app_console_isOn_f = TRUE;
		}
		eptk_utils_UART_printf(app_console_current_struct_ptr->uart_str_ptr, UART_SEND_NOW, "Local log is %s\r\nGlobal log is %s\r\n",
				app_console_current_struct_ptr->log_active_f ?"on":"off",
				app_console_isOn_f ?"on":"off");
	}

	return 0;
}
