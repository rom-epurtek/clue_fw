/*
 * app_mox.c
 *
 *  Created on: 3 f�vr. 2017
 *      Author: Paul
 */

#include "driverlib.h"
#include "app_console.h"
#include "app_mox.h"

#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include "../eptk_utils/adc.h" //For typedef and RTRN codes
#include "../conf/bsp.h"
#include "../conf/app_conf.h"

#include <stdint.h>
#include <inttypes.h>


// Structure containing flags, and timer informations to manage automatic measurement.
typedef struct {
	mox_sensor_e_t sensor_e;
	volatile bool_t measure_auto_timer_f;
	bool_t measure_auto_running_f;
	timer_str_t auto_measure_timer_str;
} app_mox_autoMeasure_str_t;

app_mox_autoMeasure_str_t autoMeasure_str = {0};
app_mox_str_t app_mox_data_str;
uint16_t tempADCValue = 0;

int8_t app_mox_init(void){
	int8_t error = RTRN_OK;

	GPIO_setAsPeripheralModuleFunctionInputPin(MOX_SENSOR_NOX); //ADC
	GPIO_setAsPeripheralModuleFunctionInputPin(MOX_SENSOR_CO);	//ADC
	GPIO_setOutputLowOnPin(MOX_3STE_NOX);
	GPIO_setOutputLowOnPin(MOX_3STE_CO);
	GPIO_INPUT(MOX_3STE_NOX);
	GPIO_INPUT(MOX_3STE_CO);
	//Set off heater
	GPIO_setOutputHighOnPin(MOX_HEAT_NOX);
	GPIO_OUTPUT(MOX_HEAT_NOX);
	GPIO_setOutputHighOnPin(MOX_HEAT_CO);
	GPIO_OUTPUT(MOX_HEAT_CO);

	app_mox_data_str.nox_heat_state_f = FALSE;
	app_mox_data_str.co_heat_state_f = FALSE;
	app_mox_data_str.pvt_nox_3ste_state_f = FALSE; //Small resistance, 2 resistances used
	app_mox_data_str.pvt_co_3ste_state_f = FALSE; //Small resistance, 2 resistances used
	app_mox_data_str.pvt_getting_nox_f = FALSE;
	app_mox_data_str.pvt_getting_co_f = FALSE;
	app_mox_data_str.pvt_newValueNox_f = FALSE;
	app_mox_data_str.pvt_newValueCo_f = FALSE;
	app_mox_data_str.dataNOXReadyToWrite_f = FALSE;
	app_mox_data_str.dataCOReadyToWrite_f = FALSE;
	app_mox_data_str.nox_sensing_res = MOX_OX_RSERIELOW_VAL;
	app_mox_data_str.co_sensing_res = MOX_RED_RSERIELOW_VAL;
	app_mox_data_str.nox_value = 0;
	app_mox_data_str.co_value = 0;
	autoMeasure_str.measure_auto_running_f = FALSE;

	return error;
}

int8_t app_mox_heatOnNOx(void){
	int8_t error = RTRN_OK;

	if(app_mox_data_str.nox_heat_state_f){
		error = RTRN_ERR;
	}
	else{
		//Set on
		GPIO_setOutputLowOnPin(MOX_HEAT_NOX);
		app_mox_data_str.nox_heat_state_f = TRUE;
	}

	return error;
}

int8_t app_mox_heatOffNOx(void){
	int8_t error = RTRN_OK;

	if(!app_mox_data_str.nox_heat_state_f){
		error = RTRN_ERR;
	}
	else{
		//Set off
		GPIO_setOutputHighOnPin(MOX_HEAT_NOX);
		app_mox_data_str.nox_heat_state_f = FALSE;
	}
	return error;
}

int8_t app_mox_heatOnCO(void){
	int8_t error = RTRN_OK;

	if(app_mox_data_str.co_heat_state_f){
		error = RTRN_ERR;
	}
	else{
		//Set on
		GPIO_setOutputLowOnPin(MOX_HEAT_CO);
		app_mox_data_str.co_heat_state_f = TRUE;
	}

	return error;
}

int8_t app_mox_heatOffCO(void){

	int8_t error = RTRN_OK;

	if(!app_mox_data_str.co_heat_state_f){
		error = RTRN_ERR;
	}
	else{
		//Set off
		GPIO_setOutputHighOnPin(MOX_HEAT_CO);
		app_mox_data_str.co_heat_state_f = FALSE;
	}
	return error;
}


int8_t app_mox_measureAuto(uint32_t duration, mox_sensor_e_t sensor_e){
	int8_t error = RTRN_OK;

	if(autoMeasure_str.measure_auto_running_f){
		error = RTRN_BUSY;
	}
	else{
		autoMeasure_str.measure_auto_running_f = TRUE;
		autoMeasure_str.sensor_e = sensor_e;

		if(autoMeasure_str.sensor_e & MOX_NOX){
			app_console_log("LAUNCH NOX AUTO MEAS\r\n");
			app_mox_data_str.pvt_getting_nox_f = FALSE;
			app_mox_data_str.pvt_newValueNox_f =FALSE;
			app_mox_heatOnNOx();
		}
		if(autoMeasure_str.sensor_e & MOX_CO){
			app_console_log("LAUNCH CO AUTO MEAS\r\n");
			app_mox_data_str.pvt_newValueCo_f = FALSE;
			app_mox_data_str.pvt_getting_co_f = FALSE;
			app_mox_heatOnCO();
		}

		/*timer*/
		autoMeasure_str.measure_auto_timer_f = FALSE;

		eptk_utils_sw_timer_start(&(autoMeasure_str.auto_measure_timer_str),
				duration,
				SW_TIMER_SINGLE_SHOT,
				NULL,
				&(autoMeasure_str.measure_auto_timer_f), TRUE);
	}

	return error;
}

void app_mox_measureAutoCancel(void){
	eptk_utils_sw_timer_disable(&(autoMeasure_str.auto_measure_timer_str));
	autoMeasure_str.measure_auto_timer_f = FALSE;
	autoMeasure_str.measure_auto_running_f = FALSE;
	app_mox_heatOffNOx();
	app_mox_heatOffCO();
}

int8_t app_mox_task(void){
	int8_t error = RTRN_OK;
	uint64_t res = 0;

	if(autoMeasure_str.measure_auto_timer_f){
		app_console_log("MOX Auto heat end\r\n");
		if(autoMeasure_str.sensor_e & MOX_NOX){
			app_mox_heatOffNOx();
			app_mox_measureNOx();
		}
		if(autoMeasure_str.sensor_e & MOX_CO){
			app_mox_heatOffCO();
			app_mox_measureCO();
		}

		autoMeasure_str.measure_auto_running_f = FALSE;
		autoMeasure_str.measure_auto_timer_f = FALSE;
	}

	if(app_mox_data_str.pvt_newValueNox_f){
		app_mox_data_str.pvt_newValueNox_f = FALSE;
		app_mox_data_str.pvt_getting_nox_f = FALSE;
		app_mox_data_str.nox_value = (uint32_t)ADC12_A_getResults(ADC12_A_BASE, MOX_NOX_ADC_INPUT);
		if(app_mox_data_str.nox_value == 0){
			app_mox_data_str.nox_value = 1;
		}

		/* If measured value is too high (= sensor res low) and sensing res was the high res, sensing res switch
		 * to low res and a new measure is performed
		 */
		if((app_mox_data_str.nox_value > ADC_FS_70PERCENT) && app_mox_data_str.pvt_nox_3ste_state_f){
			app_mox_data_str.pvt_nox_3ste_state_f = FALSE;
			app_mox_data_str.nox_sensing_res = MOX_OX_RSERIELOW_VAL;
			GPIO_setOutputLowOnPin(MOX_3STE_NOX);
			GPIO_OUTPUT(MOX_3STE_NOX);
			app_mox_measureNOx();
		}
		/* If measured value is too low (= sensor res high) and sensing res was the low res, sensing res switch
		 * to high res and a new measure is performed
		 */
		else if((app_mox_data_str.nox_value < ADC_FS_20PERCENT) && !app_mox_data_str.pvt_nox_3ste_state_f){
			app_mox_data_str.pvt_nox_3ste_state_f = TRUE;
			app_mox_data_str.nox_sensing_res = MOX_OX_RSERIEFULL_VAL;
			GPIO_INPUT(MOX_3STE_NOX);
			app_mox_measureNOx();
		}
		else{
			res = ((uint64_t)((uint64_t)app_mox_data_str.nox_sensing_res*(uint64_t)4096)/((uint64_t)app_mox_data_str.nox_value))
							- MOX_OX_RSERIE_VAl - (uint64_t)app_mox_data_str.nox_sensing_res;
			/*If superior than maximum 32 bits value
			 */
			if(res > 0xFFFFFFFF){
				res = 0;
			}
			app_mox_data_str.nox_value = (uint32_t) res;
			app_mox_data_str.dataNOXReadyToWrite_f = TRUE;
		}
	}

	if(app_mox_data_str.pvt_newValueCo_f){
		app_mox_data_str.pvt_newValueCo_f = FALSE;
		app_mox_data_str.pvt_getting_co_f = FALSE;
		app_mox_data_str.co_value = (uint32_t)ADC12_A_getResults(ADC12_A_BASE, MOX_CO_ADC_INPUT);
		if(app_mox_data_str.co_value == 0){
			app_mox_data_str.co_value = 1;
		}
		/* If measured value is too high (= sensor res low) and sensing res was the high res, sensing res switch
		 * to low res and a new measure is performed
		 */
		if((app_mox_data_str.co_value > ADC_FS_70PERCENT) && app_mox_data_str.pvt_co_3ste_state_f){
			app_mox_data_str.pvt_co_3ste_state_f = FALSE;
			app_mox_data_str.co_sensing_res = MOX_RED_RSERIELOW_VAL;
			GPIO_setOutputLowOnPin(MOX_3STE_CO);
			GPIO_OUTPUT(MOX_3STE_CO);
			app_mox_measureCO();
		}
		/* If measured value is too low (= sensor res high) and sensing res was the low res, sensing res switch
		 * to high res and a new measure is performed
		 */
		else if((app_mox_data_str.co_value < ADC_FS_20PERCENT) && !app_mox_data_str.pvt_co_3ste_state_f){
			app_mox_data_str.pvt_co_3ste_state_f = TRUE;
			app_mox_data_str.co_sensing_res = MOX_RED_RSERIEFULL_VAL;
			GPIO_INPUT(MOX_3STE_CO);
			app_mox_measureCO();
		}
		else{
			res = ((uint64_t)((uint64_t)app_mox_data_str.co_sensing_res*(uint64_t)4096)/((uint64_t)app_mox_data_str.co_value))
	                    		- MOX_RED_RSERIE_VAl - (uint64_t)app_mox_data_str.co_sensing_res;
			/*If superior than maximum 32 bits value
			 */
			if(res > 0xFFFFFFFF){
				res = 0;
			}
			app_mox_data_str.co_value = (uint32_t) res;
			app_mox_data_str.dataCOReadyToWrite_f = TRUE;
		}
	}

	return error;
}

int8_t app_mox_measureNOx(void){

	int8_t error = RTRN_OK;

	// Start a single conversion, no repeating or sequences.
	if(!app_mox_data_str.pvt_getting_nox_f){
		app_mox_data_str.pvt_getting_nox_f = TRUE;
		eptk_utils_adc_startConversion(MOX_NOX_ADC_INPUT, &(app_mox_data_str.pvt_newValueNox_f));
	}
	else{
		error = RTRN_BUSY;
	}

	return error;
}
//
int8_t app_mox_measureCO(void){

	int8_t error = RTRN_OK;

	// Start a single conversion, no repeating or sequences.
	if(!app_mox_data_str.pvt_getting_co_f){
		app_mox_data_str.pvt_getting_co_f = TRUE;
		eptk_utils_adc_startConversion(MOX_CO_ADC_INPUT, &(app_mox_data_str.pvt_newValueCo_f));
	}
	else{
		error = RTRN_BUSY;
	}

	return error;
}
