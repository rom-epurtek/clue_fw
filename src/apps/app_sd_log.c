/*
 * \file app_sd_log.c
 *	\date 15 juil. 2016
 *  \author: RS
 *  \brief SD card log system
 *  Includes raw SD card write and read functions. Wear leveling. No file system
 */

#include <driverlib.h>
#include "../hal/hal_spi_b.h"
#include "app_sd_log.h"
#include "../eptk_utils/eptk_utils.h"
#include "app_console.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <inttypes.h>

//#define withDMA //TODO CODE DMA

/* Variables declaration */
//GLOBALS
volatile app_sd_log_state_str_t app_sd_log_struc = {0};

//LOCALS
//eptk_utils_buffer_struc_t app_sd_log_struc.logBuffer_str;
uint8_t eptk_utils_buffer_a[SD_LOG_SECTOR_SIZE] = {0x00};
uint8_t IdxSectorBuff[SD_LOG_IDX_LENGTH] = {0};
char convertBuffer[APP_SD_LOG_MAX_FORMATED_SIZE] = {0};

int8_t app_sd_log_writeCurrentSector(void);
int8_t app_sd_log_readIndexes(void);

// Low level Function Prototypes
uint8_t mmcGetResponse(void);
uint8_t mmcGetXXResponse(const uint8_t resp);
int8_t mmcCheckBusy(void);
int8_t mmcGoIdle();
int8_t mmcPing(void);
void mmcSendCmd (const int8_t cmd, uint32_t data, const int8_t crc);
int8_t mmcSetBlockLength (const uint32_t blocklength);
uint32_t mmcReadCardSize();
int8_t mmcInit();

// read a size Byte big block beginning at the address.
int8_t mmcReadBlock(const uint32_t address, const uint32_t count, uint8_t *pBuffer);
#define mmcReadSector(sector, pBuffer) mmcReadBlock(sector*SD_LOG_SECTOR_SIZE, SD_LOG_SECTOR_SIZE, pBuffer)

// write a 512 Byte big block beginning at the (aligned) address
int8_t mmcWriteBlock (const uint32_t address, const uint32_t count, uint8_t *pBuffer);
#define mmcWriteSector(sector, pBuffer) mmcWriteBlock(sector*SD_LOG_SECTOR_SIZE, SD_LOG_SECTOR_SIZE, pBuffer)


int8_t app_sd_log_writeCurrentSector(void){
	int8_t mmcStat = 0;
	uint32_t tempIndex;

	//Check card presence before writing
	if(!app_sd_log_isCardInserted()){
		return RTRN_NOT_FOUND;
	}

	if(eptk_utils_bufferGetCharNb(&app_sd_log_struc.logBuffer_str) == 0){
		return RTRN_EMPTY;
	}

	//Makes break char for printf functions
	*(app_sd_log_struc.logBuffer_str.tail_ptr) = '\0';

	mmcStat = mmcWriteSector(app_sd_log_struc.sectorWriteIdx, eptk_utils_buffer_a);

	if(mmcStat == MMC_SUCCESS){
		//tempIndex = app_sd_log_struc.sectorWriteIdx++;
		app_sd_log_struc.sectorWriteIdx++;
		if(app_sd_log_struc.sectorWriteIdx == app_sd_log_struc.sectorNB){
			app_sd_log_struc.sectorWriteIdx = SD_LOG_FIRST_WRITE_SECTOR;
		}
		//TODO Check if buffer full
		app_sd_log_writeIndexes();
		eptk_utils_bufferReset(&app_sd_log_struc.logBuffer_str);
		//Restore write indec
		//app_sd_log_struc.sectorWriteIdx = tempIndex;
		return RTRN_OK;
	}
	return RTRN_ERR;
}

int8_t app_sd_log_readIndexes(void){
	uint8_t i = 0;
	char* initKey_str = SD_LOG_INIT_KEY;
	int8_t mmcStat = 0;
	volatile bool_t toDoInit = FALSE;

	mmcStat = mmcReadBlock(SD_LOG_IDX_SECTOR, SD_LOG_IDX_LENGTH, &IdxSectorBuff[0]);
	if(MMC_SUCCESS == mmcStat){
		/* Indexes are 32bits long each -> 64 bits (8 bytes)*/
		app_sd_log_struc.sectorReadIdx = 0;
		app_sd_log_struc.sectorWriteIdx = 0;

		/*Check if sd card is already initialised*/
		i = SD_LOG_INIT_KEY_IDX;
		while(*initKey_str){
			if(IdxSectorBuff[i++] != *(initKey_str++)){ //If different init key
				toDoInit = TRUE;
				break;
			}
		}
		if(!toDoInit){
			for(i = 0 ; i < 4 ; i++){
				app_sd_log_struc.sectorReadIdx += ((int32_t) IdxSectorBuff[i]) << (8*i) ;
				app_sd_log_struc.sectorWriteIdx += ((int32_t) IdxSectorBuff[i+4]) << (8*i) ;
			}
		}
		else{
			app_sd_log_struc.sectorReadIdx = SD_LOG_FIRST_WRITE_SECTOR;
			app_sd_log_struc.sectorWriteIdx = SD_LOG_FIRST_WRITE_SECTOR;
			app_sd_log_writeIndexes();
		}
		return RTRN_OK;
	}
	//Read error
	else{
		return RTRN_ERR;
	}
}

int8_t app_sd_log_writeIndexes(void){
	uint8_t i = 0;
	char* initKey_str = SD_LOG_INIT_KEY;

	/*Write INIT KEY*/
	i = SD_LOG_INIT_KEY_IDX;
	while(*initKey_str){
		IdxSectorBuff[i++] = *(initKey_str++); //Copy char
	}

	for(i = 0 ; i < 4 ; i++){
		IdxSectorBuff[i] = (uint8_t)((app_sd_log_struc.sectorReadIdx >> (8*i) ) & 0xFF);
		IdxSectorBuff[i+4] = (uint8_t)((app_sd_log_struc.sectorWriteIdx >> (8*i)) & 0xFF);
	}

	if(MMC_SUCCESS == mmcWriteSector(SD_LOG_IDX_SECTOR , IdxSectorBuff)){
		return RTRN_OK;
	}
	else{
		return 	RTRN_ERR;
	}
}

/*! High level functions
 *
 * */
bool_t app_sd_log_isCardInserted(void){
	if(mmcPing() != MMC_SUCCESS){
		return FALSE;
	}
	else{
		return TRUE;
	}
}


int8_t app_sd_log_init(void){
	int8_t mmcStat = MMC_BLOCK_SET_ERROR ;
	uint8_t timeout = 0;
	int8_t error;

	/*! Pin conf
	 */

	// Chip Select
	GPIO_setOutputHighOnPin(SD_CS_BASE,SD_CS_PIN);
	GPIO_setAsOutputPin(SD_CS_BASE,SD_CS_PIN);

	// Card Detect
	GPIO_setAsInputPin(SD_CARD_DET_BASE,SD_CARD_DET_PIN);

	//Todo map CD IRQ on mmcPing

	// Init SPI Module
	error = hal_spi_b_init(MMC_SPEED_INIT, HAL_SPI_B_PHASE_0,HAL_SPI_B_POL_0, SD_SPI_BASE);

	if(!app_sd_log_isCardInserted()){
		return RTRN_NOT_FOUND;
	}

	//Initialisation of the MMC/SD-card
	while (mmcStat != MMC_SUCCESS){     // if return in not NULL an error did occur and the                                       // MMC/SD-card will be initialized again

		mmcStat = mmcInit();
		timeout++;
		if (timeout >= MMC_MAX_TRIES)    // Try 50 times till error
		{
			//printf ("No MMC/SD-card found!! %x\n", status);
			return RTRN_ERR;
		}
	}
	//Speed up SPI
	hal_spi_b_init(MMC_SPEED_MAX,HAL_SPI_B_PHASE_0,HAL_SPI_B_POL_0, SD_SPI_BASE);

	// Read the Card Size from the CSD Register
	app_sd_log_struc.cardSize =  mmcReadCardSize();
	app_sd_log_struc.sectorNB =  app_sd_log_struc.cardSize/SD_LOG_SECTOR_SIZE;

	//Feed app_sd_log_struc.sectorReadIdx & sectorWriteIdx
	error = app_sd_log_readIndexes();
	if( error != RTRN_OK){
		return RTRN_ERR;
	}

	//init sector buffer
	eptk_utils_bufferInit( &app_sd_log_struc.logBuffer_str , eptk_utils_buffer_a, SD_LOG_SECTOR_SIZE );

	if(MMC_SUCCESS == mmcGoIdle()){
		app_sd_log_struc.state_e = SD_STANDBY;
		return RTRN_OK;
	}
	else{
		return RTRN_ERR;
	}
}

int8_t app_sd_puts(int8_t mode, uint8_t* buff_ptr, uint16_t charNB){
	uint16_t availableSpace = 0;
	int8_t error = RTRN_OK, mmcStat=0;

	__disable_interrupt(); //TEST RSA20171004
	error = eptk_utils_bufferWrite(&app_sd_log_struc.logBuffer_str, buff_ptr, charNB);
	if(error == RTRN_FULL){
		availableSpace = eptk_utils_bufferGetAvailableSpace(&app_sd_log_struc.logBuffer_str);
		error = eptk_utils_bufferWrite(&app_sd_log_struc.logBuffer_str, buff_ptr, availableSpace);

		if(error != RTRN_OK){
			__enable_interrupt();
			return error;
		}

		//Check card presence before writing
		if(!app_sd_log_isCardInserted()){
			__enable_interrupt();
			return RTRN_NOT_FOUND;
		}


		//write sector on MMC
		mmcStat = mmcWriteSector(app_sd_log_struc.sectorWriteIdx, eptk_utils_buffer_a);
		if(mmcStat == MMC_SUCCESS){
			//update sector write idx and save it
			app_sd_log_struc.sectorWriteIdx++;
			if(app_sd_log_struc.sectorWriteIdx >= app_sd_log_struc.sectorNB){
				app_sd_log_struc.sectorWriteIdx = SD_LOG_FIRST_WRITE_SECTOR;
			}
			app_sd_log_writeIndexes();
			//re-init buffer
			eptk_utils_bufferReset(&app_sd_log_struc.logBuffer_str);

			//Debug
			/*uint8_t sectNbAscii[20] = {0};
			uint16_t sizeSectNb;
			sizeSectNb = snprintf(sectNbAscii, 20, "Sector %" PRIu32 "\r\n", app_sd_log_struc.sectorWriteIdx);
			eptk_utils_bufferWrite(&app_sd_log_struc.logBuffer_str, sectNbAscii, sizeSectNb);*/
		}
		else{
			__enable_interrupt();
			return mmcStat;
		}

		__enable_interrupt();
		//Then recursively write the rest
		return app_sd_puts(mode, buff_ptr + availableSpace, charNB - availableSpace);

	}

	else if(mode == SD_LOG_WRITE_NOW){

		//Check card presence before writing
		if(!app_sd_log_isCardInserted()){
			__enable_interrupt();
			return RTRN_NOT_FOUND;
		}

		*(app_sd_log_struc.logBuffer_str.tail_ptr) = '\0'; //Makes break char for printf functions

		mmcStat = mmcWriteSector(app_sd_log_struc.sectorWriteIdx, eptk_utils_buffer_a);

		if(mmcStat == MMC_SUCCESS){
			//app_sd_log_struc.sectorWriteIdx++;
			error = RTRN_OK;
			//app_sd_log_writeIndexes();
		}
	}
	__enable_interrupt();
	return error;
}

int8_t app_sd_log(int8_t mode, const char *fmt, ...){
	int8_t size = 0;

	if(app_sd_log_struc.state_e == SD_NOT_FOUND){
		return RTRN_NOT_FOUND;
	}
	else if(app_sd_log_struc.state_e == SD_UNINITIALIZED){
		app_console_log("SD\tInit\r\n");
		if(app_sd_log_init() != RTRN_OK){
			return RTRN_ERR;
		}
	}

	size += eptk_utils_hx8toAscii(eptk_utils_rtcTime_str.Hours, convertBuffer + size);
	*(convertBuffer + size++) = ':';
	size += eptk_utils_hx8toAscii(eptk_utils_rtcTime_str.Minutes, convertBuffer + size);
	*(convertBuffer + size++) = ':';
	size += eptk_utils_hx8toAscii(eptk_utils_rtcTime_str.Seconds, convertBuffer + size);
	*(convertBuffer + size++) = '\t';

	__VALIST args;
	va_start(args, fmt);
	size += vsnprintf(convertBuffer + size, APP_SD_LOG_MAX_FORMATED_SIZE - size, fmt, args);
	va_end(args);

	/* RETOUR de snprintf :
	 * APP_SD_LOG_MAX_FORMATED_SIZE doit contenir le 0
	 * snprintf retourne le nombre converti (sans compter le 0)
	 * donc size < APP_SD_LOG_MAX_FORMATED_SIZE
	 * si size >= APP_SD_LOG_MAX_FORMATED_SIZE c'est qu'il n'y avait pas la place
	 */
	if(size >= APP_SD_LOG_MAX_FORMATED_SIZE){
		return RTRN_FULL;
	}
	else{
		return app_sd_puts(mode, (uint8_t* ) convertBuffer, size);
	}
}

/*! getLogSize
 *  returns nb of written sectors on SD for log
 */
int32_t app_sd_log_getLogSizeOnSD(void){
	if(app_sd_log_struc.sectorReadIdx > app_sd_log_struc.sectorWriteIdx){
		return app_sd_log_struc.sectorNB - app_sd_log_struc.sectorReadIdx + app_sd_log_struc.sectorWriteIdx - 1;
	}
	else{
		return app_sd_log_struc.sectorWriteIdx - app_sd_log_struc.sectorReadIdx;
	}
}

int32_t app_sd_log_read(uint8_t* dest_buff_ptr, uint16_t sectorNB){
	uint16_t readSectors = 0;
	uint16_t newCharNb = 0;
	uint32_t backupReadIdx = app_sd_log_struc.sectorReadIdx;

	if(app_sd_log_struc.sectorReadIdx == app_sd_log_struc.sectorWriteIdx){
		if(eptk_utils_bufferGetCharNb(&app_sd_log_struc.logBuffer_str)){	/* Reste des caract�res dans le buff temporaire */
			newCharNb = eptk_utils_bufferFlush(&app_sd_log_struc.logBuffer_str, dest_buff_ptr, SD_LOG_SECTOR_SIZE);
			if( newCharNb <SD_LOG_SECTOR_SIZE){
				*(dest_buff_ptr + newCharNb) = '\0';
			}
			return 1;
		}
		return RTRN_EMPTY;
	}

	//Si on a fait un tour (read > write)
	if(app_sd_log_struc.sectorReadIdx > app_sd_log_struc.sectorWriteIdx){

		while(( app_sd_log_struc.sectorReadIdx <  app_sd_log_struc.sectorNB ) && (sectorNB-- > 0 )){
			if(mmcReadSector(app_sd_log_struc.sectorReadIdx, dest_buff_ptr) == MMC_SUCCESS){
				readSectors++;
				dest_buff_ptr++;
				app_sd_log_struc.sectorReadIdx++;

			}
			else{
				return RTRN_ERR;
			}
		}
		if(app_sd_log_struc.sectorReadIdx == app_sd_log_struc.sectorNB){
			//Reset read sector after reaching end
			app_sd_log_struc.sectorReadIdx = SD_LOG_FIRST_WRITE_SECTOR;
		}
		else{
			//app_sd_log_writeIndexes();
			app_sd_log_struc.sectorReadIdx = backupReadIdx;
			return readSectors;
		}

	}

	//S'il reste des secteurs � lire
	while(( app_sd_log_struc.sectorReadIdx < app_sd_log_struc.sectorWriteIdx )
			&& (sectorNB-- > 0 )){
		if(mmcReadSector(app_sd_log_struc.sectorReadIdx, dest_buff_ptr) == MMC_SUCCESS){
			readSectors++;
			app_sd_log_struc.sectorReadIdx++;
			dest_buff_ptr+=SD_LOG_SECTOR_SIZE;
		}
		else{
			//app_sd_log_writeIndexes();
			return RTRN_ERR;
		}
	}
	//app_sd_log_writeIndexes();

	return readSectors;
}

#ifdef DRIVERS_TEST_MODE
int8_t app_sd_log_test(void){
	char * testPhrase = "CLUE_SD_LOG_AUTOTEST";
	uint8_t rx_buffer[SD_LOG_SECTOR_SIZE] = {0};
	int8_t errorCode = RTRN_OK;
	int32_t readSectors  = 0;
	uint8_t c = 0;
	uint8_t size = 0;

	//Write to log
	errorCode = app_sd_puts(SD_LOG_WRITE_NOW, (uint8_t*) testPhrase, 20);
	if(errorCode != RTRN_OK){
		return RTRN_ERR; //Debug
	}
	//Get available log sectors
	size = app_sd_log_getLogSizeOnSD();

	//Read 1 sector
	readSectors = app_sd_log_read(rx_buffer, size);
	//Si probl�me de lecture
	if(readSectors < 0){
		_no_operation(); //Debug
	}
	//Compare written and read
	for(c=0 ; *(testPhrase + c) !=0 ; c++){
		if(rx_buffer[c]!=*(testPhrase + c)){
			return RTRN_ERR;
		}
	}
	return RTRN_OK;
}
#endif

/* Low level functions */

// Initialize MMC card
int8_t mmcInit(void)
{
	//raise CS and MOSI for 80 clock cycles
	//SendByte(0xff) 10 times with CS high
	//RAISE CS
	int i;

	// Port x Function           Dir       On/Off
	//         mmcCS         Out       0 - Active 1 - none Active
	//         Dout          Out       0 - off    1 - On -> init in SPI_Init
	//         Din           Inp       0 - off    1 - On -> init in SPI_Init
	//         Clk           Out       -                 -> init in SPI_Init
	//         mmcCD         In        0 - card inserted


	//initialization sequence on PowerUp
	SD_LOG_CS_HIGH();
	for(i=0;i<=9;i++){
		hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
	}


	return (mmcGoIdle());
}


// set MMC in Idle mode
int8_t mmcGoIdle()
{
	char response=0x01;
	SD_LOG_CS_LOW();

	//Send Command 0 to put MMC in SPI mode
	mmcSendCmd(MMC_GO_IDLE_STATE,0,0x95);
	//Now wait for READY RESPONSE
	if(mmcGetResponse()!=0x01){
		return MMC_INIT_ERROR;
	}

	while(response==0x01)
	{
		SD_LOG_CS_HIGH();
		hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		SD_LOG_CS_LOW();
		mmcSendCmd(MMC_SEND_OP_COND/*MMC_SEND_ACMD41*/,0x00/*0x40*/,0xff);
		response=mmcGetResponse();
	}
	SD_LOG_CS_HIGH();
	hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
	return (MMC_SUCCESS);
}

// mmc Get Response
uint8_t mmcGetResponse(void)
{
	//Response comes 1-8bytes after command
	//the first bit will be a 0
	//followed by an error code
	//data will be 0xff until response
	int i=0;

	uint8_t response;

	while(i<=64)
	{
		response=hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		if(response==0x00)
			break;
		if(response==0x01)
			break;
		i++;
	}
	return response;
}

uint8_t mmcGetXXResponse(const uint8_t resp)
{
	//Response comes 1-8bytes after command
	//the first bit will be a 0
	//followed by an error code
	//data will be 0xff until response
	int i=0;

	uint8_t response;

	while(i<=1000)
	{
		response=hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		if(response==resp)break;
		i++;
	}
	return response;
}

// Check if MMC card is still busy
int8_t mmcCheckBusy(void)
{
	//Response comes 1-8bytes after command
	//the first bit will be a 0
	//followed by an error code
	//data will be 0xff until response
	int i=0;

	int8_t response;
	int8_t rvalue;
	while(i<=64)
	{
		response=hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		response &= 0x1f;
		switch(response)
		{
		case 0x05: rvalue=MMC_SUCCESS;break;
		case 0x0b: return(MMC_CRC_ERROR);
		case 0x0d: return(MMC_WRITE_ERROR);
		default:
			rvalue = MMC_OTHER_ERROR;
			break;
		}
		if(rvalue==MMC_SUCCESS)break;
		i++;
	}
	i=0;
	do
	{
		response=hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		i++;
	}while(response==0);
	return response;
}
// The card will respond with a standard response token followed by a data
// block suffixed with a 16 bit CRC.

// read a size Byte big block beginning at the address.
int8_t mmcReadBlock(const uint32_t address, const uint32_t count, uint8_t *pBuffer)
{
	char rvalue = MMC_RESPONSE_ERROR;

	// Set the block length to read
	if (mmcSetBlockLength (count) == MMC_SUCCESS)   // block length could be set
	{
		// CS = LOW (on)
		SD_LOG_CS_LOW();
		// send read command MMC_READ_SINGLE_BLOCK=CMD17
		mmcSendCmd (MMC_READ_SINGLE_BLOCK,address, 0xFF);
		// Send 8 Clock pulses of delay, check if the MMC acknowledged the read block command
		// it will do this by sending an affirmative response
		// in the R1 format (0x00 is no errors)
		if (mmcGetResponse() == 0x00)
		{
			// now look for the data token to signify the start of
			// the data
			if (mmcGetXXResponse(MMC_START_DATA_BLOCK_TOKEN) == MMC_START_DATA_BLOCK_TOKEN)
			{
				// clock the actual data transfer and receive the bytes; spi_read automatically finds the Data Block
				hal_spi_b_readFrame(pBuffer, count, SD_SPI_BASE);
				// get CRC bytes (not really needed by us, but required by MMC)
				hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
				hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
				rvalue = MMC_SUCCESS;
			}
			else
			{
				// the data token was never received
				rvalue = MMC_DATA_TOKEN_ERROR;      // 3
			}
		}
		else
		{
			// the MMC never acknowledge the read command
			rvalue = MMC_RESPONSE_ERROR;          // 2
		}
	}
	else
	{
		rvalue = MMC_BLOCK_SET_ERROR;           // 1
	}
	SD_LOG_CS_HIGH();
	hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
	return rvalue;
}// mmc_read_block



//int8_t mmcWriteBlock (const unsigned long address)
int8_t mmcWriteBlock (const uint32_t address, const uint32_t count, uint8_t *pBuffer)
{
	int8_t rvalue = MMC_RESPONSE_ERROR;         // MMC_SUCCESS;
	//  int8_t c = 0x00;

	// Set the block length to read
	if (mmcSetBlockLength (count) == MMC_SUCCESS)   // block length could be set
	{
		// CS = LOW (on)
		SD_LOG_CS_LOW();
		// send write command
		mmcSendCmd (MMC_WRITE_BLOCK,address, 0xFF);

		// check if the MMC acknowledged the write block command
		// it will do this by sending an affirmative response
		// in the R1 format (0x00 is no errors)
		if (mmcGetXXResponse(MMC_R1_RESPONSE) == MMC_R1_RESPONSE)
		{
			hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
			// send the data token to signify the start of the data
			hal_spi_b_sendByte(0xfe, SD_SPI_BASE);
			// clock the actual data transfer and transmitt the bytes

			hal_spi_b_sendFrame(pBuffer, count, SD_SPI_BASE);

			// put CRC bytes (not really needed by us, but required by MMC)
			hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
			hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
			// read the data response xxx0<status>1 : status 010: Data accected, status 101: Data
			//   rejected due to a crc error, status 110: Data rejected due to a Write error.
			mmcCheckBusy();
			rvalue = MMC_SUCCESS;
		}
		else
		{
			// the MMC never acknowledge the write command
			rvalue = MMC_RESPONSE_ERROR;   // 2
		}
	}
	else
	{
		rvalue = MMC_BLOCK_SET_ERROR;   // 1
	}
	// give the MMC the required clocks to finish up what ever it needs to do
	//  for (i = 0; i < 9; ++i)
	//    spiSendByte(0xff);

	SD_LOG_CS_HIGH();
	// Send 8 Clock pulses of delay.
	hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
	return rvalue;
} // mmc_write_block


// send command to MMC
void mmcSendCmd (const int8_t cmd, uint32_t data, const int8_t crc)
{
	uint8_t frame[6];
	int8_t temp;
	int i;
	frame[0]=(cmd|0x40);
	for(i=3;i>=0;i--){
		temp=(int8_t)(data>>(8*i));
		frame[4-i]=(temp);
	}
	frame[5]=(crc);
	hal_spi_b_sendFrame(frame,6, SD_SPI_BASE);
}


//--------------- set blocklength 2^n ------------------------------------------------------
int8_t mmcSetBlockLength (const uint32_t blocklength)
{
	// CS = LOW (on)
	SD_LOG_CS_LOW();
	// Set the block length to read
	mmcSendCmd(MMC_SET_BLOCKLEN, blocklength, 0xFF);

	// get response from MMC - make sure that its 0x00 (R1 ok response format)
	if(mmcGetResponse()!=0x00)
	{ mmcInit();
	mmcSendCmd(MMC_SET_BLOCKLEN, blocklength, 0xFF);
	mmcGetResponse();
	}

	SD_LOG_CS_HIGH();

	// Send 8 Clock pulses of delay.
	hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);

	return MMC_SUCCESS;
} // Set block_length


// Reading the contents of the CSD and CID registers in SPI mode is a simple
// read-block transaction.
int8_t mmcReadRegister (const int8_t cmd_register, const uint8_t length, uint8_t *pBuffer)
{
	int8_t uc = 0;
	int8_t rvalue = MMC_TIMEOUT_ERROR;

	if (mmcSetBlockLength (length) == MMC_SUCCESS)
	{
		SD_LOG_CS_LOW ();
		// CRC not used: 0xff as last byte
		mmcSendCmd(cmd_register, 0x000000, 0xff);

		// wait for response
		// in the R1 format (0x00 is no errors)
		if (mmcGetResponse() == 0x00)
		{
			if (mmcGetXXResponse(0xfe)== 0xfe)
				for (uc = 0; uc < length; uc++)
					pBuffer[uc] = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);  //mmc_buffer[uc] = spiSendByte(0xff);
			// get CRC bytes (not really needed by us, but required by MMC)
			hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
			hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
			rvalue = MMC_SUCCESS;
		}
		else
			rvalue = MMC_RESPONSE_ERROR;
		// CS = HIGH (off)
		SD_LOG_CS_HIGH();

		// Send 8 Clock pulses of delay.
		hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
	}
	SD_LOG_CS_HIGH();
	return rvalue;
} // mmc_read_register


//#include "math.h"
uint32_t mmcReadCardSize(void)
{
	// Read contents of Card Specific Data (CSD)

	unsigned long MMC_CardSize;
	unsigned short i,      // index
	j,      // index
	b,      // temporary variable
	response,   // MMC response to command
	mmc_C_SIZE = 0;

	uint8_t mmc_READ_BL_LEN = 0,  // Read block length
	mmc_C_SIZE_MULT = 0;

	SD_LOG_CS_LOW();

	hal_spi_b_sendByte(MMC_READ_CSD, SD_SPI_BASE);   // CMD 9
	for(i=4; i>0; i--)      // Send four dummy bytes
		hal_spi_b_sendByte(0, SD_SPI_BASE);
	hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);   // Send CRC byte

	response = mmcGetResponse();

	// data transmission always starts with 0xFE
	b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);

	if( !response )
	{
		while (b != 0xFE) b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		// bits 127:87
		for(j=5; j>0; j--)          // Host must keep the clock running for at
			b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);

		// 4 bits of READ_BL_LEN
		// bits 84:80
		b =hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);  // lower 4 bits of CCC and
		mmc_READ_BL_LEN = b & 0x0F;
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		// bits 73:62  C_Size
		// xxCC CCCC CCCC CC
		mmc_C_SIZE = (b & 0x03) << 10;
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		mmc_C_SIZE += b << 2;
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		mmc_C_SIZE += b >> 6;
		// bits 55:53
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		// bits 49:47
		mmc_C_SIZE_MULT = (b & 0x03) << 1;
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		mmc_C_SIZE_MULT += b >> 7;
		// bits 41:37
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
	}

	for(j=4; j>0; j--)          // Host must keep the clock running for at
		b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);  // least Ncr (max = 4 bytes) cycles after
	// the card response is received
	b = hal_spi_b_sendByte(DUMMY_CHAR, SD_SPI_BASE);
	SD_LOG_CS_LOW();

	MMC_CardSize = (mmc_C_SIZE + 1);
	// power function with base 2 is better with a loop
	// i = (pow(2,mmc_C_SIZE_MULT+2)+0.5);
	for(i = 2,j=mmc_C_SIZE_MULT+2; j>1; j--)
		i <<= 1;
	MMC_CardSize *= i;
	// power function with base 2 is better with a loop
	//i = (pow(2,mmc_READ_BL_LEN)+0.5);
	for(i = 2,j=mmc_READ_BL_LEN; j>1; j--)
		i <<= 1;
	MMC_CardSize *= i;

	return (MMC_CardSize);

}


int8_t mmcPing(void)
{
	if (!GPIO_getInputPinValue(SD_CARD_DET_BASE, SD_CARD_DET_PIN)){
		if(app_sd_log_struc.state_e == SD_NOT_FOUND){
			app_sd_log_struc.state_e = SD_UNINITIALIZED;
		}
		return (MMC_SUCCESS);
	}
	else{
		app_sd_log_struc.state_e = SD_NOT_FOUND;
		return (MMC_INIT_ERROR);
	}
}


#ifdef withDMA
#ifdef __IAR_SYSTEMS_ICC__
#pragma vector = DACDMA_VECTOR
__interrupt void DMA_isr(void)
#endif

#ifdef __TI_COMPILER_VERSION__
__interrupt void DMA_isr(void);
DMA_ISR(DMA_isr)
__interrupt void DMA_isr(void)
#endif
{
	DMA0CTL &= ~(DMAIFG);
	LPM3_EXIT;
}
#endif



