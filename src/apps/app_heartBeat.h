/*
 * app_heartBeat.h
 *
 *  Created on: 19 janv. 2017
 *      Author: romain
 */

#ifndef SRC_APPS_APP_HEARTBEAT_H_
#define SRC_APPS_APP_HEARTBEAT_H_

extern timer_str_t heartBeat_timer_struc;
extern volatile bool_t heartBeat_toDo_f;
extern volatile uint32_t app_heartbeatMode;
void heartBeatTimer_task(void);

#endif /* SRC_APPS_APP_HEARTBEAT_H_ */
