/*
 * app_ble.c
 *
 *  Created on: 6 janv. 2017
 *      Author: romain
 */


#include "driverlib.h"
#include "app_ble.h"
#include "../eptk_utils/uart.h"
#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include <stdio.h>
#include <stdarg.h>

#include "app_console.h"

/* Variables declaration */

//GLOBALS
eptk_utils_UART_str_t app_ble_uart_infos_str;
//LOCALS
//eptk_utils_UART_struc_t app_ble_uart_infos_str;
timer_str_t ble_todo_timer_str;
volatile bool_t app_ble_todo_f = FALSE;

int8_t app_ble_init(){

	int8_t error = RTRN_OK;

	error = eptk_utils_UART_Init(BLE_RX_BASE, BLE_RX_PIN, BLE_TX_PIN, &app_ble_uart_infos_str, app_ble_rx_callback,
						USCI_A_UART_CLOCKSOURCE_SMCLK, 162, 12, 0,
						USCI_A_UART_NO_PARITY, USCI_A_UART_LSB_FIRST, USCI_A_UART_ONE_STOP_BIT, USCI_A_UART_MODE, USCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION);

	if(error < RTRN_OK){
		return error;
	}

	//GPIO_setOutputHighOnPin(BLE_NRESET_BASE, BLE_NRESET_PIN);
	//GPIO_setAsOutputPin(BLE_NRESET_BASE, BLE_NRESET_PIN);

	// Configure UART pins
    GPIO_setOutputHighOnPin(BLE_OTA_BASE, BLE_OTA_PIN);
	GPIO_setAsOutputPin(BLE_OTA_BASE, BLE_OTA_PIN);

    GPIO_setOutputHighOnPin(BLE_AUTORUN_BASE, BLE_AUTORUN_PIN);
	GPIO_setAsOutputPin(BLE_AUTORUN_BASE, BLE_AUTORUN_PIN);

    GPIO_setOutputHighOnPin(BLE_PWR_EN_BASE, BLE_PWR_EN_PIN);
	GPIO_setAsOutputPin(BLE_PWR_EN_BASE, BLE_PWR_EN_PIN);


	/*	INIT Interruption */
	eptk_utils_UART_rx_on(&app_ble_uart_infos_str);

	//eptk_utils_sw_timer_delay_ms(1000);

    GPIO_setOutputHighOnPin(BLE_NRESET_BASE, BLE_NRESET_PIN);
	GPIO_setAsOutputPin(BLE_NRESET_BASE, BLE_NRESET_PIN);

	//eptk_utils_sw_timer_start(&ble_todo_timer_str, 5000, SW_TIMER_CONTINUOUS, NULL, &app_ble_todo_f, TRUE);

	return error;


}
void app_ble_task(void){
	if(app_ble_todo_f){
		app_ble_todo_f = FALSE;
		app_console_log("Test Auto \r\n");
		//eptk_utils_UART_puts(&ble_uart_infos,UART_SEND_NOW,"Coucou !\r\n", sizeof("Coucou !\r\n");
		//eptk_utils_UART_send(&app_ble_uart_infos_str,UART_SEND_NOW,"Coucou !\r\n");
	}
}

int8_t app_ble_rx_callback(uint8_t ble_rawchar){

	int8_t error = RTRN_OK;
	eptk_utils_bufferPush(&app_ble_uart_infos_str.RX_buffer_struc,ble_rawchar);

	app_console_todo_f = TRUE;
	app_ble_todo_f = TRUE;
	//Routine d'interruption RX
			//if(!lora_msgToRead_f){

			/*DEBUG CHAR BY CHAR*/
			/*if(eptk_utils_bufferPush(&lora_rx_buff_struc, lora_rawchar)
						== !RTRN_FULL){
					eptk_utils_bufferReset(&lora_rx_buff_struc);
				}
				else{
					lora_msgToRead_f = TRUE;
				}*/

//			if(lora_rawchar == LORA_PRM_END_CHAR){
//				app_lora_data_struc.msgToRead_f = TRUE;
//				if(eptk_utils_bufferPush(&lora_rx_buff_struc, '\0')
//						== RTRN_FULL){
//					eptk_utils_bufferReset(&lora_rx_buff_struc);
//				}
//				//LP_MODE_EXIT();
//			}
//			else if(eptk_utils_bufferPush(&lora_rx_buff_struc, lora_rawchar)
//					== RTRN_FULL){
//				eptk_utils_bufferReset(&lora_rx_buff_struc);
//			}


			/*if(lora_recepIndex < LORA_PRM_SIZE_RX_BUFFER){ //BUFF NOT FULL
					lora_rx_buff[lora_recepIndex] = EUSCI_A_UART_receiveData(EUSCI_A1_BASE);
					if(lora_rx_buff[lora_recepIndex] == LORA_PRM_END_CHAR){
						lora_msgToRead_f = TRUE;
						lora_rx_buff[++lora_recepIndex] = '\0';
					}
					else{
						lora_recepIndex++;
					}
			}
			else{
				lora_recepIndex = 0;
			}
		}*/
	return error;
}
