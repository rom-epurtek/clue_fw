/*
 * app_stateMachine.c
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */

#include "driverlib.h"
#include "app_stateMachine.h"

#include "../conf/bsp.h"
#include "../conf/app_conf.h"

#include "app_acc.h"
#include "app_alim.h"
#include "app_ble.h"
#include "app_bme280.h"
#include "app_console.h"
#include "app_gps.h"
#include "app_heartBeat.h"
#include "app_lora.h"
#include "app_mox.h"
#include "app_sd_log.h"
#include "app_usb.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes

#if defined(__GNUC__)
__attribute__ ((section(".infoA")))
#elif defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma DATA_SECTION(id_str, ".infoA")
#endif
struct {
	uint8_t SN[11];
} flash_id_str ;

struct {
	uint8_t SN[11];
} id_str =
{SN : "NOSN",
};

enum  {
	STEADY = 0,
	MOVING,
} acc_state_e;

enum {
	SLEEP = 0,
	WORK,
} main_state_e;

//State machine timers
timer_str_t stateMachine_movingTimer_str = {0}, stateMachine_mox_timer_str = {0};
bool_t stateMachineAlimCritical_f = FALSE;
void app_stateMachine_gotoWorkMode(){
	int8_t errorCode;

	app_heartbeatMode = HEARTBEAT_TIME_OFF_FAST;
	//Start GPS
	errorCode=app_gps_setON();
	if(RTRN_OK != errorCode){
		app_console_log("GPS Set On Err %d\r\n", errorCode);
	}

	app_gps_setLPM(FALSE);
	//Ask for RTC Update
	app_gps_data_str.toDoUpdateRTC_f = TRUE;

	//BME START
	app_bme280_start();

	//Update STATE
	main_state_e = WORK;
	app_heartbeatMode = HEARTBEAT_TIME_OFF_FAST;
	app_console_log("GO WORK MODE\r\n");

}

void app_stateMachine_gotoSleepMode(){
	app_console_log("GO TO SLEEP\r\n");
	//Set GPS LPM
	app_gps_setLPM(TRUE);
	//Stop BME280
	app_bme280_stop();
	//Stop MOX
	eptk_utils_sw_timer_disable(&stateMachine_mox_timer_str);
	app_mox_measureAutoCancel();
	//Update STATE
	main_state_e = SLEEP;

	app_heartbeatMode = HEARTBEAT_TIME_OFF_LONG;
}

int8_t app_stateMachine_init(){
	uint8_t strSNLen = 0;

	//Check SN length
	while((strSNLen < 13) && (flash_id_str.SN[strSNLen])){
		strSNLen++;
	}

	if(strSNLen < 13){
		strncpy(id_str.SN, flash_id_str.SN, strSNLen + 1);
	}

	app_console_log("START\t%s\tv%s\tbuild %s\r\n",id_str.SN, VERSION_STRING, COMPIL_TIME_STRING);

	//Log on sd at start
	app_sd_log(SD_LOG_WRITE_NOW, "\t\t\t\t\t\t\t\tSTART %s v%s build %s\r\n",id_str.SN, VERSION_STRING, COMPIL_TIME_STRING);

	//Start State
	main_state_e = SLEEP;
	app_acc_startMvtDetectMode();

	return RTRN_OK;
}
int8_t app_stateMachine_task(){

	//Check ACCELERO state
	if(STEADY == acc_state_e){
		//Transition
		if(app_acc_data_str.mvtDetected_f){
			app_acc_data_str.mvtDetected_f = FALSE;
			acc_state_e = MOVING;
			app_console_log("ACC\tMOVING\r\n");
			eptk_utils_sw_timer_start(&stateMachine_movingTimer_str, ACC_MVMT_CHECK_PERIOD, SW_TIMER_CONTINUOUS, NULL, NULL, FALSE);
		}
	}
	else if(MOVING == acc_state_e){
		if(stateMachine_movingTimer_str.isFinished){
			stateMachine_movingTimer_str.isFinished = FALSE;
			if(!app_acc_data_str.mvtDetected_f){
				eptk_utils_sw_timer_disable(&stateMachine_movingTimer_str);
				app_console_log("ACC\tSTEADY\r\n");
				acc_state_e = STEADY;
			}
			else{
				app_acc_data_str.mvtDetected_f = FALSE;
			}
		}

		switch(main_state_e){
		case SLEEP:
			//Check BATT CRITICAL to set GPS OFF properly
			if((app_alim_data_str.battCharge_percent <= 5) && !stateMachineAlimCritical_f){
				stateMachineAlimCritical_f = TRUE;
				app_gps_setOFF();
				app_sd_log_writeCurrentSector();
				app_console_log("Alim critical shutdown : %dmV\r\n", app_alim_data_str.battCharge_mV);
			}

			//Transition
			//Si on bouge
			else if((MOVING == acc_state_e) && !stateMachineAlimCritical_f){
				//Si pas branch�, on peut sortir du sleep
				if(!app_alim_data_str.usbPW){
					//DEBUG_LED_OFF(LED_GREEN);
					//Si niveau de batterie suffisant
					if(app_alim_data_str.battCharge_percent > ALIM_BATT_OK_THRESH){
						stateMachineAlimCritical_f = FALSE;
						app_stateMachine_gotoWorkMode();
					}
				}
			}

			break;
		case WORK:
			//Log if new data
			if(app_mox_data_str.dataCOReadyToWrite_f
					&& app_mox_data_str.dataNOXReadyToWrite_f
					&& app_gps_data_str.newData2log_f
					&& app_bme280_data_str.newData_f){
				//Clear flags
				app_mox_data_str.dataCOReadyToWrite_f = FALSE;
				app_mox_data_str.dataNOXReadyToWrite_f = FALSE;
				app_gps_data_str.newData2log_f = FALSE;
				app_bme280_data_str.newData_f = FALSE;
				//Write SD
				app_sd_log(SD_LOG_WRITE_WHEN_FULL,
						"%" PRId32	 //GPS Lat i32
						"\t%" PRId32 //GPS Long i32
						"\t%" PRIu32 //BME Press u32
						"\t%" PRId32 //BME Temp s32
						"\t%" PRIu32 //BME Hum u32
						"\t%" PRIu8  //BAT % u8
						"\t%" PRIu16 //BAT V u16
						"\t%" PRIu32 //MOX Nox R u32
						"\t%" PRIu32 //MOX CO R u32
						"\r\n",
						app_gps_data_str.posLat,
						app_gps_data_str.posLong,
						app_bme280_data_str.comp_press_u32,
						app_bme280_data_str.comp_temp_s32,
						app_bme280_data_str.comp_humidity_u32,
						app_alim_data_str.battCharge_percent,
						app_alim_data_str.battCharge_mV,
						app_mox_data_str.nox_value,
						app_mox_data_str.co_value
				);
				app_console_log(		"%" PRId32    //GPS Lat i32
						"\t%" PRId32  //GPS Long i32
						"\t%" PRId32  //BME Press u32
						"\t%" PRId32  //BME Temp s32
						"\t%" PRId32  //BME Hum u32
						"\t%" PRId8   //BAT % u8
						"\t%" PRId16  //BAT V u16
						"\t%" PRId32  //MOX Nox R u32
						"\t%" PRId32 //MOX CO R u32
						"\r\n",
						app_gps_data_str.posLat,
						app_gps_data_str.posLong,
						app_bme280_data_str.comp_press_u32,
						app_bme280_data_str.comp_temp_s32,
						app_bme280_data_str.comp_humidity_u32,
						app_alim_data_str.battCharge_percent,
						app_alim_data_str.battCharge_mV,
						app_mox_data_str.nox_value,
						app_mox_data_str.co_value
				);
			}

			//Check GPS to launch/stop MOX timer on first fix
			if(app_gps_data_str.navValid_f && !stateMachine_mox_timer_str.isActive){
				//Launch MOX timer
				eptk_utils_sw_timer_start(&stateMachine_mox_timer_str,
						MOX_LAUNCH_MEAS_PERIOD_MS, //1 min
						SW_TIMER_CONTINUOUS,
						NULL,
						NULL,
						0);
				//Launch MOX measure (available 30sec later)
				app_mox_measureAuto(MOX_HEAT_PERIOD_MS, MOX_NOX + MOX_CO);
			}
			else if(stateMachine_mox_timer_str.isFinished){
				stateMachine_mox_timer_str.isFinished = FALSE;
				//Launch MOX measure (available 30sec later)
				app_mox_measureAuto(MOX_HEAT_PERIOD_MS, MOX_NOX + MOX_CO);
			}
			else if(!app_gps_data_str.navValid_f && stateMachine_mox_timer_str.isActive){
				//Stop timer
				eptk_utils_sw_timer_disable(&stateMachine_mox_timer_str);
				//Stop MOXs
				app_mox_measureAutoCancel();
			}


			// Transitions
			//Si on ne bouge plus
			if(STEADY == acc_state_e){
				app_stateMachine_gotoSleepMode();
			}
			//Si batterie low
			else if(app_alim_data_str.battCharge_percent < ALIM_BATT_LOW_THRESH){
				app_stateMachine_gotoSleepMode();
			}
			//Si USB branch�, on part en sleep
			else if(app_alim_data_str.usbPW){
				app_stateMachine_gotoSleepMode();

			}
			break;
		default:break;
		}
	}
	return RTRN_SLEEP_OK;
}
/*if(STEADY == acc_state_e){

		//Transition
		if(app_acc_data_str.mvtDetected_f){
			app_acc_data_str.mvtDetected_f = FALSE;
			app_sd_log(SD_LOG_WRITE_WHEN_FULL, "ACC\tMOVING\r\n");
			app_console_log("ACC\tMOVING\r\n");
			acc_state_e = MOVING;
			eptk_utils_sw_timer_start(&stateMachine_movingTimer_str, ACC_MVMT_CHECK_PERIOD, SW_TIMER_CONTINUOUS, NULL, NULL, FALSE);
			eptk_utils_sw_timer_start(&stateMachine_lora_timer_str, LORA_PERIOD, SW_TIMER_CONTINUOUS, NULL, NULL, FALSE);
			eptk_utils_sw_timer_start(&stateMachine_mox_timer_str, MOX_PERIOD, SW_TIMER_CONTINUOUS, NULL, NULL, FALSE);
			app_heartbeatMode = HEARTBEAT_TIME_OFF_FAST;
			app_gps_setON();
			app_gps_setLPM(FALSE);
			//Debug
			//app_lora_start();
		}
	}
	else if(MOVING == acc_state_e){

		if(app_gps_data_str.navValid_f && app_gps_data_str.newData2log_f){
			app_gps_data_str.newData2log_f = FALSE;
			app_sd_log(SD_LOG_WRITE_WHEN_FULL,"GPS\tLT:%ld\tLG:%ld\tSV:%d\r\n",app_gps_data_str.posLat, app_gps_data_str.posLong, app_gps_data_str.SV);
		}
		//LoRa
		if(stateMachine_lora_timer_str.isFinished){
			stateMachine_lora_timer_str.isFinished = FALSE;

			//Send Serial number
			uint8_t sn_strlen = strlen(id_str.SN);
			if(sn_strlen < 10){
				app_lora_radio_printf(UART_SEND_NORMAL, id_str.SN);
			}
			//Send GPS data
			if(app_gps_data_str.navValid_f){
				app_lora_radio_printf(UART_SEND_NORMAL,
						"\tGPS\tLT:%ld,LG:%ld",app_gps_data_str.posLat, app_gps_data_str.posLong);
			}
			app_lora_radio_puts(&lora_ctr, UART_SEND_NOW,1);
			lora_ctr++;

			//Log on SD
			app_sd_log(SD_LOG_WRITE_WHEN_FULL,"LORA\tTX\r\n");
		}

		//MOX auto measure
		if(stateMachine_mox_timer_str.isFinished){
			stateMachine_mox_timer_str.isFinished = FALSE;

			//Start automatic measure
			app_console_log("MOX\tNOXBEFORE\t%"PRIu32"Ohms\r\n", app_mox_data_str.nox_value);
			app_sd_log(SD_LOG_WRITE_WHEN_FULL,"MOX\tNOXBEFORE\t%"PRIu32"Ohms\t%"PRIu32"\r\n", app_mox_data_str.nox_value, app_mox_data_str.nox_sensing_res);
			app_mox_measureAuto(MOX_AUTO_HEAT_DURATION, MOX_NOX);
		}
		//MOX NOX write SD
		if(app_mox_data_str.pvt_dataNOXReadyToWrite_f){
			app_mox_data_str.pvt_dataNOXReadyToWrite_f = FALSE;

			//Log on SD
			app_console_log("MOX\tNOXAFTER\t%"PRIu32"Ohms\r\n", app_mox_data_str.nox_value);
			app_sd_log(SD_LOG_WRITE_WHEN_FULL,"MOX\tNOXAFTER\t%"PRIu32"Ohms\r\n", app_mox_data_str.nox_value);
		}

		//Transition
		if(stateMachine_movingTimer_str.isFinished){
			stateMachine_movingTimer_str.isFinished = FALSE;

			if(!app_acc_data_str.mvtDetected_f){
				eptk_utils_sw_timer_disable(&stateMachine_movingTimer_str);
				eptk_utils_sw_timer_disable(&stateMachine_lora_timer_str);
				eptk_utils_sw_timer_disable(&stateMachine_mox_timer_str);

				app_heartbeatMode = HEARTBEAT_TIME_OFF_LONG;

				if(app_gps_data_str.navValid_f){

				}
				app_gps_setLPM(TRUE);
				//app_gps_setOFF();
				app_sd_log(SD_LOG_WRITE_NOW, "ACC\tSTEADY\r\n"); //Write current sector
				app_console_log("ACC\tSTEADY\r\n");
				acc_state_e = STEADY;
			}
			else{
				app_acc_data_str.mvtDetected_f = FALSE;

				//Debug
				//ACC read
				int8_t acc_data_temp[6];
				app_acc_read(ACC_REG_OUT_X_L, 6, (uint8_t *) acc_data_temp);
				app_console_log("X : %"PRId8" | Y : %"PRId8" | Z : %"PRId8"\r\n",
						acc_data_temp[1],acc_data_temp[3],acc_data_temp[5]
				);

			}
		}
	}*/
//	return RTRN_SLEEP_OK;




