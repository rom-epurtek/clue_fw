/**
 * \file app_alim.h
 *  \date 03 mar. 2017
 *  \author: RS
 *  \brief Power supply initialization and management functions and data structures.
 *
 */

#ifndef SRC_APPS_APP_ALIM_H_
#define SRC_APPS_APP_ALIM_H_

/**
 * \addtogroup PowerSupplyManagement
 * \brief Power supply management
 * @{
 */

#include "../eptk_utils/eptk_utils.h"

/** \brief Initialize data structure, GPIOs and timer to call periodically app_alim_task().
 * \return ERROR CODE : eptk_utils_sw_timer_start() error code
 */
int8_t app_alim_init(void);
/**
 * \brief Power supply management task
 * Refresh data struct
 */
int8_t app_alim_task(void);

/*Inputs*/
#define FTDI_nPWREN			FTDI_BATT_CHG_BASE, FTDI_BATT_CHG_PIN
#define ALIM_SIG_nPG		ALIM_SIG_PG_BASE, ALIM_SIG_PG_PIN
#define ALIM_SIG_STAT1		ALIM_SIG_STAT1_BASE, ALIM_SIG_STAT1_PIN
#define ALIM_SIG_STAT2		ALIM_SIG_STAT2_BASE, ALIM_SIG_STAT2_PIN
#define ALIM_BATT			ALIM_BATT_BASE, ALIM_BATT_PIN

//Donn�es sur https://www.ordinoscope.net/index.php/Mod%C3%A9lisme/Connaissance/Piles_et_accus/Li-Po
#define ALIM_BATT_0PRCT         3000u
#define ALIM_BATT_5PRCT         3300u
#define ALIM_BATT_10PRCT        3600u
#define ALIM_BATT_20PRCT        3700u
#define ALIM_BATT_30PRCT        3750u
#define ALIM_BATT_40PRCT        3790u
#define ALIM_BATT_50PRCT        3830u
#define ALIM_BATT_60PRCT        3870u
#define ALIM_BATT_70PRCT        3920u
#define ALIM_BATT_80PRCT        3970u
#define ALIM_BATT_90PRCT        4100u
#define ALIM_BATT_100PRCT       4170u

/* Output */
#define ALIM_SIG_PROG2		ALIM_SIG_PROG2_BASE, ALIM_SIG_PROG2_PIN

/** \brief Enumeration of possible battery STATE. Informations are given by battery charger chip signals, updated in app_alim_task().
 */
typedef enum{
	BATT_DISCONNECTED = 0,
	BATT_FAULT,
	BATT_LOW,
	BATT_IDLE,
	BATT_CHARGING,
	BATT_CHARGE_COMPLETE,
}battState_e_t;

/** \brief Structure containing power supply informations
 */
typedef struct{
	bool_t pwGood_f; /*!< \brief Power Supply ok flag (PGOOD) */
	battState_e_t battState_e; /*!< \brief Battery state : see enum battState_e_t */
	uint8_t battCharge_percent; /*!< \brief Battery charge [%] */
	uint16_t battCharge_mV; /*!< \brief Battery charge [mV] */
	bool_t newBattLvl_f; /*!< \brief New Battery charge measure is available. Must be reset by user */
	bool_t usbPW; /*!< \brief Power provided by USB */
} app_alim_state_str_t;

/**
 * \brief Global Power Supply management data structure
 * Holds Power supply state data. Handled by app_alim_task fn
 */
extern app_alim_state_str_t app_alim_data_str;

/**
 * Close group
 * @}
 */

#endif /* SRC_APPS_APP_ALIM_H_ */
