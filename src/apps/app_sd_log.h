/**
 * \file app_sd_log.h
 *	\date 15 juil. 2016
 *  \author: RS
 *  \brief SD card log system
 *  Includes raw SD card write and read functions. Wear leveling. No file system
 *
 *  \todo Handle runtime card insertion -> init (if state uninitialized) and removal to avoid pin checking at every call
 */

#ifndef APPS_APP_SD_LOG_H_
#define APPS_APP_SD_LOG_H_

/**
 * \addtogroup SD_Log
 * \brief SD Card logging management
 */

#include "driverlib.h"
#include "../eptk_utils/eptk_utils.h"


#define APP_SD_LOG_MAX_FORMATED_SIZE 128 /** \brief Buffer size for printf style  function app_sd_log(...)*/
#define SD_LOG_IDXS_START_IDX		0
#define SD_LOG_IDX_SECTOR 			0 /** \brief Sector used to store read and write indexes */
#define SD_LOG_INIT_KEY				"CLUE"
#define SD_LOG_INIT_KEY_IDX			(SD_LOG_IDXS_START_IDX + 8)
#define SD_LOG_IDX_LENGTH			(SD_LOG_INIT_KEY_IDX + sizeof(SD_LOG_INIT_KEY) + 1 )

#define SD_LOG_FIRST_WRITE_SECTOR 	1
#define SD_LOG_SECTOR_SIZE 			512UL //Bytes
#define MMC_MAX_TRIES 				50 // MMC init timeout (tries)
#define MMC_SPEED_INIT				400000

#if CLK_SMCLK_FREQ <= 25000000
	#define MMC_SPEED_MAX			CLK_SMCLK_FREQ
#else
	#define MMC_SPEED_MAX			25000000
#endif

#define SD_LOG_WRITE_NOW 	0
#define SD_LOG_WRITE_WHEN_FULL 	1

//#define MMC_BLOCK_LENGTH 512

typedef  enum {
  SD_UNINITIALIZED = 0,
  SD_NOT_FOUND 	= 1,
  SD_STANDBY  	= 2,
  SD_BUSY 		= 3,
} sd_log_state_e_t;

/** \brief SD card variables
 */
typedef struct{
	uint32_t cardSize;	/*!< \brief Card size in bytes */
	uint32_t sectorNB;	/*!< \brief Sector NB = Card size / sector size (512B) */
	uint32_t sectorWriteIdx; /*!< \brief Current block ready to be written in SD card */
	uint32_t sectorReadIdx; /*!< \brief Current block ready to be written in SD card */
	sd_log_state_e_t state_e; /*!< \brief SD card state (busy / standby) */
	eptk_utils_buffer_str_t logBuffer_str; /*!< \brief Handler for block size buffer */
}app_sd_log_state_str_t;

/* Variables declaration */
/* Global vars */
extern volatile app_sd_log_state_str_t app_sd_log_struc;

#define SD_LOG_CS_LOW()    GPIO_setOutputLowOnPin(SD_CS_BASE,SD_CS_PIN)             		 // Card Select
#define SD_LOG_CS_HIGH()   while(USCI_B_SPI_isBusy(SD_SPI_BASE)); GPIO_setOutputHighOnPin(SD_CS_BASE,SD_CS_PIN)    // Card Deselect

/*	High level functions */

/** \brief Init SD card, buffers and place in idle
 * 	\li Init MMC
 * 	\li Get write index from SD Card
 * 	\li Init buffer
 */
int8_t app_sd_log_init(void);

/** \brief Check card presence with Card dectect pin
 * \return TRUE if card inserted
 *
 */
bool_t app_sd_log_isCardInserted(void);

/** \brief Write N chars to SD Card (buffer or actual sector on MMC)
 * \param mode Mode :
 * \li SD_LOG_WRITE_NOW : writes to buffer and MMC sector but keeps pointing on it until full (used to avoid loss of data)
 * \li SD_LOG_WRITE_WHEN_FULL : only write in buffer until buffer is full
 * \param charNB Number of characters to write
 * \return ERROR CODE :
 * 	\li RTRN_OK			-1
 * 	\li RTRN_ERR		-2
 * 	\li RTRN_NOT_FOUND	-3
 * 	\li RTRN_FULL		-4
 * 	\li RTRN_EMPTY		-5
 * 	\li RTRN_BUSY		-6
 */
int8_t app_sd_puts(int8_t mode, uint8_t* buff_ptr, uint16_t charNB);

/** \brief Printf style function to write on SD card with format string
 * \param mode Mode :
 * \li SD_LOG_WRITE_NOW : writes to buffer and MMC sector but keeps pointing on it until full (used to avoid loss of data)
 * \li SD_LOG_WRITE_WHEN_FULL : only write in buffer until buffer is full
 * \param char *fmt Format string used in snprintf function
 */
int8_t app_sd_log(int8_t mode, const char *fmt, ...);

/** \brief Used to avoid loss of data if we plan to have a loss of power
 * Write sd_log buffer into MMC
 */
int8_t app_sd_log_writeCurrentSector(void);

/**
 * \brief Calculates log size in Bytes
 * \return Log size in bytes
 */
int32_t app_sd_log_getLogSizeOnSD(void);

/** \brief Flush SD log to dest_buff_ptr
 * \param uint8_t *dest_buff_ptr Pointer to destination buffer
 * \param uint16_t sectorNB Max sector number to read
 * \return Negative error code or read sector number
 */
int32_t app_sd_log_read(uint8_t* dest_buff_ptr, uint16_t sectorNB);

/** \brief SD Card auto test
 * SD card test. Write a sector, read back and compare.
 * Define \def DRIVERS_TEST_MODE to compile this function
 * /!\ Can damage actual data on SD card. DO NOT USE IN PRODUCTION
 * \return int8_t Error code :
 * \li RTRN_OK	-1
 * \li RTRN_ERR	-2
 */
int8_t app_sd_log_test(void);

/** \brief Write SD Card indexes to reserved block
 *
 */
int8_t app_sd_log_writeIndexes(void);

/**
 * @}
 */

/*! LOW LEVEL DEFINITIONS
 *
 */
// macro defines
#define HIGH(a) ((a>>8)&0xFF)               // high byte from word
#define LOW(a)  (a&0xFF)                    // low byte from word

#define DUMMY 0xff

// Tokens (necessary  because at NPO/IDLE (and CS active) only 0xff is on the data/command line)
#define MMC_START_DATA_BLOCK_TOKEN          0xfe   // Data token start byte, Start Single Block Read
#define MMC_START_DATA_MULTIPLE_BLOCK_READ  0xfe   // Data token start byte, Start Multiple Block Read
#define MMC_START_DATA_BLOCK_WRITE          0xfe   // Data token start byte, Start Single Block Write
#define MMC_START_DATA_MULTIPLE_BLOCK_WRITE 0xfc   // Data token start byte, Start Multiple Block Write
#define MMC_STOP_DATA_MULTIPLE_BLOCK_WRITE  0xfd   // Data toke stop byte, Stop Multiple Block Write


// an affirmative R1 response (no errors)
#define MMC_R1_RESPONSE       0x00

// this variable will be used to track the current block length
// this allows the block length to be set only when needed
// unsigned long _BlockLength = 0;

// error/success codes
#define MMC_SUCCESS           0x00
#define MMC_BLOCK_SET_ERROR   0x01
#define MMC_RESPONSE_ERROR    0x02
#define MMC_DATA_TOKEN_ERROR  0x03
#define MMC_INIT_ERROR        0x04
#define MMC_CRC_ERROR         0x10
#define MMC_WRITE_ERROR       0x11
#define MMC_OTHER_ERROR       0x12
#define MMC_TIMEOUT_ERROR     0xFF


// commands: first bit 0 (start bit), second 1 (transmission bit); CMD-number + 0ffsett 0x40
#define MMC_GO_IDLE_STATE          0x40     //CMD0
#define MMC_SEND_OP_COND           0x41     //CMD1
#define MMC_READ_CSD               0x49     //CMD9
#define MMC_SEND_CID               0x4a     //CMD10
#define MMC_STOP_TRANSMISSION      0x4c     //CMD12
#define MMC_SEND_STATUS            0x4d     //CMD13
#define MMC_SET_BLOCKLEN           0x50     //CMD16 Set block length for next read/write
#define MMC_READ_SINGLE_BLOCK      0x51     //CMD17 Read block from memory
#define MMC_READ_MULTIPLE_BLOCK    0x52     //CMD18
#define MMC_CMD_WRITEBLOCK         0x54     //CMD20 Write block to memory
#define MMC_WRITE_BLOCK            0x58     //CMD24
#define MMC_WRITE_MULTIPLE_BLOCK   0x59     //CMD25
#define MMC_WRITE_CSD              0x5b     //CMD27 PROGRAM_CSD
#define MMC_SET_WRITE_PROT         0x5c     //CMD28
#define MMC_CLR_WRITE_PROT         0x5d     //CMD29
#define MMC_SEND_WRITE_PROT        0x5e     //CMD30
#define MMC_TAG_SECTOR_START       0x60     //CMD32
#define MMC_TAG_SECTOR_END         0x61     //CMD33
#define MMC_UNTAG_SECTOR           0x62     //CMD34
#define MMC_TAG_EREASE_GROUP_START 0x63     //CMD35
#define MMC_TAG_EREASE_GROUP_END   0x64     //CMD36
#define MMC_UNTAG_EREASE_GROUP     0x65     //CMD37
#define MMC_EREASE                 0x66     //CMD38
#define MMC_READ_OCR               0x67     //CMD39
#define MMC_CRC_ON_OFF             0x68     //CMD40

//RS
#define MMC_SEND_ACMD41            0x69     //CMD41 //FOR SDHC ?


#endif /* APPS_APP_SD_LOG_H_ */
