/*
 * buffers.h
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */

#ifndef EPTK_UTILS_BUFFERS_H_
#define EPTK_UTILS_BUFFERS_H_

#include "driverlib.h"
#include "eptk_utils.h"
/*! Utilitaire de gestion de buffers
 *
 *
 */

/*! Buffer Struct def
 *
 */
typedef struct {
	uint8_t* begin_ptr;
	uint8_t* end_ptr;
	uint8_t* head_ptr;
	uint8_t* tail_ptr;
	uint16_t size;
	bool_t buffFull_f;
}eptk_utils_buffer_str_t;

/*! eptk_buffer_getLastChar
 *  @param
 *  @return
 */
int8_t eptk_utils_buffer_getLastChar(eptk_utils_buffer_str_t* bufferStruc_ptr);

/*! eptk_buffers_createBuffer
 *  @param
 *  @return
 */
int8_t eptk_utils_bufferInit(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t* bufferBegin_ptr, uint16_t bufferSize);

/*! eptk_buffers_createGetCharNb
 *  @param
 *  @return
 */
uint16_t eptk_utils_bufferGetCharNb(eptk_utils_buffer_str_t* bufferStruc_ptr);

/*! eptk_buffers_createPop
 *  @param
 *  @return
 */
uint8_t eptk_utils_bufferPop(eptk_utils_buffer_str_t* bufferStruc_ptr);

/*! eptk_buffers_bufferPopLast
 *  @param
 *  @return
 */
uint8_t eptk_utils_bufferPopLast(eptk_utils_buffer_str_t* bufferStruc_ptr);
/*! eptk_buffers_createPush
 *  @param
 *  @return
 */
int8_t eptk_utils_bufferPush(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t newChar);

/*! eptk_buffers_createFlush
 *  @param
 *  @return
 */
uint16_t eptk_utils_bufferFlush(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t* dest_ptr, uint16_t maxCharNb);

/*! eptk_buffers_Reset
 *  @param
 *  @return
 */
int16_t eptk_utils_bufferReset(eptk_utils_buffer_str_t* bufferStruc_ptr);

/*! eptk_buffers_createWrite
 *  @param
 *  @return
 */
int8_t eptk_utils_bufferWrite(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t* src_ptr, uint16_t charNB);

/*! eptk_utils_bufferGetAvailableSpace
 *  @param
 *  @return*/
uint16_t eptk_utils_bufferGetAvailableSpace(eptk_utils_buffer_str_t* bufferStruc_ptr);

uint8_t eptk_utils_htoi (const char *ptr);
uint8_t eptk_utils_hx8toAscii(uint8_t hexVal, char* dest_str);
uint8_t eptk_utils_decToBCD8(uint8_t dec);
uint16_t eptk_utils_decToBCD16(uint16_t dec);
#endif /* EPTK_UTILS_BUFFERS_H_ */
