/*
 * buffers.c
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */
#include "eptk_utils.h"

/*Private fns*/



/*Public functions*/
int8_t eptk_utils_buffer_getLastChar(eptk_utils_buffer_str_t* bufferStruc_ptr){
	if(bufferStruc_ptr->head_ptr == bufferStruc_ptr->tail_ptr){
		return RTRN_NOT_FOUND;
	}
	if(bufferStruc_ptr->tail_ptr == bufferStruc_ptr->begin_ptr)//Go get last char of array
	{
		return *(bufferStruc_ptr->end_ptr);
	}
	else{
		return *( bufferStruc_ptr->head_ptr - 1 );
	}

}

uint16_t eptk_utils_bufferGetAvailableSpace(eptk_utils_buffer_str_t* bufferStruc_ptr){

	int16_t space = bufferStruc_ptr->head_ptr - bufferStruc_ptr->tail_ptr;

	if(space == 0){
		return bufferStruc_ptr->size;
	}

	if(bufferStruc_ptr->head_ptr < bufferStruc_ptr->tail_ptr){
		space = bufferStruc_ptr->size + space;
	}

	return space;
}

int8_t eptk_utils_bufferInit(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t* bufferBegin_ptr, uint16_t bufferSize){
	int8_t error = RTRN_OK;

	bufferStruc_ptr->begin_ptr = bufferBegin_ptr;
	bufferStruc_ptr->head_ptr = bufferBegin_ptr;
	bufferStruc_ptr->tail_ptr = bufferBegin_ptr;
	bufferStruc_ptr->end_ptr = bufferBegin_ptr + bufferSize - 1;
	bufferStruc_ptr->size = bufferSize;
	bufferStruc_ptr->buffFull_f = FALSE;

	return error;
}

int16_t eptk_utils_bufferReset(eptk_utils_buffer_str_t* bufferStruc_ptr){

	bufferStruc_ptr->head_ptr = bufferStruc_ptr->begin_ptr;
	bufferStruc_ptr->tail_ptr = bufferStruc_ptr->begin_ptr;
	bufferStruc_ptr->buffFull_f = FALSE;

	return RTRN_OK;
}

uint16_t eptk_utils_bufferGetCharNb(eptk_utils_buffer_str_t* bufferStruc_ptr){

	int16_t charNb = bufferStruc_ptr->tail_ptr - bufferStruc_ptr->head_ptr;

	if(bufferStruc_ptr->tail_ptr < bufferStruc_ptr->head_ptr){
		charNb = bufferStruc_ptr->size + charNb;
	}

	return charNb;
}

uint8_t eptk_utils_bufferPop(eptk_utils_buffer_str_t* bufferStruc_ptr){
	uint8_t returnValue = 0;

	//Teste si le buffer est vide
	if(bufferStruc_ptr->head_ptr == bufferStruc_ptr->tail_ptr){
		//returnValue = RTRN_EMPTY;
	}
	else{
		returnValue = *(bufferStruc_ptr->head_ptr);

		if(bufferStruc_ptr->head_ptr == bufferStruc_ptr->end_ptr){
			bufferStruc_ptr->head_ptr = bufferStruc_ptr->begin_ptr;
		}
		else{
			bufferStruc_ptr->head_ptr++;
		}

		bufferStruc_ptr->buffFull_f = FALSE;
	}

	return returnValue;
}

uint8_t eptk_utils_bufferPopLast(eptk_utils_buffer_str_t* bufferStruc_ptr){
	uint8_t returnCh;
	if(!eptk_utils_bufferGetCharNb(bufferStruc_ptr)){
		return 0;
	}
	else if(bufferStruc_ptr->buffFull_f){
		bufferStruc_ptr->buffFull_f = FALSE;
	}
	returnCh = *(bufferStruc_ptr->tail_ptr);
	if(bufferStruc_ptr->tail_ptr == bufferStruc_ptr->begin_ptr){
		bufferStruc_ptr->tail_ptr = bufferStruc_ptr->end_ptr;
	}
	else{
		bufferStruc_ptr->tail_ptr--;
	}

	return returnCh;
}

int8_t eptk_utils_bufferPush(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t newChar){

	int8_t error = RTRN_OK;

	//Teste si le buffer est plein
	if(bufferStruc_ptr->buffFull_f){
		error = RTRN_FULL;
	}
	else{
		*(bufferStruc_ptr->tail_ptr) = newChar;

		}
		if(bufferStruc_ptr->tail_ptr + 1 == bufferStruc_ptr->head_ptr){
			bufferStruc_ptr->buffFull_f = TRUE;
		}
		else if((bufferStruc_ptr->head_ptr == bufferStruc_ptr->begin_ptr) && (bufferStruc_ptr->tail_ptr == bufferStruc_ptr->end_ptr)){
			bufferStruc_ptr->buffFull_f = TRUE;
		}
		else{
			bufferStruc_ptr->tail_ptr++;
			if(bufferStruc_ptr->tail_ptr > bufferStruc_ptr->end_ptr){
				bufferStruc_ptr->tail_ptr = bufferStruc_ptr->begin_ptr;
		}
	}

	return error;
}

uint16_t eptk_utils_bufferFlush(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t* dest_ptr, uint16_t maxCharNb){
	int8_t newChar;
	uint16_t copiedCharNb = 0;

	while(copiedCharNb < maxCharNb){
		newChar = eptk_utils_bufferPop(bufferStruc_ptr);
		if(newChar > RTRN_OK){
			*dest_ptr = (uint8_t) newChar;
			dest_ptr++;
			copiedCharNb++;
		}
		else{
			return copiedCharNb;
		}
	}
	return copiedCharNb;
}

int8_t eptk_utils_bufferWrite(eptk_utils_buffer_str_t* bufferStruc_ptr, uint8_t* src_ptr, uint16_t charNB){
	int8_t error = RTRN_OK;
	uint16_t copiedChars = 0;

	if(eptk_utils_bufferGetAvailableSpace(bufferStruc_ptr) < charNB){
		error = RTRN_FULL;
	}
	else{
		while(copiedChars++ < charNB){
			eptk_utils_bufferPush(bufferStruc_ptr, *src_ptr);
			src_ptr++;
		}
	}

	return error;
}

/*Helpers*/
//Hex ascii to bin
uint8_t eptk_utils_htoi (const char *ptr)
{
uint8_t value = 0;
char ch = *ptr;
uint8_t chCount = 0;

    while(chCount < 2) {
        ch = *(ptr + chCount++);

        if (ch >= '0' && ch <= '9')
            value = (value << 4) + (ch - '0');
        else if (ch >= 'A' && ch <= 'F')
            value = (value << 4) + (ch - 'A' + 10);
        else if (ch >= 'a' && ch <= 'f')
            value = (value << 4) + (ch - 'a' + 10);
    }
        return value;
}

//8B Hex to ascii
uint8_t eptk_utils_hx8toAscii(uint8_t hexVal, char* dest_str){
	uint8_t digitNb = 2;
	uint8_t quartet, size =0;
	while(digitNb--){
		quartet = (hexVal>>(4*digitNb)) & 0x0F;
		if(quartet < 0x0A){
			*(dest_str++) = quartet + '0';
		}
		else{
			*(dest_str++) = quartet - 0x0A + 'A';
		}
		size++;
	}
	return size;
}

//8Bdec to BCD
uint8_t eptk_utils_decToBCD8(uint8_t dec){
	return (((dec / 10) % 10) << 4) + ((dec % 10)) ;
}

uint16_t eptk_utils_decToBCD16(uint16_t dec){
	return (((dec / 1000) % 10) << 12) + (((dec / 100) % 10)<<8) + (((dec / 10) % 10) << 4) + (dec % 10);

}
