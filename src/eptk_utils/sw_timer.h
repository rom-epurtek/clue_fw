#ifndef SW_TIMERS_H
#define SW_TIMERS_H

#include "driverlib.h"
#include "typedefs.h"

/*SW TIMER PARAMS*/
#define SW_TIMER_HW_BASE 			TIMER_A1_BASE
#define SW_TIMER_HW_CCR0_VECTOR		TIMER1_A0_VECTOR 			//TA1_CCR0 IFG (seule interruption sur ce vecteur)
#define SW_TIMER_HW_CLK_SRC 		TIMER_A_CLOCKSOURCE_ACLK 	//32768 Hz
#define SW_TIMER_HW_CLK_DIVIDER		TIMER_A_CLOCKSOURCE_DIVIDER_2
#define SW_TIMER_HW_PERIOD_MS		10							//~10ms
#define SW_TIMER_HW_COMPARE_VALUE	164						//(32768/2 / PERIOD_MS) = 164 -> 10ms period
#define SW_TIMERS_MAX_NUM 			20

#define SW_TIMER_CONTINUOUS  1
#define SW_TIMER_SINGLE_SHOT 0

typedef struct {
	volatile bool_t isContinuous;
	volatile bool_t isFinished;
	volatile bool_t isActive;
	volatile uint32_t timerCount;
	volatile uint32_t timerRestart;
	void (*callback_fn_ptr)(); //Callback fin timer -> isr
	volatile bool_t* flag2modify_ptr;
	volatile bool_t newFlagValue;
	volatile bool_t isAllocated;
	volatile bool_t idx;
}timer_str_t;

typedef timer_str_t* timer_str_ptr_t;

/*Privates
void eptk_utils_sw_timer_stopGlobalTimer(void);
void eptk_utils_sw_timer_startGlobalTimer(void);*/

int8_t eptk_utils_sw_timer_init(void);

int8_t eptk_utils_sw_timer_start(timer_str_ptr_t timer_str_ptr,
								uint32_t time,
								bool_t isContinuous,
								void (*fptr_function)(),
								volatile bool_t* flag2modify_ptr,
								bool_t newVal);

int8_t eptk_utils_sw_timer_delete(timer_str_ptr_t timer_str_ptr);

void eptk_utils_sw_timer_disable(timer_str_ptr_t timer_str_ptr);

int8_t eptk_utils_sw_timer_enable(timer_str_ptr_t timer_str_ptr);

int8_t eptk_utils_sw_timer_clear(timer_str_ptr_t timer_str_ptr);

int8_t eptk_utils_sw_timer_toggle(timer_str_ptr_t timer_str_ptr);

void eptk_utils_sw_timer_forceFinish(timer_str_ptr_t timer_str_ptr);

bool_t eptk_utils_sw_timer_isFinished(timer_str_ptr_t timer_str_ptr); // return 1 if finished

bool_t eptk_utils_sw_timer_isActive(timer_str_ptr_t timer_str_ptr);

void eptk_utils_sw_timer_delay_ms(uint32_t time_ms);

#endif


