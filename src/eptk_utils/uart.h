/*
 * uart.h
 *
 *  Created on: 19 d�c. 2016
 *      Author: Polo
 */

#ifndef SRC_EPTK_UTILS_UART_H_
#define SRC_EPTK_UTILS_UART_H_

#include "eptk_utils.h"
#include "buffers.h"

//Nombre d'interface UART sur le microcontroleur
#define EPTK_UTILS_UART_NB_INTERFACE				4

#define EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE		512
#define EPTK_UTILS_UART_BUFFER_SIZE					512

#define UART_SEND_NOW		1	//L'envoie est d�clench� apr�s chaque appel de eptk_utils_UART_send
#define UART_SEND_NORMAL	2	//L'envoi est g�r� et d�clench� par le uart_task

/*! \struct eptk_utils_UART_struc_t uart.h
 *	\brief Structure made to contain all useful information to call other uart function
 */
typedef struct {
	volatile bool_t trans_f;
	volatile bool_t tx_end_f;
	volatile bool_t wait_sending_f;
	volatile bool_t wait_reading_f;
	uint16_t interface;
	eptk_utils_buffer_str_t TX_buffer_struc;
	uint8_t TX_buffer[EPTK_UTILS_UART_BUFFER_SIZE + 1]; // +1 pour le '\0'
	eptk_utils_buffer_str_t RX_buffer_struc;
	uint8_t RX_buffer[EPTK_UTILS_UART_BUFFER_SIZE + 1]; // +1 pour le '\0'
} eptk_utils_UART_str_t;

/** FONCTIONS GLOBALES **/
int8_t eptk_utils_UART_task();

void eptk_utils_UART_rx_on(eptk_utils_UART_str_t* uart_infos);
void eptk_utils_UART_rx_off(eptk_utils_UART_str_t* uart_infos);

int8_t eptk_utils_UART_printf(eptk_utils_UART_str_t* uart_infos, int8_t mode, const char *fmt, ...);
uint8_t  eptk_utils_UART_Init(uint8_t port, uint8_t TXpin, uint8_t RXpin, eptk_utils_UART_str_t* uart_infos, int8_t (*rx_callback)(uint8_t),
							uint8_t clockSource, uint16_t clockPrescalar, uint8_t firstModReg,
							uint8_t secondModReg, uint8_t parity, uint16_t msborLsbFirst, uint16_t numberofStopBits,
							uint16_t uartMode, uint8_t overSampling);

/** FONCTIONS LOCALES **/
int8_t eptk_utils_UART_puts(eptk_utils_UART_str_t* uart_infos, int8_t mode, uint8_t* buff_ptr);
int8_t eptk_utils_UART_sendMsg(uint8_t* msg, eptk_utils_UART_str_t* uart_infos,  uint16_t charNB);

#endif /* SRC_EPTK_UTILS_UART_H_ */
