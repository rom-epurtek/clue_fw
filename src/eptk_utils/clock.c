/**
 * \file clock.c
 * \brief Clock management custom functions
 * \date 6 juil. 2016
 * \author RS
 */

#include "clock.h"
#include "driverlib.h"
#include "../conf/app_conf.h"
#include "typedefs.h"

volatile Calendar eptk_utils_rtcTime_str = {
		Seconds : 0,
		Minutes : 0,
		Hours : 0,
		DayOfWeek : 4,
		DayOfMonth : 1,
		Month : 1,
		Year : 0x2016,
};
volatile Calendar eptk_utils_rtcTimeNEW_str = {0};

uint16_t status;
bool_t result_b = STATUS_SUCCESS;

void eptk_utils_rtcTimeUpdate(void){
	//Init RTC_A Clock in Calendar mode
	RTC_A_initCalendar(RTC_A_BASE,
			&eptk_utils_rtcTimeNEW_str,
			RTC_A_FORMAT_BCD);
	//Enable interrupt for RTC Ready Status, which asserts when the RTC
	//Calendar registers are ready to read.
	//Also, enable interrupts for the Calendar alarm and Calendar event.
	RTC_A_clearInterrupt(RTC_A_BASE,RTCRDYIFG);
	RTC_A_enableInterrupt(RTC_A_BASE,RTCRDYIE);

	//Start RTC Clock
	RTC_A_startClock(RTC_A_BASE);
}

void eptk_utils_initClock(void)
{
	/**
	 * see http://processors.wiki.ti.com/index.php/MSP430_FAQ#Why_should_it_be_possible_to_run_5xx.2F6xx_UCS_DCO_clock_to_frequency_higher_than_25_MHz.3F
	 *
	 * \todo dans la machine � �tats, g�rer une horloge plus rapide quand on est sur USB et activer la console (#PWREN...)
	 * \todo handle LFXT and HFXT
	 * */
	//Set VCore
	if(STATUS_SUCCESS != PMM_setVCore(CLK_PMM_CORE_LEVEL)){
		SW_RESET();
	}

	/* Init DCO if no HF crystal */
#ifndef CLK_HFXT_FREQ


	//Set DCO FLL reference = REFO
	UCS_initClockSignal(
			UCS_FLLREF,
			UCS_REFOCLK_SELECT,
			UCS_CLOCK_DIVIDER_1
	);
#else

	//Initializes the XT1 and XT2 crystal frequencies being used
	UCS_setExternalClockSource(
			CLK_LFXT_FREQ,
			CLK_HFXT_FREQ
	);

	//Startup HF XT2 crystal Port select XT2
	GPIO_setAsPeripheralModuleFunctionInputPin(
			GPIO_PORT_P5,
			GPIO_PIN2 + GPIO_PIN3
	);

	// Enable global oscillator fault flag
	//SFR_clearInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
	//SFR_enableInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);

	//Initialize XT2. Returns STATUS_SUCCESS if initializes successfully
	result_b = UCS_turnOnXT2WithTimeout(
			UCS_XT2_DRIVE_24MHZ_32MHZ,
			UCS_XT2_TIMEOUT
	);
#endif
#ifdef CLK_LFXT_FREQ
    //Port select XT1
    GPIO_setAsPeripheralModuleFunctionInputPin(
        GPIO_PORT_P7,
        GPIO_PIN0 + GPIO_PIN1
        );
	//Initialize XT1. Returns STATUS_SUCCESS if initializes successfully
	result_b = UCS_turnOnLFXT1WithTimeout(
			UCS_XT1_DRIVE_0,
			UCS_XCAP_3,
			UCS_XT1_TIMEOUT
	);
	if(result_b == STATUS_SUCCESS){
		///Set ACLK = LXFT
		UCS_initClockSignal(
				UCS_ACLK,
				UCS_XT1CLK_SELECT,
				CLK_ACLK_DIVIDER
		);
	}
	else{
		///Set ACLK = REFO
		UCS_initClockSignal(
				UCS_ACLK,
				CLK_ACLK_BASE,
				CLK_ACLK_DIVIDER
		);
	}
#else
	///Set ACLK = REFO
	UCS_initClockSignal(
			UCS_ACLK,
			CLK_ACLK_BASE,
			CLK_ACLK_DIVIDER
	);
#endif


	///Set SMCLK = DCO
	UCS_initClockSignal(
			UCS_SMCLK,
			CLK_SMCLK_BASE,
			CLK_SMCLK_DIVIDER
	);

	if(result_b == STATUS_SUCCESS){
		///Set MCLK = DCO
		UCS_initClockSignal(
				UCS_MCLK,
				CLK_MCLK_BASE,
				CLK_MCLK_DIVIDER
		);
	}

#ifndef CLK_HFXT_FREQ
	///Set Ratio and Desired MCLK Frequency  and initialize DCO
	UCS_initFLLSettle(
			CLK_FLL_FREQ_IN_KHZ,
			CLK_FLL_REF_RATIO
	);
#endif

	//Init RTC_A Clock in Calendar mode
	RTC_A_initCalendar(RTC_A_BASE,
			&eptk_utils_rtcTime_str,
			RTC_A_FORMAT_BCD);

	//Enable interrupt for RTC Ready Status, which asserts when the RTC
	//Calendar registers are ready to read.
	//Also, enable interrupts for the Calendar alarm and Calendar event.
	RTC_A_clearInterrupt(RTC_A_BASE,RTCRDYIFG);
	RTC_A_enableInterrupt(RTC_A_BASE,RTCRDYIE);

	//Start RTC Clock
	RTC_A_startClock(RTC_A_BASE);

#ifdef DRIVERS_TEST_MODE
	//ACLK, MCLK, SMCLk set out to pins
	GPIO_setAsPeripheralModuleFunctionOutputPin(
			GPIO_PORT_P11,
			/*GPIO_PIN0 +*/ GPIO_PIN1  + GPIO_PIN2
	);
	UCS_getSMCLK();
	UCS_getACLK();
	UCS_getMCLK();
#endif

}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=RTC_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(RTC_VECTOR)))
#endif
void RTC_A_ISR(void)
{
	switch(__even_in_range(RTCIV,16))
	{
	case RTCIV_NONE: break;      		/* No Interrupt pending */
	case RTCIV_RTCRDYIFG:             	/* RTC ready: RTCRDYIFG */
		//Update rtc time
		eptk_utils_rtcTime_str = RTC_A_getCalendarTime(RTC_A_BASE);
		eptk_utils_rtcTime_str.Seconds = HWREG8(RTC_A_BASE + OFS_RTCTIM0_L);
		eptk_utils_rtcTime_str.Minutes = HWREG8(RTC_A_BASE + OFS_RTCTIM0_H);
		eptk_utils_rtcTime_str.Hours = HWREG8(RTC_A_BASE + OFS_RTCTIM1_L);
		eptk_utils_rtcTime_str.DayOfWeek = HWREG8(RTC_A_BASE + OFS_RTCTIM1_H);
		eptk_utils_rtcTime_str.DayOfMonth = HWREG8(RTC_A_BASE + OFS_RTCDATE_L);
		eptk_utils_rtcTime_str.Month = HWREG8(RTC_A_BASE + OFS_RTCDATE_H);
		eptk_utils_rtcTime_str.Year = HWREG16(RTC_A_BASE + OFS_RTCYEAR);
		break;

	case RTCIV_RTCTEVIFG:break;       	/* RTC interval timer: RTCTEVIFG */
	case RTCIV_RTCAIFG:break;         	/* RTC user alarm: RTCAIFG */
	case RTCIV_RT0PSIFG: break;       	/* RTC prescaler 0: RT0PSIFG */
	case RTCIV_RT1PSIFG: break;       	/* RTC prescaler 1: RT1PSIFG */
	case 12: break;     //Reserved
	case 14: break;     //Reserved
	case 16: break;     //Reserved
	default: break;
	}
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=UNMI_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(UNMI_VECTOR)))
#endif
void NMI_ISR(void)
{
	do
	{
		// If it still can't clear the oscillator fault flags after the timeout,
		// trap and wait here.
		status = UCS_clearAllOscFlagsWithTimeout(1000);
	}
	while(status != 0);
}

