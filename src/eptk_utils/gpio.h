/*
 * gpio.h
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */

#ifndef EPTK_UTILS_GPIO_H_
#define EPTK_UTILS_GPIO_H_

#include <driverlib.h>
#define GPIO_HIGH(x)			GPIO_setOutputHighOnPin(x)
#define GPIO_LOW(x)				GPIO_setOutputLowOnPin(x)
#define GPIO_OUTPUT(x)			GPIO_setAsOutputPin(x)
#define GPIO_INPUT(x)			GPIO_setAsInputPin(x)
#define GPIO_INPUT_PULLUP(x) 	GPIO_setAsInputPinWithPullUpResistor(x)
#define GPIO_INPUT_PULLDOWN(x) 	GPIO_setAsInputPinWithPullDownResistor(x)
#define GPIO_GET(x)				GPIO_getInputPinValue(x)


//*****************************************************************************
//
// The following are values that can be passed to the selectedPort parameter
// for functions: GPIO_setAsOutputPin(), GPIO_setAsInputPin(),
// GPIO_setAsPeripheralModuleFunctionOutputPin(),
// GPIO_setAsPeripheralModuleFunctionInputPin(), GPIO_setOutputHighOnPin(),
// GPIO_setOutputLowOnPin(), GPIO_toggleOutputOnPin(),
// GPIO_setAsInputPinWithPullDownResistor(),
// GPIO_setAsInputPinWithPullUpResistor(), GPIO_getInputPinValue(),
// GPIO_selectInterruptEdge(), GPIO_enableInterrupt(), GPIO_disableInterrupt(),
// GPIO_getInterruptStatus(), and GPIO_clearInterrupt().
//
//*****************************************************************************
#define GPIO_PORT_P1                                                          1
#define GPIO_PORT_P2                                                          2
#define GPIO_PORT_P3                                                          3
#define GPIO_PORT_P4                                                          4
#define GPIO_PORT_P5                                                          5
#define GPIO_PORT_P6                                                          6
#define GPIO_PORT_P7                                                          7
#define GPIO_PORT_P8                                                          8
#define GPIO_PORT_P9                                                          9
#define GPIO_PORT_P10                                                        10
#define GPIO_PORT_P11                                                        11
#define GPIO_PORT_PA                                                          1
#define GPIO_PORT_PB                                                          3
#define GPIO_PORT_PC                                                          5
#define GPIO_PORT_PD                                                          7
#define GPIO_PORT_PE                                                          9
#define GPIO_PORT_PF                                                         11
#define GPIO_PORT_PJ                                                         13

//*****************************************************************************
//
// The following are values that can be passed to the selectedPins parameter
// for functions: GPIO_setAsOutputPin(), GPIO_setAsInputPin(),
// GPIO_setAsPeripheralModuleFunctionOutputPin(),
// GPIO_setAsPeripheralModuleFunctionInputPin(), GPIO_setOutputHighOnPin(),
// GPIO_setOutputLowOnPin(), GPIO_toggleOutputOnPin(),
// GPIO_setAsInputPinWithPullDownResistor(),
// GPIO_setAsInputPinWithPullUpResistor(), GPIO_getInputPinValue(),
// GPIO_enableInterrupt(), GPIO_disableInterrupt(), GPIO_getInterruptStatus(),
// GPIO_clearInterrupt(), and GPIO_selectInterruptEdge() as well as returned by
// the GPIO_getInterruptStatus() function.
//
//*****************************************************************************
#define GPIO_PIN0                                                      (0x0001)
#define GPIO_PIN1                                                      (0x0002)
#define GPIO_PIN2                                                      (0x0004)
#define GPIO_PIN3                                                      (0x0008)
#define GPIO_PIN4                                                      (0x0010)
#define GPIO_PIN5                                                      (0x0020)
#define GPIO_PIN6                                                      (0x0040)
#define GPIO_PIN7                                                      (0x0080)
#define GPIO_PIN8                                                      (0x0100)
#define GPIO_PIN9                                                      (0x0200)
#define GPIO_PIN10                                                     (0x0400)
#define GPIO_PIN11                                                     (0x0800)
#define GPIO_PIN12                                                     (0x1000)
#define GPIO_PIN13                                                     (0x2000)
#define GPIO_PIN14                                                     (0x4000)
#define GPIO_PIN15                                                     (0x8000)
#define GPIO_PIN_ALL8                                                    (0xFF)
#define GPIO_PIN_ALL16                                                 (0xFFFF)



// Set all GPIO pins to output low to prevent floating input and reduce power consumption
void eptk_utils_gpio_reset();
//Sets isr fn ptr
int8_t eptk_utils_gpio_setIsr(uint8_t port_base, uint8_t bit, void (*callback_fn_ptr)());



#endif /* EPTK_UTILS_GPIO_H_ */
