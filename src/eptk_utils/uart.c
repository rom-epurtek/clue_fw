/**
 * \file uart.c
 * \brief UART configuration and management functions
 * \date 19 dec. 2016
 * \author PDE
 */


//Permet l'utilisation du driverlib UART

#include "typedefs.h"
#include "return_codes.h"
#include "driverlib.h"
#include "uart.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "../conf/app_conf.h"
#include "gpio.h"

int8_t (*USCI_A0_callback)(uint8_t);
int8_t (*USCI_A1_callback)(uint8_t);
int8_t (*USCI_A2_callback)(uint8_t);
int8_t (*USCI_A3_callback)(uint8_t);

/** LOCAL **/
//Tableau de pointeurs sur les structures g�rant les interface UART
eptk_utils_UART_str_t* eptk_utils_UART_data2Send[EPTK_UTILS_UART_NB_INTERFACE] = {0};

eptk_utils_UART_str_t** eptk_utils_UART_data2SendIndex_head_ptr = eptk_utils_UART_data2Send;
eptk_utils_UART_str_t** eptk_utils_UART_data2SendIndex_tail_ptr = eptk_utils_UART_data2Send;


char uart_convertBuffer[EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE] = {0};

volatile bool_t* UART_usciA0_tx_end_f=0;
volatile bool_t* UART_usciA1_tx_end_f=0;
volatile bool_t* UART_usciA2_tx_end_f=0;
volatile bool_t* UART_usciA3_tx_end_f=0;

uint8_t uartA0_rawchar;
uint8_t uartA1_rawchar;
uint8_t uartA2_rawchar;
uint8_t uartA3_rawchar;


//TODO affecter les fonctions de callback

uint8_t eptk_utils_UART_Init(uint8_t port, uint8_t TXpin, uint8_t RXpin, eptk_utils_UART_str_t* uart_infos, int8_t (*rx_callback)(uint8_t),
		uint8_t clockSource, uint16_t clockPrescalar, uint8_t firstModReg,uint8_t secondModReg,
		uint8_t parity, uint16_t msborLsbFirst, uint16_t numberofStopBits,uint16_t uartMode, uint8_t overSampling){

	int8_t error = RTRN_OK;

	uart_infos->trans_f = FALSE;
	uart_infos->tx_end_f = FALSE;
	uart_infos->wait_sending_f = FALSE;
	uart_infos->wait_reading_f = FALSE;

	// Configure UART pins
	GPIO_setAsPeripheralModuleFunctionInputPin(port, RXpin);
	GPIO_setAsPeripheralModuleFunctionOutputPin(port, TXpin);

	USCI_A_UART_initParam param = {0};
	param.selectClockSource = clockSource;
	param.clockPrescalar = clockPrescalar;
	param.firstModReg = firstModReg;
	param.secondModReg = secondModReg;
	param.parity = parity;
	param.msborLsbFirst = msborLsbFirst;
	param.numberofStopBits = numberofStopBits;
	param.uartMode = uartMode;
	param.overSampling = overSampling;

	//Hardcoded call of ESCI A0, A1, A2 or A3 according to UART port value
	if(port == GPIO_PORT_P3){
		uart_infos->interface = USCI_A0_BASE;
		UART_usciA0_tx_end_f = &(uart_infos->tx_end_f);
		USCI_A0_callback = rx_callback;
	}
	else if(port == GPIO_PORT_P5){
		uart_infos->interface = USCI_A1_BASE;
		UART_usciA1_tx_end_f = &(uart_infos->tx_end_f);
		USCI_A1_callback = rx_callback;
	}
	else if(port == GPIO_PORT_P9){ //LORA
		uart_infos->interface = USCI_A2_BASE;
		UART_usciA2_tx_end_f = &(uart_infos->tx_end_f);
		USCI_A2_callback = rx_callback;
	}
	else if(port == GPIO_PORT_P10){
		uart_infos->interface = USCI_A3_BASE;
		UART_usciA3_tx_end_f = &uart_infos->tx_end_f;
		USCI_A3_callback = rx_callback;
	}

	if(STATUS_FAIL == USCI_A_UART_init(uart_infos->interface, &param))
	{
		error = RTRN_ERR;
	}

	//GPIO RESET
	/*GPIO_setOutputHighOnPin(GPIO_PORT_P3, GPIO_PIN3);
	GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN3);*/


	//Buffer initialisation
	eptk_utils_bufferInit(&(uart_infos->TX_buffer_struc), uart_infos->TX_buffer, EPTK_UTILS_UART_BUFFER_SIZE);
	eptk_utils_bufferInit(&(uart_infos->RX_buffer_struc), uart_infos->RX_buffer, EPTK_UTILS_UART_BUFFER_SIZE);

	USCI_A_UART_enable(uart_infos->interface);
	//Clear and enable TX int
	USCI_A_UART_clearInterrupt(uart_infos->interface, USCI_A_UART_TRANSMIT_INTERRUPT_FLAG); // Enable interrupt
	USCI_A_UART_enableInterrupt(uart_infos->interface, USCI_A_UART_TRANSMIT_INTERRUPT); // Enable interrupt

	return error;
}

void eptk_utils_UART_rx_on(eptk_utils_UART_str_t* uart_infos){
	USCI_A_UART_clearInterrupt(uart_infos->interface,USCI_A_UART_RECEIVE_INTERRUPT_FLAG);
	USCI_A_UART_enableInterrupt(uart_infos->interface,	USCI_A_UART_RECEIVE_INTERRUPT); // Enable interrupt
}

void eptk_utils_UART_rx_off(eptk_utils_UART_str_t* uart_infos){
	USCI_A_UART_disableInterrupt(uart_infos->interface,	USCI_A_UART_RECEIVE_INTERRUPT);
	USCI_A_UART_clearInterrupt(uart_infos->interface,USCI_A_UART_RECEIVE_INTERRUPT_FLAG);
}

int8_t eptk_utils_UART_task(){

	uint8_t newChar = 0;

	if(eptk_utils_UART_data2SendIndex_head_ptr < eptk_utils_UART_data2SendIndex_tail_ptr){
		newChar = eptk_utils_bufferPop(&((*eptk_utils_UART_data2SendIndex_head_ptr)->TX_buffer_struc));
		if(newChar){
			eptk_utils_UART_sendMsg(&newChar,*eptk_utils_UART_data2SendIndex_head_ptr, 1);
		}
		else{
			//eptk_utils_UART_data2Send[eptk_utils_UART_data2SendIndex]->wait_sending_f = FALSE;
			(*eptk_utils_UART_data2SendIndex_head_ptr)->wait_sending_f = FALSE;
			eptk_utils_UART_data2SendIndex_head_ptr++;
			if(eptk_utils_UART_data2SendIndex_head_ptr == eptk_utils_UART_data2SendIndex_tail_ptr){
				eptk_utils_UART_data2SendIndex_tail_ptr = eptk_utils_UART_data2Send;
				eptk_utils_UART_data2SendIndex_head_ptr = eptk_utils_UART_data2Send;
			}
		}
		return RTRN_BUSY;
	}
	else{
		return RTRN_SLEEP_OK;
	}
}


int8_t eptk_utils_UART_printf(eptk_utils_UART_str_t* uart_infos, int8_t mode, const char *fmt, ...){
	int8_t size = 0;

	__VALIST args;
	va_start(args, fmt);
	size = vsnprintf(uart_convertBuffer, EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE, fmt, args);
	va_end(args);

	/* RETOUR de snprintf :
	 * APP_UART_LOG_MAX_FORMATED_SIZE doit contenir le 0
	 * snprintf retourne le nombre converti (sans compter le 0)
	 * donc size < APP_UART_LOG_MAX_FORMATED_SIZE
	 * si size >= APP_UART_LOG_MAX_FORMATED_SIZE c'est qu'il n'y avait pas la place
	 */
	if(size >= EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE){
		return RTRN_FULL;
	}
	else{
		return eptk_utils_UART_puts(uart_infos, mode, (uint8_t* ) uart_convertBuffer);
	}

}

int8_t eptk_utils_UART_puts(eptk_utils_UART_str_t* uart_infos_ptr, int8_t mode, uint8_t* buff_ptr){

	uint16_t availableSpace = 0;
	int8_t error = RTRN_OK;
	uint16_t charNB = 0;
	/*uint8_t msgLength = 0;
	uint8_t msg[EPTK_UTILS_UART_LOG_MAX_FORMATED_SIZE];*/

	//Count chars
	charNB = strlen((char*)buff_ptr);
	charNB = 0;
	while( *(buff_ptr + charNB)){
		charNB++;
	}
	/*if(charNB){
		charNB--; //Escape Null char
	}*/

	if(mode == UART_SEND_NOW){

		//*(uart_infos->buffer_struc.tail_ptr) = '\0'; //Makes break char for printf functions

		/*
		 * 		On envoie les donn�es qu'on a dans le buffer
		 */

		eptk_utils_UART_sendMsg(buff_ptr, uart_infos_ptr, charNB);
		//Check d'erreur ?
	}
	else{
		error = eptk_utils_bufferWrite(&uart_infos_ptr->TX_buffer_struc, buff_ptr, charNB);
		if((error == RTRN_OK) && (eptk_utils_UART_data2SendIndex_tail_ptr < &eptk_utils_UART_data2Send[EPTK_UTILS_UART_NB_INTERFACE])){
			//On l�ve un flag pour dire qu'on a des donn�es � envoyer au UART_task si ce n'�tait d�j� fait
			if(!uart_infos_ptr->wait_sending_f){
				*eptk_utils_UART_data2SendIndex_tail_ptr = uart_infos_ptr;
				eptk_utils_UART_data2SendIndex_tail_ptr++;
				uart_infos_ptr->wait_sending_f = TRUE;
				if(eptk_utils_UART_data2SendIndex_tail_ptr > &eptk_utils_UART_data2Send[EPTK_UTILS_UART_NB_INTERFACE - 1]){
					eptk_utils_UART_data2SendIndex_tail_ptr =  &eptk_utils_UART_data2Send[EPTK_UTILS_UART_NB_INTERFACE - 1];
				}
			}
		}
		else {
			error = RTRN_FULL;
		}
		if(error == RTRN_FULL){

			//Si on est full, on stock ce qu'on peut dans le buffer

			availableSpace = eptk_utils_bufferGetAvailableSpace(&uart_infos_ptr->TX_buffer_struc);
			error = eptk_utils_bufferWrite(&uart_infos_ptr->TX_buffer_struc, buff_ptr, availableSpace);

			if(error != RTRN_OK){
				return error;
			}
			/*
			 * 		On envoie les donn�es qu'on a dans le buffer
			 */

			/*msgLength = eptk_utils_bufferFlush(&(uart_infos_ptr->TX_buffer_struc), msg, msgLength);
			eptk_utils_UART_sendMsg(msg, uart_infos_ptr, charNB);

			//Then recursively write the rest
			return eptk_utils_UART_puts(uart_infos_ptr, mode, buff_ptr + availableSpace);*/

		}

	}

	return error;
}

int8_t eptk_utils_UART_sendMsg(uint8_t* msg, eptk_utils_UART_str_t* uart_infos,  uint16_t charNB){
	uint16_t i = 0;

	uart_infos->trans_f = TRUE;
	uart_infos->tx_end_f = FALSE;
	while(charNB-- > 0){
		// Load data onto buffer
		USCI_A_UART_transmitData(uart_infos->interface,msg[i]);
		while(!uart_infos->tx_end_f)
		{
			LP_MODE();
		}
		i++;
		uart_infos->tx_end_f = FALSE;

		//DEBUG TODO MOVE USED FOR LORA
		/*while(timeout--);
		timeout = 0xFFFF;*/
	} //TODO non bloquant

	uart_infos->tx_end_f = FALSE;
	uart_infos->trans_f = FALSE;

	return RTRN_OK;
}


//******************************************************************************
//
//This is the USCI_A0 interrupt vector service routine.
//
//******************************************************************************

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A0_VECTOR)))
#endif
void USCI_A0_ISR(void)
{
	switch(__even_in_range(UCA0IV,18))
	{
	case USCI_NONE: break;
	case USCI_UCRXIFG:
		(*USCI_A0_callback)(USCI_A_UART_receiveData(USCI_A0_BASE));
		LP_MODE_EXIT();
		break;
	case USCI_UCTXIFG:
		*UART_usciA0_tx_end_f = TRUE;
		LP_MODE_EXIT();
		break;
		/*case USCI_UART_UCSTTIFG:
    	break;
    case USCI_UART_UCTXIFG:
    	break;*/
	default:break;
	}
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A1_VECTOR)))
#endif
void USCI_A1_ISR(void)
{
	switch(__even_in_range(UCA1IV,4))
	{
	case USCI_NONE: break;
	case USCI_UCRXIFG:
		(*USCI_A1_callback)(USCI_A_UART_receiveData(USCI_A1_BASE));
		LP_MODE_EXIT();
		break;
	case USCI_UCTXIFG:
		*UART_usciA1_tx_end_f = TRUE;
		LP_MODE_EXIT();
		break;
		/*case USCI_UART_UCSTTIFG:
    	break;
    case USCI_UART_UCTXIFG:
    	break;*/
	default:break;
	}
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A2_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A2_VECTOR)))
#endif
void USCI_A2_ISR(void)
{
	switch(__even_in_range(UCA2IV,4))
	{
	case USCI_NONE: break;
	case USCI_UCRXIFG:
		(*USCI_A2_callback)(USCI_A_UART_receiveData(USCI_A2_BASE));
		LP_MODE_EXIT();
		//rCh = UCA2RXBUF;
		break;
	case USCI_UCTXIFG:
		*UART_usciA2_tx_end_f = TRUE;
		LP_MODE_EXIT();
		break;
		/*case USCI_UART_UCSTTIFG:
    	break;
    case USCI_UART_UCTXIFG:
    	break;*/
	default:break;
	}
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A3_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A3_VECTOR)))
#endif
void USCI_A3_ISR(void)
{
	switch(__even_in_range(UCA3IV,18))
	{
	case USCI_NONE: break;
	case USCI_UCRXIFG:
		(*USCI_A3_callback)(USCI_A_UART_receiveData(USCI_A3_BASE));
		LP_MODE_EXIT();
		break;
	case USCI_UCTXIFG:
		*UART_usciA3_tx_end_f = TRUE;
		LP_MODE_EXIT();
		break;
		/*case USCI_UART_UCSTTIFG:
    	break;
    case USCI_UART_UCTXIFG:
    	break;*/
	default:break;
	}
}
