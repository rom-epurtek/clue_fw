/*
 * adc.h
 *
 *  Created on: 13 f�vr. 2017
 *      Author: Paul
 */

#ifndef SRC_EPTK_UTILS_ADC_H_
#define SRC_EPTK_UTILS_ADC_H_

#include "driverlib.h"
#include "../eptk_utils/eptk_utils.h"

#define EPTK_UTILS_ADC_NBCHAN		16
#define NB_CONV_BUFF_MAX			12
#define ADC_FS_70PERCENT			2867 //4096*0.7
#define ADC_FS_20PERCENT			819 //4096*0.2

typedef struct{
	uint8_t inputNo;
	bool_t* flag_ptr;
}adc_convParam_str_t;

int8_t eptk_utils_adc_init();

int8_t eptk_utils_adc_startConversion(uint8_t inputNo, bool_t* flag);


#endif /* SRC_EPTK_UTILS_ADC_H_ */
