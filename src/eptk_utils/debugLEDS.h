/*
 * debugLEDS.h
 *
 *  Created on: 20 juil. 2016
 *      Author: romain
 */

#ifndef EPTK_UTILS_DEBUGLEDS_H_
#define EPTK_UTILS_DEBUGLEDS_H_

#include "driverlib.h"
#include "../conf/bsp.h"
#include "../conf/app_conf.h"

#ifdef USE_DEBUG_LEDS
	#define DEBUG_LED_OFF(x) GPIO_setOutputHighOnPin(x)
	#define DEBUG_LED_ON(x)  GPIO_setOutputLowOnPin(x)
	#define DEBUG_LED_TOGGLE(x) GPIO_toggleOutputOnPin(x)
#else
	#define DEBUG_LED_ON(x) _no_operation();
	#define DEBUG_LED_OFF(x) _no_operation();
#endif

#define DEBUG_LED_IS_ON(x) !GPIO_getInputPinValue(x)

void eptk_utils_debugLEDS_init(void);

#endif /* EPTK_UTILS_DEBUGLEDS_H_ */
