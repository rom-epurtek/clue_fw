#ifndef RETURNCODES_
#define RETURNCODES_

	#define RTRN_SLEEP_OK	 0
	#define RTRN_OK			-1
	#define RTRN_ERR		-2
	#define RTRN_NOT_FOUND	-3
	#define RTRN_FULL		-4
	#define RTRN_EMPTY		-5
	#define RTRN_BUSY		-6

	
#endif /*RETURNCODES_*/
