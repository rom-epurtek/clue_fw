/*
 * adc.c
 *
 *  Created on: 13 févr. 2017
 *      Author: Paul
 */


#include "driverlib.h"
#include "adc.h"

#include "../eptk_utils/eptk_utils.h" //For typedef and RTRN codes
#include "../conf/bsp.h"
#include "../conf/app_conf.h"
#include "../apps/app_console.h"


bool_t* interruptFlags[EPTK_UTILS_ADC_NBCHAN] = {FALSE};
bool_t runningConversion_f = {FALSE};
uint32_t* clear_int_addr;
adc_convParam_str_t convBuff_str_a[NB_CONV_BUFF_MAX];
uint8_t convBuffIdx = 0;

int8_t eptk_utils_adc_init(){
	int8_t error = RTRN_OK;

	//Initialize ADC
	//Clock period ACLK : 32768Hz -> 30.5µs
	ADC12_A_init(ADC12_A_BASE, ADC12_A_SAMPLEHOLDSOURCE_SC, ADC12_A_CLOCKSOURCE_ACLK, ADC12_A_CLOCKDIVIDER_1);
	ADC12_A_enable(ADC12_A_BASE);

	/*Calcul nombre de cycles
	 * MSP430x5xx User Guide 28.2.5.3 :
	 * tsample > (Rs + 1.8 kΩ) × ln(2n+1) × 25 pF + 800 ns
	 * avec RsMax (CO) = 1.5M(Rco) + 2.1M(Rpont)
	 * => tsample > 587µs
	 * => nbCycles > 587/30.5
	 * => nbCycles > 20
	 * => nbCycles = 32, tsample = 976µs */
	ADC12_A_setupSamplingTimer(ADC12_A_BASE, ADC12_A_CYCLEHOLD_32_CYCLES, ADC12_A_CYCLEHOLD_32_CYCLES, ADC12_A_MULTIPLESAMPLESDISABLE);

	return error;
}


int8_t eptk_utils_adc_startConversion(uint8_t inputNo, bool_t* flag_ptr){
	int8_t error = RTRN_OK;
	ADC12_A_configureMemoryParam param = {0};
	if(runningConversion_f){
		if(convBuffIdx >= NB_CONV_BUFF_MAX){
			app_console_log("ADC Buffer full\r\n");
			error = RTRN_BUSY;
		}
		else{
			convBuff_str_a[convBuffIdx].inputNo = inputNo;
			convBuff_str_a[convBuffIdx].flag_ptr = flag_ptr;
			convBuffIdx = convBuffIdx + 1;
		}
	}
	else{
		if((inputNo >= 0) && (inputNo < EPTK_UTILS_ADC_NBCHAN) && (flag_ptr != NULL)){

			runningConversion_f = TRUE;
			interruptFlags[inputNo] = flag_ptr;
			ADC12_A_disableConversions(ADC12_A_BASE, ADC12_A_COMPLETECONVERSION);

			param.memoryBufferControlIndex = inputNo;
			param.inputSourceSelect = inputNo;
			param.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
			param.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
			param.endOfSequence = ADC12_A_NOTENDOFSEQUENCE;
			ADC12_A_configureMemory(ADC12_A_BASE ,&param);

			ADC12_A_clearInterrupt(ADC12_A_BASE, 1 << inputNo);
			ADC12_A_enableInterrupt(ADC12_A_BASE, 1 << inputNo);

			ADC12_A_startConversion(ADC12_A_BASE, inputNo, ADC12_A_SINGLECHANNEL);
		}
		else{
			error = RTRN_ERR;
		}
	}

	return error;
}



#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC12_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(ADC12_VECTOR)))
#endif
void ADC12ISR (void){
	int8_t int_idx = (__even_in_range(ADC12IV,34) - 6)/2;
	switch(int_idx)
	{
	case 0:
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
	case 9:
	case 10:
	case 11:
	case 12:
	case 13:
	case 14:
	case 15:
		*(interruptFlags[int_idx]) = TRUE;
		runningConversion_f = FALSE;
		ADC12_A_clearInterrupt(ADC12_A_BASE, 1 << int_idx);
		while(convBuffIdx > 0){
			convBuffIdx = convBuffIdx - 1;
			eptk_utils_adc_startConversion(convBuff_str_a[convBuffIdx].inputNo,
					convBuff_str_a[convBuffIdx].flag_ptr);
		}
		LP_MODE_EXIT();

	break;

	default:break;
	}
}
