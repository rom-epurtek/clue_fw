 
#include "sw_timer.h"
#include "../eptk_utils/eptk_utils.h"
#include "../conf/app_conf.h"
#include "../apps/app_console.h"
#include <driverlib.h>

//-----------------------------------------------------------
//------------------------local variables------------------
//-----------------------------------------------------------
timer_str_ptr_t timers_ptr_a[SW_TIMERS_MAX_NUM]={0};
volatile bool_t hw_timer_running_f = FALSE;
timer_str_t eptk_utils_sw_timer_delay_str = {0};

//HW Timer Conf
Timer_A_initUpModeParam 		timerInitUpParam = {0};
const Timer_A_initCompareModeParam 	timerInitCompareParam = {0};

/* Local Utils fns*/
inline void eptk_utils_sw_timer_stopGlobalTimer(void)
{
    Timer_A_stop(SW_TIMER_HW_BASE);
	hw_timer_running_f = FALSE;
}

void eptk_utils_sw_timer_startGlobalTimer(void)
{
    Timer_A_startCounter(SW_TIMER_HW_BASE, TIMER_A_UP_MODE);
	hw_timer_running_f = TRUE;
}


/*! ***************************************************************************
 * \fn         int8_t eptk_utils_sw_timer_init(void)
 * \brief      Inits sw_timers utils (doesn't starts hw timer unless needed)
 * \param      none
 * \return     err
 ****************************************************************************** */
int8_t eptk_utils_sw_timer_init(void)
{
	//Init timer up mode (counts to CCR0 then triggers TAxIFG)
	timerInitUpParam.clockSource = SW_TIMER_HW_CLK_SRC;
	timerInitUpParam.clockSourceDivider = SW_TIMER_HW_CLK_DIVIDER;
	timerInitUpParam.timerPeriod = SW_TIMER_HW_COMPARE_VALUE;
	timerInitUpParam.timerInterruptEnable_TAIE =
			TIMER_A_TAIE_INTERRUPT_DISABLE; //Don't use TAxIFG
	timerInitUpParam.captureCompareInterruptEnable_CCR0_CCIE =
			TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE; //UUse CCR0_IFG
	timerInitUpParam.timerClear = TIMER_A_DO_CLEAR;
	timerInitUpParam.startTimer = FALSE;
	Timer_A_initUpMode(SW_TIMER_HW_BASE, &timerInitUpParam);

    return RTRN_OK;
}


/*! ***************************************************************************
 * \fn          eptk_utils_sw_timer_start
 * \brief       Starts sw timer
 * \param       timer_str_ptr 	Pointer to timer struct
 * \param		time_ms			Time in ms (uint32)
 * \param		isContinuous	If TRUE timer restarts after finish
 * \param		fptr_function	If provided, callback function executed at end (/!\ runs in ISR)
 * \param		flag2modify_ptr	If provided, boolean flag to be modified at timer end
 * \param		newVal			Value to set to flag after timer end
 * \return      error code
 *******************************************************************************/
int8_t eptk_utils_sw_timer_start(timer_str_ptr_t timer_str_ptr,
								uint32_t time_ms,
								bool_t isContinuous,
								void (*fptr_function)(),
								volatile bool_t* flag2modify_ptr,
								bool_t newVal)
{
	timer_str_ptr_t* timer_allocator_ptr_ptr;
	uint8_t spaceLeft = 0, found_slot=0, idx=0;

	if(!(timer_str_ptr->isAllocated)){
		/*Try to allocate*/
		timer_allocator_ptr_ptr = &timers_ptr_a[0];
		spaceLeft = SW_TIMERS_MAX_NUM;
		while((spaceLeft-- > 0) && !found_slot){
			if(!*timer_allocator_ptr_ptr || (*timer_allocator_ptr_ptr && !(*timer_allocator_ptr_ptr)->isAllocated)){
				*timer_allocator_ptr_ptr = timer_str_ptr;
				(*timer_allocator_ptr_ptr)->isAllocated = 1;
				(*timer_allocator_ptr_ptr)->idx = idx;
				found_slot = 1;
			}
			else{
				timer_allocator_ptr_ptr++;
			}
			idx++;
		}
		if(!found_slot){
			app_console_log("Timer alloc failed\r\n");
			return RTRN_FULL;
		}
	}

	timer_str_ptr->isFinished = FALSE;
	timer_str_ptr->timerCount = time_ms;
	timer_str_ptr->timerRestart = time_ms;
	timer_str_ptr->isContinuous = isContinuous;

	//Set fn_ptr
	timer_str_ptr->callback_fn_ptr = fptr_function;
	timer_str_ptr->flag2modify_ptr = flag2modify_ptr;
	timer_str_ptr->newFlagValue = newVal;

	/*If HW timer not running start it*/
	if(!hw_timer_running_f){
		eptk_utils_sw_timer_startGlobalTimer();
	}

    if(time_ms){ //Time == 0 -> on ne l'active pas -> timer infini
		timer_str_ptr->isActive=1;
	}
	else{
		timer_str_ptr->isActive=0;
	}
    return RTRN_OK;
}

/* De-alocate timer in array */
int8_t eptk_utils_sw_timer_delete(timer_str_ptr_t timer_str_ptr)
{
	if(timer_str_ptr->isAllocated){
		timers_ptr_a[timer_str_ptr->idx] = 0 ; /* De-alocate timer in array */
		timer_str_ptr->isAllocated = 0 ;
		timer_str_ptr->isActive = 0 ;
		return RTRN_OK;
	}
	else{
		return RTRN_NOT_FOUND;
	}
}

void eptk_utils_sw_timer_disable(timer_str_ptr_t timer_str_ptr)
{
	timer_str_ptr->isActive = FALSE;
}

int8_t eptk_utils_sw_timer_enable(timer_str_ptr_t timer_str_ptr)
{
	if(timer_str_ptr->isAllocated){
		timer_str_ptr->isActive = TRUE;
		if(!hw_timer_running_f){
			eptk_utils_sw_timer_stopGlobalTimer();
		}
		return RTRN_OK;
	}
	else return RTRN_NOT_FOUND;
}

int8_t eptk_utils_sw_timer_toggle(timer_str_ptr_t timer_str_ptr)
{
	if(timer_str_ptr->isAllocated){
		timer_str_ptr->isActive ^= BIT0;
		if(timer_str_ptr->isActive && !hw_timer_running_f){
			eptk_utils_sw_timer_startGlobalTimer();
		}
		return RTRN_OK;
	}
	else return RTRN_NOT_FOUND;
}

void eptk_utils_sw_timer_forceFinish(timer_str_ptr_t timer_str_ptr)
{
	timer_str_ptr->timerCount = SW_TIMER_HW_PERIOD_MS;
}
bool_t eptk_utils_sw_timer_isFinished(timer_str_ptr_t timer_str_ptr) // return 1 if finished
{
	return timer_str_ptr->isFinished;
}

bool_t eptk_utils_sw_timer_isActive(timer_str_ptr_t timer_str_ptr)
{
    return timer_str_ptr->isActive;
}

/**
 * eptk_utils_sw_timer_delay_ms
 */
void eptk_utils_sw_timer_delay_ms(uint32_t time_ms){

#ifdef USE_WATCHDOG
	WDT_A_hold(WDT_A_BASE);
#endif
	eptk_utils_sw_timer_start(&eptk_utils_sw_timer_delay_str,
				time_ms,
				SW_TIMER_SINGLE_SHOT,
				NULL,
				NULL,NULL);
		while(!eptk_utils_sw_timer_isFinished(&eptk_utils_sw_timer_delay_str)){
			LP_MODE();
		}
#ifdef USE_WATCHDOG
	//WDT_A_start(WDT_A_BASE);
#endif
}

/*! ***************************************************************************
 * \fn          eptk_utils_sw_timer_clear
 * \brief       Reset the sw timer and restart it.
 * \param       timer_str_ptr pointer to timer struct
 * \return      error code
 ******************************************************************************
 */
int8_t eptk_utils_sw_timer_clear(timer_str_ptr_t timer_str_ptr) {
	if(!(timer_str_ptr->isAllocated)){
		return RTRN_NOT_FOUND;
	}
	else if (!timer_str_ptr->isActive){
		return RTRN_ERR;
	}
	else{
		timer_str_ptr->timerCount = timer_str_ptr->timerRestart;
		timer_str_ptr->isFinished = FALSE;
	}
	return RTRN_OK;
}

/**************************************
* Interrupt Service Routines
**************************************/

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=SW_TIMER_HW_CCR0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(SW_TIMER_HW_CCR0_VECTOR)))
#endif
void  eptk_utils_sw_timer_periodIsr(void) //CCR0 ISR
{
	timer_str_ptr_t* timer_allocator_ptr_ptr;
	uint8_t spaceLeft = 0;
	bool_t runningSwTimers = 0;

	timer_allocator_ptr_ptr = &timers_ptr_a[0];
		spaceLeft = SW_TIMERS_MAX_NUM;
		while(spaceLeft-- > 0){
			if(*timer_allocator_ptr_ptr && (*timer_allocator_ptr_ptr)->isAllocated){ //Si allou�
				if ( (*timer_allocator_ptr_ptr)->isActive ){
					if ((*timer_allocator_ptr_ptr)->timerCount > SW_TIMER_HW_PERIOD_MS){
						(*timer_allocator_ptr_ptr)->timerCount -= SW_TIMER_HW_PERIOD_MS;
						runningSwTimers++;
					}
					else{ //End of ws_timer
						//If continuous
						if((*timer_allocator_ptr_ptr)->isContinuous){
							(*timer_allocator_ptr_ptr)->timerCount = (*timer_allocator_ptr_ptr)->timerRestart;
							runningSwTimers++;
						}
						else{
							(*timer_allocator_ptr_ptr)->isActive = 0;
						}
						//Set finish flag
						(*timer_allocator_ptr_ptr)->isFinished = TRUE;

						//Callback funtion if not NULL
						if((*timer_allocator_ptr_ptr)->callback_fn_ptr){ //Not null
							(*timer_allocator_ptr_ptr)->callback_fn_ptr();
						}
						//Flag set callback if exists
						else if((*timer_allocator_ptr_ptr)->flag2modify_ptr){
							*((*timer_allocator_ptr_ptr)->flag2modify_ptr) = (*timer_allocator_ptr_ptr)->newFlagValue;
							LPM3_EXIT; //Exit LPM after setting flag value (flag mode)
						}
						else{
							LPM3_EXIT; //For polling mode
						}
					}//else end of timer
				}//If active
			}//If allocated
			timer_allocator_ptr_ptr++;
		} //While iterates timers

		/*Stop HW timer if nothing running to save power*/
		if(!runningSwTimers){
			eptk_utils_sw_timer_stopGlobalTimer();
		}
}

