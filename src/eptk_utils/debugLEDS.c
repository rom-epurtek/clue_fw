/*
 * debugLEDS.c
 *
 *  Created on: 20 juil. 2016
 *      Author: romain
 */

#include "debugLEDS.h"
#include "driverlib.h"

void eptk_utils_debugLEDS_init(void){
	#ifdef LED_GREEN
		DEBUG_LED_OFF(LED_GREEN);
		GPIO_setAsOutputPin(LED_GREEN);
	#endif
	#ifdef LED_ORANGE
		DEBUG_LED_OFF(LED_ORANGE);
		GPIO_setAsOutputPin(LED_ORANGE);
	#endif
	#ifdef LED_BLUE
		DEBUG_LED_OFF(LED_BLUE);
		GPIO_setAsOutputPin(LED_BLUE);
#endif
}

