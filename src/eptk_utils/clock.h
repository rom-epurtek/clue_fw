/*
 * clock.h
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */
#include "driverlib.h"
#include "../conf/bsp.h"

#ifndef EPTK_UTILS_CLOCK_H_
#define EPTK_UTILS_CLOCK_H_

#define UCS_XT2_TIMEOUT 50000UL
#define UCS_XT1_TIMEOUT 50000UL

/**
 * \fn eptk_utils_initClock()
 * \brief Init clock system
 *
 * Uses BSP conf file
 *
 */
void eptk_utils_initClock(void);
void eptk_utils_rtcTimeUpdate(void);

extern volatile Calendar eptk_utils_rtcTime_str;
extern volatile Calendar eptk_utils_rtcTimeNEW_str;

#endif /* EPTK_UTILS_CLOCK_H_ */
