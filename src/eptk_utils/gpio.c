/*
 * gpio.c
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */
#include "driverlib.h"
#include "gpio.h"
#include "return_codes.h"
#include "../conf/app_conf.h"

/* ISR Callbacks arrays */
void (*port1_callback_fn_ptr[8])() = {0};
void (*port2_callback_fn_ptr[8])() = {0};

/*! Reset all GPIOs
 * Set all GPIO pins to output low to prevent floating input and reduce power consumption
 */
void eptk_utils_gpio_reset(){

	    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|/*GPIO_PIN2|*/GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5/*|GPIO_PIN6|GPIO_PIN7*/);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P9, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setOutputLowOnPin(GPIO_PORT_P10, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

	    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P6, /*GPIO_PIN0|*/GPIO_PIN1|/*GPIO_PIN2|*/GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1/GPIO_PIN2/*|GPIO_PIN3*/|GPIO_PIN4|GPIO_PIN5/*|GPIO_PIN6|GPIO_PIN7*/); //RSA PB 0R sur BLE OTA
	    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P9, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
	    GPIO_setAsOutputPin(GPIO_PORT_P10, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

}

int8_t eptk_utils_gpio_setIsr(uint8_t port_base, uint8_t bit, void (*callback_fn_ptr)()){
	int8_t error = RTRN_OK;
	uint8_t clbk_idx = 0;
	uint8_t clbk_bit = bit;

	/* Look for idx from bit */
	while(!(clbk_bit & 0x01)){
		clbk_idx++;
		clbk_bit  = clbk_bit >> 1 ;
	}

	switch(port_base){
	case GPIO_PORT_P1:
		port1_callback_fn_ptr[clbk_idx] = callback_fn_ptr;
		break;

	case GPIO_PORT_P2:
		port2_callback_fn_ptr[clbk_idx] = callback_fn_ptr;
		break;

	default:
		return RTRN_ERR;

	}
	return error;
}

/*
 * PORT2 Interrupt Service Routine
 * Handles acc int 2
 */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(PORT1_VECTOR)))
#endif
void PORT1_ISR (void){
	uint8_t clbk_idx = (__even_in_range(P1IV,8) - 2 ) / 2;

	/* Then callback */
	if(port1_callback_fn_ptr[clbk_idx]){
		(*port1_callback_fn_ptr[clbk_idx])();
		LP_MODE_EXIT();           // exit LPM3
	}

}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT2_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(PORT2_VECTOR)))
#endif
void PORT2_ISR (void){
	uint8_t clbk_idx = (__even_in_range(P2IV,8) - 2 ) / 2;

	/* Then callback */
	if(port2_callback_fn_ptr[clbk_idx]){
		(*port2_callback_fn_ptr[clbk_idx])();
		LP_MODE_EXIT();           // exit LPM3
	}
}
