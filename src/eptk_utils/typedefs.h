/*
 * typedefs.h
 *
 *  Created on: 6 juil. 2016
 *      Author: romain
 */
#ifndef TYPEDEF_H
#define TYPEDEF_H

#ifndef NULL
	#define NULL 0
#endif

typedef enum {
	FALSE = 0,
	TRUE = 1,
} bool_t;

#endif /*TYPEDEF_H*/
