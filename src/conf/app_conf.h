/**
 * \file app_conf.h
 * \brief App related configuration
 * \date 18 dec. 2016
 * \author: RS
 */

#ifndef SRC_CONF_APP_CONF_H_
#define SRC_CONF_APP_CONF_H_


/** \def DRIVERS_TEST_MODE
 * 	Adds driver test functions
 * */
//#define DRIVERS_TEST_MODE

/** \def CLUE_DEBUG
 * Uses DEBUG_LEDS, HEARTBEAT and WDT.
 */
#define CLUE_DEBUG

/** \def USE_DEBUG_LEDS
 *  Activates colored LEDS */

/** \def USE_HEARTBEAT
 *   Activates HEARTBEAT (handled in main) */

/** \def USE_WATCHDOG
 *    Activates WATCHDOG TIMER*/

#ifdef CLUE_DEBUG
	#define USE_DEBUG_LEDS

	#define USE_HEARTBEAT

	#define USE_WATCHDOG

#else
	#define USE_DEBUG_LEDS
	#define USE_WATCHDOG
#endif

#define SW_RESET() 	SW_RESET_BOR()

#ifdef USE_HEARTBEAT
/*HeartBeat ON LED*/
#define HEARTBEAT_TIME_ON			10	//ms
#define HEARTBEAT_TIME_OFF_FAST 	500	//ms
#define HEARTBEAT_TIME_OFF_LONG 	1950
#endif


#define APP_ALIM_REFRESH_TIME		5000
#define LORA_PERIOD					5000
#define MOX_PERIOD					180000	//3 minutes
/* ACCELEROMETER */
#define ACC_MVMT_CHECK_PERIOD		45000
/* TRIGGER VALUE :
 *    FULL SCALE	1 bit value(mg)
 *    	2					16
 *    	4					31
 *    	8					63
 *    	16					125
 */
#define ACC_MOVT_TRIGGER_VALUE 			66 //1.015g
#define ACC_MOVT_TRIGGER_DURATION 		1
#define ACC_HALTED_TRIGGER_VALUE 		66 //1.120g (voir AN)
#define ACC_HALTED_TRIGGER_DURATION 	1
#define ACC_WDT_READ_PERIOD				1000

#define ACC_MOVING_FREQ_MASK	 ACC_DEF_ODR_50 //50Hz //See datasheet p25
#define ACC_HALTED_FREQ_MASK	 ACC_DEF_ODR_10 //10Hz
#define ACC_MOVING_LPM			 ACC_DEF_Lowpower_En
#define ACC_HALTED_LPM			 ACC_DEF_Lowpower_En

#define ALIM_BATT_LOW_THRESH	10 //%
#define ALIM_BATT_OK_THRESH		15 //%

#define MOX_LAUNCH_MEAS_PERIOD_MS	60000 //1min
#define MOX_HEAT_PERIOD_MS			30000 //30sec

/**\def VERSION_STRING
 * \brief HW Version identificator
 * \li 0001 : proto phase 1
 * \li 0002 : v1 FW
 * */
#define VERSION_STRING "0002"

/** \def LP_MODE()
 * \brief Select Low Power Mode for Sleep Mode
 */
#define LP_MODE() 		LPM3;_no_operation()
#define LP_MODE_EXIT()  LPM3_EXIT

/*HELPERS -> nothing to modify */
#define COMPIL_TIME_STRING __DATE__
#define INIT_MSG_FORMAT_STRING "\r\n[CLUE] v%s :: build %s\r\n"

#endif /* SRC_CONF_APP_CONF_H_ */
