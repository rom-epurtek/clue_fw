/**
 * \file bsp.h
 * \brief Board Support package
 * \date 18 d�c. 2016
 * \author: RS
 *
 * Contains hardware definitions.
 *
 *	\add to
 */

#ifndef SRC_CONF_BSP_H_
#define SRC_CONF_BSP_H_

#include "driverlib.h"

/** Hardware revision */
//#define BOARD_CLUE_NODE_1_0
#define BOARD_CLUE_NODE_1_1

#if !defined(BOARD_CLUE_NODE_1_0) && !defined(BOARD_CLUE_NODE_1_1)
	#error "Board is not defined in bsp.h"
#elif defined(BOARD_CLUE_NODE_1_0) && defined(BOARD_CLUE_NODE_1_1)
	#error "Multiple boards defined in bsp.h"
#endif

/** Clocks configuration
 * \todo Voir pour mettre le DCO plus haut pour avoir moins de jitter:
 * http://processors.wiki.ti.com/index.php/MSP430_FAQ#Why_should_it_be_possible_to_run_5xx.2F6xx_UCS_DCO_clock_to_frequency_higher_than_25_MHz.3F
 *  */

#define CLK_LFXT_FREQ	32768
#define CLK_HFXT_FREQ	25000000

#define CLK_REFO_FREQ		32768
#define CLK_DCO_FREQ		24000000 			/** 24MHz */
#define CLK_PMM_CORE_LEVEL 	PMM_CORE_LEVEL_3 	/** See Datasheet p16*/

#define CLK_ACLK_FREQ		CLK_REFO_FREQ	 	/** 32.768kHZ */
#define CLK_ACLK_BASE		UCS_REFOCLK_SELECT
#define CLK_ACLK_DIVIDER	UCS_CLOCK_DIVIDER_1

#ifdef CLK_HFXT_FREQ
	#define CLK_SMCLK_FREQ		CLK_HFXT_FREQ	/** 25MHz */
	#define CLK_SMCLK_BASE		UCS_XT2CLK_SELECT
	#define CLK_MCLK_FREQ		CLK_HFXT_FREQ	/** 25MHz */
	#define CLK_MCLK_BASE		UCS_XT2CLK_SELECT
#else
	#define CLK_SMCLK_FREQ		CLK_DCO_FREQ 	/** 24MHz */
	#define CLK_SMCLK_BASE		UCS_DCOCLK_SELECT
	#define CLK_MCLK_FREQ		CLK_DCO_FREQ 	/** 24MHz */
	#define CLK_MCLK_BASE		UCS_DCOCLK_SELECT
#endif
#define CLK_SMCLK_DIVIDER	UCS_CLOCK_DIVIDER_1
#define CLK_MCLK_DIVIDER	UCS_CLOCK_DIVIDER_1

#define CLK_FLL_FREQ_IN_KHZ CLK_MCLK_FREQ/1000
#define CLK_FLL_REF_RATIO	CLK_MCLK_FREQ/CLK_ACLK_FREQ


/****************************/

/* DEBUG LEDS DEFINITION */
#define LED_1_BASE 	GPIO_PORT_P4
#define LED_1_BIT	GPIO_PIN2

#define LED_2_BASE 	GPIO_PORT_P4
#define LED_2_BIT	GPIO_PIN1

#define LED_3_BASE 	GPIO_PORT_P4
#define LED_3_BIT	GPIO_PIN0

#define LED_GREEN	LED_1_BASE, LED_1_BIT
#define LED_ORANGE	LED_2_BASE, LED_2_BIT
#define LED_BLUE	LED_3_BASE, LED_3_BIT
/****************************/

//MOX
//Definition for modified proto (MiCS 4514 with only one operational sensor)

#if defined(BOARD_CLUE_NODE_1_0)

    #define MOX_OX_RSERIE_VAl		820
    #define MOX_OX_RSERIELOW_VAL	10000
    #define MOX_OX_RSERIEFULL_VAL	1010000

    #define MOX_OX_ADC_BASE		GPIO_PORT_P6
    #define MOX_OX_ADC_PIN		BIT3

    #define MOX_OX_HEAT_BASE	GPIO_PORT_P7
    #define MOX_OX_HEAT_PIN		BIT5

    #define MOX_OX_3STE_BASE 	GPIO_PORT_P6
    #define MOX_OX_3STE_PIN		BIT6

    #define MOX_NOX_ADC_INPUT   ADC12_A_INPUT_A3
    //#define MOX_CO_ADC_INPUT  ADC12_A_INPUT_A


#elif defined(BOARD_CLUE_NODE_1_1)

    #define MOX_OX_RSERIE_VAl       820
    #define MOX_OX_RSERIELOW_VAL    2000
    #define MOX_OX_RSERIEFULL_VAL   14000

    #define MOX_OX_ADC_BASE     GPIO_PORT_P6
    #define MOX_OX_ADC_PIN      BIT4

    #define MOX_OX_HEAT_BASE    GPIO_PORT_P7
    #define MOX_OX_HEAT_PIN     BIT6

    #define MOX_OX_3STE_BASE    GPIO_PORT_P6
    #define MOX_OX_3STE_PIN     BIT7


    #define MOX_RED_RSERIE_VAl       820
    #define MOX_RED_RSERIELOW_VAL    340000
    #define MOX_RED_RSERIEFULL_VAL   1090000

    #define MOX_RED_ADC_BASE	GPIO_PORT_P6
    #define MOX_RED_ADC_PIN	    BIT5

    #define MOX_RED_HEAT_BASE	GPIO_PORT_P7
    #define MOX_RED_HEAT_PIN	BIT7

    #define MOX_RED_3STE_BASE	GPIO_PORT_P7
    #define MOX_RED_3STE_PIN	BIT4

    #define MOX_NOX_ADC_INPUT   ADC12_A_INPUT_A4
    #define MOX_CO_ADC_INPUT  ADC12_A_INPUT_A5

#endif

//FTDI
//Bootloader
#define BSL_TX_BASE		GPIO_PORT_P1
#define BSL_TX_PIN		BIT1

#define BSL_RX_BASE		GPIO_PORT_P1
#define BSL_RX_PIN		BIT2

//FTDI
//UART

#define FTDI_UART_TX_BASE		GPIO_PORT_P3
#define FTDI_UART_TX_PIN		BIT4

#define FTDI_UART_RX_BASE		GPIO_PORT_P3
#define FTDI_UART_RX_PIN		BIT5

//FTDI
//BATT_CHG

#define FTDI_BATT_CHG_BASE		GPIO_PORT_P3
#define FTDI_BATT_CHG_PIN		BIT6
#define FTDI_BATT_CHG_PxIN		P3IN
//#define FTDI_BATT_CHG_PxIES		P3IES

//Alim Signals

#define ALIM_SIG_PROG2_BASE		GPIO_PORT_P1
#define ALIM_SIG_PROG2_PIN		BIT4
#define ALIM_SIG_PROG2_PxOUT	P1OUT

#define ALIM_SIG_STAT1_BASE		GPIO_PORT_P1
#define ALIM_SIG_STAT1_PIN		BIT5

#define ALIM_SIG_STAT2_BASE		GPIO_PORT_P1
#define ALIM_SIG_STAT2_PIN		BIT6

#define ALIM_SIG_PG_BASE		GPIO_PORT_P1
#define ALIM_SIG_PG_PIN			BIT7

#define ALIM_BATT_BASE			GPIO_PORT_P6
#define ALIM_BATT_PIN			BIT2

#define ALIM_ADC_INPUT			ADC12_A_INPUT_A2

//Accelerometer

#define ACC_INT2_BASE			GPIO_PORT_P2
#define ACC_INT2_PIN			BIT0

#define ACC_INT1_BASE			GPIO_PORT_P2
#define ACC_INT1_PIN			BIT1

#define ACC_CS_BASE				GPIO_PORT_P2
#define ACC_CS_PIN				BIT2

#define ACC_SPI_BASE			SENSOR_SPI_BASE
#define ACC_FREQ				8000000	/*6MHz*/

//BME280
#define BME_CS_BASE				GPIO_PORT_P2
#define BME_CS_PIN				BIT3

#define BME_FREQ				8000000/*6MHz*/
#define BME_SPI_BASE			SENSOR_SPI_BASE

//GPS
#define GPS_FREQ				6000000	/*6MHz*/
#define	GPS_SPI_BASE			SENSOR_SPI_BASE

#if defined(BOARD_CLUE_NODE_1_0)
	#define GPS_1PPS_BASE			GPIO_PORT_P2
	#define GPS_1PPS_PIN			BIT4
#endif

#define GPS_WAKEUP_BASE			GPIO_PORT_P2
#define GPS_WAKEUP_PIN			BIT5

#define GPS_RST_BASE			GPIO_PORT_P2
#define GPS_RST_PIN				BIT6

#define GPS_ON_OFF_BASE			GPIO_PORT_P2
#define GPS_ON_OFF_PIN			BIT7

#define GPS_CS_BASE				GPIO_PORT_P3
#define GPS_CS_PIN				BIT0

//Sensors SPI

#define SENSOR_SPI_MOSI_BASE		GPIO_PORT_P3
#define SENSOR_SPI_MOSI_PIN			BIT1

#define SENSOR_SPI_MISO_BASE		GPIO_PORT_P3
#define SENSOR_SPI_MISO_PIN			BIT2

#define SENSOR_SPI_CLK_BASE			GPIO_PORT_P3
#define SENSOR_SPI_CLK_PIN			BIT3

#define SENSOR_SPI_BASE				USCI_B0_BASE

//USER SWITCH
#if defined(BOARD_CLUE_NODE_1_0)
	#define USR_SW_BASE				GPIO_PORT_P4
	#define USR_SW_PIN				BIT3
#elif defined(BOARD_CLUE_NODE_1_1)
	#define USR_SW_BASE				GPIO_PORT_P1
	#define USR_SW_PIN				BIT0
#endif

//BLE
#define BLE_AUTORUN_BASE		GPIO_PORT_P5
#define BLE_AUTORUN_PIN			BIT5

#define BLE_RX_BASE				GPIO_PORT_P5
#define BLE_RX_PIN				BIT6

#define BLE_TX_BASE				GPIO_PORT_P5
#define BLE_TX_PIN				BIT7

#define BLE_NRESET_BASE			GPIO_PORT_P7
#define BLE_NRESET_PIN			BIT2

#define BLE_OTA_BASE			GPIO_PORT_P7
#define BLE_OTA_PIN				BIT3

#define BLE_PWR_EN_BASE			GPIO_PORT_P8
#define BLE_PWR_EN_PIN			BIT0

#define BLE_FAULT_BASE			GPIO_PORT_P8
#define BLE_FAULT_PIN			BIT1

//SD Card
#define SD_SPI_BASE				USCI_B2_BASE

#if defined(BOARD_CLUE_NODE_1_0)
	#define SD_CARD_DET_BASE		GPIO_PORT_P8
	#define SD_CARD_DET_PIN			BIT6
#elif defined(BOARD_CLUE_NODE_1_1)
	#define SD_CARD_DET_BASE		GPIO_PORT_P1
	#define SD_CARD_DET_PIN			BIT3
#endif

#define SD_CS_BASE				GPIO_PORT_P8
#define SD_CS_PIN				BIT7

//Lora
#define LORA_TX_BASE			GPIO_PORT_P9
#define LORA_TX_PIN				BIT4

#define LORA_RX_BASE			GPIO_PORT_P9
#define LORA_RX_PIN				BIT5

#define LORA_RST_BASE			GPIO_PORT_P10
#define LORA_RST_PIN			BIT0

//UART Ext
#define UART_EXT_TX_BASE		GPIO_PORT_P10
#define UART_EXT_TX_PIN			BIT4

#define UART_EXT_RX_BASE		GPIO_PORT_P10
#define UART_EXT_RX_PIN			BIT5

//XTAL

#define XIN_XTAL_32768HZ_BASE	GPIO_PORT_P7
#define XIN_XTAL_32768HZ_PIN	BIT0

#define XOUT_XTAL_32768HZ_BASE	GPIO_PORT_P7
#define XOUT_XTAL_32768HZ_PIN	BIT1

#define XIN_XTAL_25MHZ_BASE		GPIO_PORT_P5
#define XIN_XTAL_25MHZ_PIN		BIT2

#define XOUT_XTAL_25MHZ_BASE	GPIO_PORT_P5
#define XOUT_XTAL_25MHZ_PIN		BIT3


/**
 * SPI PINS DEFINITIONS
 * */
#ifdef __MSP430F5419A__
	#define USCI_B0_GPIO_BASE	GPIO_PORT_P3
	#define USCI_B0_MOSI_PIN	GPIO_PIN1
	#define USCI_B0_MISO_PIN	GPIO_PIN2
	#define USCI_B0_CLK_PIN		GPIO_PIN3

	#define USCI_B2_GPIO_BASE	GPIO_PORT_P9
	#define USCI_B2_MOSI_PIN	GPIO_PIN1
	#define USCI_B2_MISO_PIN	GPIO_PIN2
	#define USCI_B2_CLK_PIN		GPIO_PIN3

	// software BOR reset
	#define SW_RESET_BOR()      PMMCTL0 = PMMPW + PMMSWBOR + (PMMCTL0 & 0x0003);
	// software POR reset
	#define SW_RESET_POR()      PMMCTL0 = PMMPW + PMMSWPOR + (PMMCTL0 & 0x0003);
#endif

#endif /* SRC_CONF_BSP_H_ */
