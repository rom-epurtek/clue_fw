/**
 * \file hal_SPI.h
 */
//

#ifndef _SPILIB_C
#define _SPILIB_C
//
//---------------------------------------------------------------
#include "driverlib.h"
#include "hal_spi_b.h"

#include "../conf/bsp.h"
#include "../apps/app_sd_log.h"

//#define withDMA

#ifndef DUMMY_CHAR
#define DUMMY_CHAR 0xFF
#endif

bool_t hal_spi_b_isBusy(uint16_t UsciBase){
	return USCI_B_SPI_isBusy(UsciBase);
}

void hal_spi_b_setPhaPol(uint8_t phase, uint8_t pol, uint16_t UsciBase){
	USCI_B_SPI_changeClockPhasePolarity(UsciBase, phase, pol);
	//Clear receive interrupt flag
		USCI_B_SPI_clearInterrupt(UsciBase,
				USCI_B_SPI_RECEIVE_INTERRUPT);

		// Enable USCI_B2 RX interrupt
		USCI_B_SPI_enableInterrupt(UsciBase,
				USCI_B_SPI_RECEIVE_INTERRUPT);
}

bool_t hal_spi_b_isInit(uint16_t UsciBase){
	return  (bool_t) (~HWREG8(UsciBase + OFS_UCBxCTL1) & UCSWRST);
}

// SPI port functions
int8_t hal_spi_b_init(uint32_t desiredCLKRate, uint8_t phase, uint8_t pol, uint16_t UsciBase)
{
	static //Initialize Master
	USCI_B_SPI_initMasterParam sdSpiParam = {0};
	USCI_B_SPI_changeMasterClockParam changeClkParam = {0};

	if (hal_spi_b_isInit(UsciBase)){

		if(hal_spi_b_isBusy(UsciBase)){
			return RTRN_BUSY;
		}

		if((phase != sdSpiParam.clockPhase)||(sdSpiParam.clockPolarity != pol) ){
			hal_spi_b_setPhaPol(phase,pol,UsciBase);
		}

		if(desiredCLKRate != sdSpiParam.desiredSpiClock){
			changeClkParam.desiredSpiClock = desiredCLKRate;
			changeClkParam.clockSourceFrequency= sdSpiParam.clockSourceFrequency;
			USCI_B_SPI_changeMasterClock(UsciBase,&changeClkParam);
			//Clear receive interrupt flag
				USCI_B_SPI_clearInterrupt(UsciBase,
						USCI_B_SPI_RECEIVE_INTERRUPT);

				// Enable USCI_B2 RX interrupt
				USCI_B_SPI_enableInterrupt(UsciBase,
						USCI_B_SPI_RECEIVE_INTERRUPT);
		}
		return RTRN_OK;
	}

	/*Gpios*/
	switch(UsciBase){
	case USCI_B0_BASE:
		GPIO_setAsPeripheralModuleFunctionInputPin(USCI_B0_GPIO_BASE, USCI_B0_MISO_PIN);
		GPIO_setAsPeripheralModuleFunctionOutputPin(USCI_B0_GPIO_BASE, USCI_B0_MOSI_PIN +
				USCI_B0_CLK_PIN);
		break;
	case USCI_B2_BASE:
		GPIO_setAsPeripheralModuleFunctionInputPin(USCI_B2_GPIO_BASE, USCI_B2_MISO_PIN);
		GPIO_setAsPeripheralModuleFunctionOutputPin(USCI_B2_GPIO_BASE, USCI_B2_MOSI_PIN +
				USCI_B2_CLK_PIN);
		break;

	default:
		return RTRN_NOT_FOUND;
	}

	sdSpiParam.selectClockSource = USCI_B_SPI_CLOCKSOURCE_SMCLK;
	sdSpiParam.clockSourceFrequency = UCS_getSMCLK();
	sdSpiParam.desiredSpiClock = desiredCLKRate;
	sdSpiParam.msbFirst = USCI_B_SPI_MSB_FIRST;
	sdSpiParam.clockPhase = phase;
	sdSpiParam.clockPolarity = 	pol;

	USCI_B_SPI_initMaster(UsciBase, &sdSpiParam);

	//Enable SPI module
	USCI_B_SPI_enable(UsciBase);

	//Clear receive interrupt flag
	USCI_B_SPI_clearInterrupt(UsciBase,
			USCI_B_SPI_RECEIVE_INTERRUPT);

	// Enable USCI_B2 RX interrupt
	USCI_B_SPI_enableInterrupt(UsciBase,
			USCI_B_SPI_RECEIVE_INTERRUPT);

	return RTRN_OK;
}

//Send one byte via SPI
unsigned char hal_spi_b_sendByte(const unsigned char data , uint16_t UsciBase){
	WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
	USCI_B_SPI_transmitData( UsciBase, data);            // write
	WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
	return (USCI_B_SPI_receiveData(UsciBase)/*halSPIRXBUF*/);
}

unsigned char hal_spi_b_readByte(uint16_t UsciBase){
	WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
	USCI_B_SPI_transmitData(UsciBase,DUMMY_CHAR);     // dummy write
	WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
	return USCI_B_SPI_receiveData(UsciBase);
}

//Read a frame of bytes via SPI
unsigned char hal_spi_b_readFrame(unsigned char* pBuffer, unsigned int size, uint16_t UsciBase){
	unsigned long i = 0;
	// clock the actual data transfer and receive the bytes; spi_read automatically finds the Data Block
	WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
	for (i = 0; i < size; i++){
		USCI_B_SPI_transmitData(UsciBase,DUMMY_CHAR);     // dummy write
		WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
		pBuffer[i] = USCI_B_SPI_receiveData(UsciBase);
	}

	return(0);
}
unsigned char  hal_spi_readWriteFrame(unsigned char* srcBuffer, unsigned char* destBuffer, unsigned int size, uint16_t UsciBase){
	unsigned long i = 0;
	// clock the actual data transfer and receive the bytes; spi_read automatically finds the Data Block
		WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
		for (i = 0; i < size; i++){
			USCI_B_SPI_transmitData(UsciBase,srcBuffer[i]);     // dummy write
			WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
			destBuffer[i] = USCI_B_SPI_receiveData(UsciBase);
		}
		return(0);
}


//Send a frame of bytes via SPI
unsigned char hal_spi_b_sendFrame(unsigned char* pBuffer, unsigned int size, uint16_t UsciBase)
{
	unsigned long i = 0;
	// clock the actual data transfer and receive the bytes; spi_read automatically finds the Data Block
	WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
	for (i = 0; i < size; i++){
		USCI_B_SPI_transmitData(UsciBase, pBuffer[i]);     // write
		WAIT_WHILE_HAL_SPI_IS_BUSY(UsciBase);
	}
	return 0;
}
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector= USCI_B0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_B0_VECTOR)))
#endif
void USCI_B0_ISR(void)
{
	switch(__even_in_range(UCB0IV,4))
	{
	//Vector  - TXIFG
	case USCI_UCTXIFG:
		LP_MODE_EXIT();
		break;
	//Vector 2 - RXIFG
	case USCI_UCRXIFG:
		LP_MODE_EXIT();
		break;
	default: break;
	}
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector= USCI_B1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_B1_VECTOR)))
#endif
void USCI_B1_ISR(void)
{
	switch(__even_in_range(UCB1IV,4))
	{
	//Vector  - TXIFG
	case USCI_UCTXIFG:
		LP_MODE_EXIT();
		break;
	//Vector 2 - RXIFG
	case USCI_UCRXIFG:
		LP_MODE_EXIT();
		break;
	default: break;
	}
}
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector= USCI_B2_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_B2_VECTOR)))
#endif
void USCI_B2_ISR(void)
{
	switch(__even_in_range(UCB2IV,4))
	{
	//Vector  - TXIFG
	case USCI_UCTXIFG:
		LP_MODE_EXIT();
		break;
	//Vector 2 - RXIFG
	case USCI_UCRXIFG:
		LP_MODE_EXIT();
		break;
	default: break;
	}
}


//---------------------------------------------------------------------
#endif /* _SPILIB_C */
