// *************************************************************************************
//
// Filename:  hal_SPI.h: 
// Declarations for Communication with the MMC (see mmc.c) in unprotected SPI mode.
//
// Version 1.1
//    added ul declaration in macros mmcWriteSector and mmcReadSector
// *************************************************************************************

#ifndef _SPILIB_H
#define _SPILIB_H

#include <driverlib.h>
#include "../eptk_utils/eptk_utils.h"

/*http://dlnware.com/theory/SPI-Transfer-Modes*/
#define HAL_SPI_B_PHASE_0 	USCI_B_SPI_PHASE_DATA_CAPTURED_ONFIRST_CHANGED_ON_NEXT
#define HAL_SPI_B_PHASE_1 	USCI_B_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT
#define HAL_SPI_B_POL_0		USCI_A_SPI_CLOCKPOLARITY_INACTIVITY_LOW
#define HAL_SPI_B_POL_1		USCI_A_SPI_CLOCKPOLARITY_INACTIVITY_HIGH

//#define WAIT_WHILE_HAL_SPI_IS_BUSY(x)	while(USCI_B_SPI_isBusy(x)){ LP_MODE(); }// CPU off, enable interrupts //UCB2STATW & UCBUSY
#define WAIT_WHILE_HAL_SPI_IS_BUSY(x)	while(HWREG8(x + OFS_UCBxSTAT) & UCBUSY){ LP_MODE(); }// CPU off, enable interrupts //UCB2STATW & UCBUSY


#define DUMMY_CHAR 	0xFF

// Function Prototypes
bool_t hal_spi_b_isBusy(uint16_t UsciBase);
int8_t hal_spi_b_init (uint32_t desiredCLKRate, uint8_t phase, uint8_t pol, uint16_t UsciBase);
unsigned char hal_spi_b_sendByte(const unsigned char data, uint16_t UsciBase);
unsigned char hal_spi_b_readByte(uint16_t UsciBase);
unsigned char hal_spi_b_readFrame(unsigned char* pBuffer, unsigned int size, uint16_t UsciBase);
unsigned char  hal_spi_b_sendFrame(unsigned char* pBuffer, unsigned int size, uint16_t UsciBase);
unsigned char  hal_spi_readWriteFrame(unsigned char* srcBuffer, unsigned char* destBuffer, unsigned int size, uint16_t UsciBase);

#endif /* _SPILIB_H */
