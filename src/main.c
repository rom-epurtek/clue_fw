/**
 * \file main.c
 * \brief FW Main src file
 * \author RS
 * \version 1.0
 * \date 17 dec 2016
 *
 * Contains FW entry point
 *
 */

#include <driverlib.h>

#include "eptk_utils/eptk_utils.h"

#include "apps/app_heartBeat.h"
#include "apps/app_acc.h"
#include "apps/app_alim.h"
#include "apps/app_ble.h"
#include "apps/app_bme280.h"
#include "apps/app_console.h"
#include "apps/app_gps.h"
#include "apps/app_lora.h"
#include "apps/app_sd_log.h"
#include "apps/app_usb.h"
#include "apps/app_mox.h"
#include "apps/app_stateMachine.h"

// disables the watchdog between the __start() and the __crt_0init()
#if defined(__GNUC__)
/*__attribute__ ((section(".noinit")))
uint16_t SysRstIv;*/
__attribute__ ((naked,section(".crt_0000start")))
void __preinit_disable_watchdog() {
	WDTCTL = (WDTPW + WDTHOLD);
	// save reset information
	//SysRstIv = SYSRSTIV;
}
#endif

/**
 * \fn int main(void)
 * \brief Main function
 *
 * Contains inits + main loop
 *
 */


int main(void) {
	volatile int8_t errorCode = RTRN_OK;


	/*WATCHDOG*/
#ifdef USE_WATCHDOG
	// ~ 45sec based on VLOCLK
	WDT_A_initWatchdogTimer(WDT_A_BASE, WDT_A_CLOCKSOURCE_VLOCLK, WDT_A_CLOCKDIVIDER_512K);
	WDT_A_start(WDT_A_BASE);
#else
	// Stop watchdog timer
		WDT_A_hold(__MSP430_BASEADDRESS_WDT_A__);
#endif

	eptk_utils_gpio_reset();
	__enable_interrupt();

	eptk_utils_initClock();
	eptk_utils_sw_timer_init();
	eptk_utils_adc_init();

#ifdef USE_DEBUG_LEDS
	eptk_utils_debugLEDS_init();//Debug LEDS INIT
#endif

	/*ALIM*/
	app_alim_init();

	/*USB*/
	app_usb_init();

	/*CONSOLE*/
	app_console_init();

	/* SD log*/
	errorCode = app_sd_log_init();
	if(errorCode != RTRN_OK){
		app_console_log("Err SD init (ERR %d)\r\n", errorCode);
		DEBUG_LED_ON(LED_ORANGE);//Debug
	}

	//Debug pour le GPS (todo clean)
	GPIO_setOutputHighOnPin(ACC_CS_BASE, ACC_CS_PIN);
	GPIO_setOutputHighOnPin(GPS_CS_BASE, GPS_CS_PIN);
	GPIO_setOutputHighOnPin(BME_CS_BASE, BME_CS_PIN);

	/*Init BME280 first to stay in SPI*/
	errorCode = app_bme280_init();
	if(errorCode != RTRN_OK){
		app_console_log("Err BME init (ERR %d)\r\n", errorCode);
		DEBUG_LED_ON(LED_ORANGE);//Debug
	}

	/* ACC*/
	errorCode = app_acc_init();
	if(errorCode != RTRN_OK){
		app_console_log("Err ACC init (ERR %d)\r\n", errorCode);
		DEBUG_LED_ON(LED_ORANGE);//Debug
	}

	/* GPS*/
	errorCode = app_gps_init();
	if(errorCode != RTRN_OK){
		app_console_log("Err GPS init (ERR %d)\r\n", errorCode);
		DEBUG_LED_ON(LED_ORANGE);//Debug
	}

	//DEBUG
	/* BLE */
	errorCode = app_ble_init();
	if(errorCode != RTRN_OK){
		app_console_log("Err BLE init (ERR %d)\r\n", errorCode);
	}

	/*LORA*/
	/*errorCode = app_lora_init();
	if(errorCode != RTRN_OK){
		app_console_log("Err LORA init (ERR %d)\r\n", errorCode);
	}*/

	/*MOX*/
	errorCode = app_mox_init();
	if(errorCode != RTRN_OK){
		app_console_log("Err MOX init (ERR %d)\r\n", errorCode);
	}

#ifdef USE_WATCHDOG
	// ~ 15sec based on ACLK
	WDT_A_initWatchdogTimer(WDT_A_BASE, WDT_A_CLOCKSOURCE_ACLK, WDT_A_CLOCKDIVIDER_512K);
	WDT_A_start(WDT_A_BASE);
#endif

	//Heartbeat
#ifdef USE_HEARTBEAT
	eptk_utils_sw_timer_start(&heartBeat_timer_struc, HEARTBEAT_TIME_ON ,SW_TIMER_CONTINUOUS,NULL, &heartBeat_toDo_f, TRUE);
#endif

	app_stateMachine_init();

	/* Main loop */
	while(1){

		errorCode+= app_acc_task();

		errorCode+= app_bme280_task();

		errorCode+= app_gps_task();//Returns SLEEP_OK/BUSY

		//errorCode+= app_lora_task();

		errorCode+= app_mox_task();

		errorCode+= app_console_task();//Returns SLEEP_OK/BUSY

		errorCode+= app_alim_task();

		errorCode+= eptk_utils_UART_task();//Returns SLEEP_OK/BUSY

		app_stateMachine_task();

#ifdef USE_HEARTBEAT
		heartBeatTimer_task();
#endif

		if( RTRN_SLEEP_OK == errorCode){

#ifdef USE_WATCHDOG
			/*WATCHDOG*/
			WDT_A_hold(WDT_A_BASE);
#endif
			/*Dodo*/
			LP_MODE();
		}
		else{
			errorCode = RTRN_SLEEP_OK;
		}
#ifdef USE_WATCHDOG
		/*WATCHDOG*/
		WDT_A_resetTimer(WDT_A_BASE);
		WDT_A_start(WDT_A_BASE);
#endif
	}

}
