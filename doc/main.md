\mainpage CLUE Node FW API
 \section intro_sec Introduction
 
 This is the introduction.
\section Configuration
\subsection BSP
see conf/bsp.h
\subsection APP CONF
see conf/app_conf.h

\section modules Modules
Found in app folder
\subsection app_state_machine Machine à états principale
see app_state_machine
\subsection app_sd_log SD Card LOG
see app_sd_log.h

\subsection app_gps GPS
see app_gps.h

\subsection app_acc ACCELEROMETER
see app_acc.h

\subsection app_bme280 Temp/Hygro sensor BME280
see app_bme280h

\subsection app_mics MiCS 4514 Air Sensor
see app_mics.h

\subsection app_lora LoRa Module
Supported module is Microchip RN2483
see app_lora.h

\subsection app_ble Bluetooth Low Energy module
see app_ble

\subsection app_batt Gestion batterie

* Mesure tension batterie
* Gestion courant de charge (#PWREN FTDI...)
* LEDS état charge
see app_batt.h

\subsection app_uart UARTs

* FTDI UART-USB
* UART ACC
* UART BLE
see app_uart.h

\subsection app_bme280 Temp/Hygro sensor BME280
see app_bme280h

\subsection app_mics MiCS 4514 Air Sensor
see app_mics.h

\section hal Hardware Abstraction Layer
Contains low level functions based on actual hardware peripherals
See hal folder