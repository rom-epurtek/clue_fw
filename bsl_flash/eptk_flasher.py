#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
from Tkinter import *
from tkFileDialog   import askopenfilename
from tkMessageBox import *
import serial.tools.list_ports
import subprocess
import binascii
import sys
import time
import string

consoleStopVar = 0
sn = str()

def refreshComPorts():
    print "Scanning serial ports..."
    liste.delete(0, END)
    ports =  serial.tools.list_ports.comports()
    for port in ports:
        if port[2] != 'n/a' and port[2].find("VID:PID=0403:6015")!= -1 :
            liste.insert(END, port[0])
            liste.selection_set(END)
            #print port[0]
    

def openFW():
    filenameVar.set(askopenfilename(initialdir="../out/",title="Open FW file",filetypes=[('txt','.txt'),('all files','.*')]))
    if filenameVar.get() :
        labelFW.config(text = filenameVar.get())
        flashBTN.configure(state='normal')

def enterBSL():
    comport =  liste.get(map(int, liste.curselection())[0])
    if len(comport) > 0:
        consoleStopVar = 1
        with serial.Serial(comport, 115200, timeout=1) as ser:
            print comport + " open"
            ser.write("bsl\r\n")# Write
            ser.close()
    else:
        showerror(title="Port COM Err", message="Select valid COM Port")

def createScript(comport, filename):
    #check SN
    b_flashSn = varChk.get()
    if b_flashSn:
        #SN creation
        sn = entree.get() + str(spin.get()).zfill(2)
        print "WRITING SN : " + sn
        sn = sn[:12] if len(sn) > 12 else sn
        print "WRITING SN : " + sn
        hexSn = open("./sn.txt",'w')
        hexSn.write("@1980\n")
        hexSn.write(' '.join(binascii.hexlify(c) for c in sn) + " 00\nq")
        hexSn.close()

    #open template
    if b_flashSn:
        templateFile = open('./template_scriptSN', 'r+')
    else:
        templateFile = open('./template_script', 'r+')
    #insert datas
    outfile= open('./script_out.txt', 'w') 
    fContent = unicode(templateFile.read(), "utf-8")
    print "Comport selected : "+comport
    fContent = fContent.replace("{COMPORT}", comport)
    print "Filename : " + filename
    fContent = fContent.replace("{FILE}", filename)
    outfile.write((fContent.encode("utf-8")))
    templateFile.close()
    outfile.close()
    print "File script_out.txt created" 

def commandFlash():
    #validate port (and delete it ?)
    comport =  liste.get(map(int, liste.curselection())[0])

    #debug
    #comport = "COM88"
    
    if len(comport) > 0:
        print comport[0]
        #create text file
        createScript(comport, filenameVar.get())
        #save log btn
        btnSave.configure(state='normal')
        #launch flash
        subprocess.call("BSL-Scripter.exe script_out.txt")
        print "End of flash process. Press Ctrl-C to close COMPORT"
        #BSL console
        with serial.Serial(comport, 115200, timeout=1) as ser:
            while not consoleStopVar:
                sr_input = ser.readline()
                if sr_input :
                    print sr_input
            ser.close
    else:
        showerror(title="Port COM Err", message="Select valid COM Port")


def naccheck(entry, var):
    if var.get() == 0:
        entry.configure(state='disabled')
    else:
        entry.configure(state='normal')

def clearConsole():
    text.delete(1.0, END)
    
def saveLog():
    current_time = time.localtime()
    logfile = open("./log.csv", 'a+')
    str2write = time.strftime('%Y:%m:%d %H:%M:%S', current_time) + "\t"
    str2write = str2write + entree.get() + str(spin.get()).zfill(2)
    str2write = str2write + "\t" + filenameVar.get()
    str2write = str2write + "\t" + liste.get(map(int, liste.curselection())[0]) + "\r\n"
    logfile.write(str2write.encode("utf-8"))
    logfile.close

fenetre = Tk()

#COM PORT
com_label = LabelFrame(fenetre, text="Select COM Port", padx=10, pady=10)
#com_label.pack(fill="none", expand="no")
com_label.grid(row=0, column=0, rowspan = 2)
# liste port serie
liste = Listbox(com_label, selectmode=SINGLE)
liste.grid(row=0, column=0, columnspan=2)
#btn refresh
btnRefrsh = Button(com_label, text="Refresh", command=refreshComPorts)
btnRefrsh.grid(row=1, column=0,  sticky=NSEW)
#enter BSL
btnBSL = Button(com_label, text="Send BSL Command", command=enterBSL)
btnBSL.grid(row=1, column=1, sticky=NSEW)

#FW
fw_label = LabelFrame(fenetre, text="FW File", padx=10, pady=10)
fw_label.grid(row=0, column=1, sticky=N)
#fw select
btnFW = Button(fw_label, text="Select FW File", command=openFW)
btnFW.grid(row=0, column=0, columnspan=2)
filenameVar = StringVar()
labelFW = Label(fw_label, text="No FW selected", width=65)
labelFW.grid(row=1, column=0, columnspan=2)
# checkbox flash SN
varChk = IntVar()
entree = Entry(fw_label, width=6, state='disabled')
checkbox = Checkbutton(fw_label, text="Flash SN", variable=varChk, command=lambda e=entree, v=varChk: naccheck(e,v))
checkbox.grid(row=2, column=0, sticky=W)
# entrée SN
value = StringVar() 
value.set("SN")
entree.grid(row=2, column=1, sticky=W)
spin = Spinbox(fw_label, from_=0, to=99, width=2)
spin.grid(row=2, column=2, sticky=W)

#FLASH BTN
flashBTN = Button(fenetre, text="FLASH", font="-weight bold -size 20", state='disabled', command=commandFlash)
flashBTN.grid(row=1, column=1, sticky=N)

#save log
btnSave = Button(fenetre, text="Save to log", command=saveLog, state="normal")
btnSave.grid(row=2, column=1, sticky=N)


#CONSOLE
##console_frame = LabelFrame(fenetre, text="Console", padx=10, pady=10)
##console_frame.grid(row=2, columnspan=2)
###add a frame and put a text area into it
##text=Text(console_frame)
### add a vertical scroll bar to the text area
##scroll=Scrollbar(console_frame)
##text.configure(yscrollcommand=scroll.set)
###pack everything
##text.pack(side=LEFT)
##scroll.pack(side=RIGHT,fill=Y)
###textPad.pack(side=TOP)
###clear button
##Button(text = "Clear console", command = clearConsole)
##
##StdoutToWidget(text).start()

refreshComPorts()
fenetre.mainloop()

